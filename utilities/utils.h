#pragma once

#include <vector>
#include <string>
#include <bitset>
#include <sstream>

namespace myutils
{
	typedef unsigned char BYTE;
	const size_t BYTE_SIZE = 8;

	std::vector<std::string> str_split(const std::string &str, const std::string &delim = " ");

	std::bitset<8> byte2bits(BYTE byte);
	std::bitset<8> byte2bits(const std::string &byte);

	std::vector<bool> to_bvector(const std::string &str);
	std::vector<bool> to_bvector(size_t number, size_t size);

	void inc_bvector(std::vector<bool> &vb);
	// convert vector of bit to bytes in little endian
	std::vector<BYTE> to_bytes(const std::vector<bool> &vb);

	size_t to_number(const std::vector<bool> &bools);
	inline size_t to_number(const std::string &str)
	{
		return std::stoi(str);
	}

	// get lower bits
	template<typename T>
	inline T get_lower(T value, T count, T start = 0)
	{
		T mask = (1 << (count + start)) - 1;
		value &= mask;
		return value >> start;
	}

	template<size_t Size>
	bool bitset_xor(const std::bitset<Size> &number)
	{
		bool res = false;
		for (size_t i = 0; i < Size; ++i)
			res ^= number[i];

		return res;
	}

	std::vector<size_t> to_number_vector(const std::string &number, size_t base, const std::string &delim = " ");
	
	template<typename Type> 
	std::string to_string(const std::vector<Type> &v)
	{
		std::string res;
		for (const auto &m : v)
			res += std::to_string(m);

		return res;
	}

	template<typename T, typename C>
	void all_combination(std::vector<std::pair<C, T> > &v1, const std::vector<std::pair<C, T > > &v2)
	{
		if (v1.empty())
		{
			v1 = v2;
			return;
		}
		if (v2.empty())
			return;

		std::vector<std::pair<C, T > > res(v1.size() * v2.size());
		auto it = begin(res);
		auto it_v2 = begin(v2);
		auto it_v1 = begin(v1);
		for (; it != end(res); ++it_v2, ++it)
		{
			if (it_v2 == end(v2))
			{
				++it_v1;
				it_v2 = begin(v2);
			}

			*it = *it_v1;	// copy v1
			std::copy(begin(it_v2->first), end(it_v2->first), std::back_inserter(it->first));	// copy v2
			it->second.insert(begin(it_v2->second), end(it_v2->second));	// insert v2			
		}

		std::swap(v1, res);
	}

	template<class T>
	std::string to_hexstring(T val)
	{
		std::string res(sizeof(T)* 2 + 2, '0');
		res[1] = 'x';

		std::stringstream ss;
		ss << std::hex << val;
		
		auto str = ss.str();
		std::copy(std::rbegin(str), std::rend(str), std::rbegin(res));

		return std::move(res);
	}

	inline std::string to_hexstring(unsigned char val)
	{
		std::string res("0x00");

		std::stringstream ss;
		ss << std::hex << static_cast<unsigned int>(val);

		auto str = ss.str();
		if (str.size() == 1)
			res[3] = str[0];
		else
		{
			res[2] = str[0];
			res[3] = str[1];
		}

		return std::move(res);
	}

	// hex string to sign integer
	inline int hstosi(std::string str)
	{
		auto ispace =  str.find(' ');
		if (ispace != std::string::npos)
			str = str.substr(ispace + 1);

		int res = static_cast<int>(stoll(str,  nullptr, 16));
		if (str[2] == 'f' || str[2] == 'e' || str[2] == 'd' || str[2] == 'c' || str[2] == 'b' || str[2] == 'a' || str[2] == '9' || str[2] == '8')	// sign
			switch (str.size() - 2)	// first prefix 0x
		{
			case 2:	// 8bit
				return res | 0xffffff00;
			case 4:	// 16bit
				return res | 0xffff0000;
			default:	// other should be right
				return res;
		}

		return res;
	}
}
