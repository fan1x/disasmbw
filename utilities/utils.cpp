#include "utils.h"

#include <algorithm>
#include <vector>
#include <cassert>

using std::bitset;
using std::string;
using std::for_each;
using std::count;
using std::vector;
using std::stoi;

std::vector<std::string> myutils::str_split(const std::string &str, const std::string &delim)
{
	std::vector<std::string> res;
	
	size_t s = 0;
	size_t f = str.find(delim, 0);

	while(f != str.npos)
	{
		res.push_back(str.substr(s, f - s));
		s = f + delim.size();
		f = str.find(delim, s);
	}

	if (str.size() - s)
	{
		res.push_back(str.substr(s, str.size() - s));
	}	

	return res;
}

std::bitset<8> myutils::byte2bits(myutils::BYTE byte)
{
	return std::bitset<8>(byte);
}

std::bitset<8> myutils::byte2bits(const std::string &byte)
{
	return byte2bits(static_cast<unsigned char>(std::stoi(byte, nullptr, 16)));
}

size_t myutils::to_number(const std::vector<bool> &bools)
{
	size_t res = 0;
	size_t multiply = 1;
	for_each(bools.cbegin(), bools.cend(), [&res,&multiply](bool b)
	{
		res += b*multiply;
		multiply *= 2;
	});
	
	return res;
}

// convert number to vector of size in little endian
std::vector<bool> myutils::to_bvector(size_t number, size_t size)
{
	std::vector<bool> res(size);
	std::generate(begin(res), end(res), [&number]()
	{
		bool ret = (number % 2) > 0;
		number >>= 1;	// shift left
		return ret;
	});

	return res;
}

// increment vector<bool> by 1
void myutils::inc_bvector(std::vector<bool> &vb)
{
	if (*begin(vb) == false)
	{
		*begin(vb) = true;
		return;
	}

	auto it = std::find_if_not(begin(vb), end(vb), [](bool bit)
	{
		return bit;
	});

	std::transform(begin(vb), it, begin(vb), [](bool b)
	{
		return false;
	});

	if (it != end(vb))
		*it = true;
}

std::vector<myutils::BYTE> myutils::to_bytes(const std::vector<bool> &vb)
{
	assert(!(vb.size() % BYTE_SIZE));
	size_t i = BYTE_SIZE -1;
	std::bitset<BYTE_SIZE> buff;
	std::vector<BYTE> res;

	std::for_each(crbegin(vb), crend(vb), [&i, &buff, &res](bool bit)
	{
		buff[i--] = bit;
		if (i < 0)
		{
			res.push_back(static_cast<BYTE>(buff.to_ulong()));
			i = BYTE_SIZE - 1;
		}
	});

	if (i)
		res.push_back(static_cast<BYTE>(buff.to_ulong()));

	return std::move(res);
}

std::vector<bool> myutils::to_bvector(const std::string &str)
{
	vector<bool> res;
	for(char c : str)
	{
		res.push_back(c == '1');
	}

	return res;
}



std::vector<size_t> myutils::to_number_vector(const std::string &number, size_t base, const std::string &delim)
{
	std::vector<size_t> res;
	for (std::string str : str_split(number, delim))
	{
		res.push_back(std::stoull(str, 0, base));
	}

	return res;
}



