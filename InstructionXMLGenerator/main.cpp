#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <exception>
#include <sstream>
#include <algorithm>

using namespace std;
using std::ifstream;
using std::string;
using std::vector;
using std::getline;
using std::set;
using std::exception;
using std::stringstream;
using std::for_each;

unsigned line = 0;

vector<string> split_string(string str, char delimiter)
{
	vector<string> res;
	
	string buff = "";
	for(string::iterator it = str.begin(); it != str.end(); ++it)
	{
		if (*it != delimiter)
		{
			buff += *it;
		}
		else
		{
			if (buff.empty())
				continue;

			res.push_back(buff);
			buff = "";
		}
	}

	if (buff.size() != 0)
	{
		res.push_back(buff);
	}

	return res;
}	

class Operand
{
public:
	Operand(string operand)
	{
		vector<string> op = split_string(operand, ' ');
		name = op[0];

		bool wait = false;
		int j = 3;
		string buff = "";
		for(size_t i=op.size() - 1; i > 0 ; --i)
		{
			buff = op.at(i)+" "+buff;
			
			if (op.at(i).find("(") != op.at(i).npos && op.at(i).find(")") != op.at(i).npos ||  op.at(i).find("+") != op.at(i).npos)
			{
				continue;
			}
			else if (op.at(i).find(")") != op.at(i).npos)
			{
				wait = true;
			}
			else if (op.at(i).find("(") != op.at(i).npos)
			{
				wait = false;
			}
			else if (!wait && op.at(i-1) != "+" && op.at(i-1) != "implicit")
			{
				operands[j--] = buff;
				buff = "";
			}
		}
	}

	string get_name()
	{
		return name;
	}
	string to_xml()
	{
		stringstream res;

		res << "<operand name=\"" << name << "\" ";
		for(int i=0; i<4; ++i)
		{
			res << "op" << i+1 << "=\"" << operands[i] << "\" ";
		}
		res << " />";
		
		return res.str();
	}
private:
	string name;
	string operands[4];
};

class Format
{
public:
	Format(vector<string> inames,const string &format):
		original(format)
	{
		string iname;
		vector<string> split = split_string(format, ' ');

		auto it = find_first_of(begin(split), end(split), begin(inames), end(inames), [](const std::string str1, const std::string str2_inames)->bool
		{
			return str1 == str2_inames || str1 == 'V' + str2_inames;
		});
		
		if (it == end(split))
		{
			std::cout << format << std::endl;
			throw std::exception("Not found instruction name");			
		}
			

		Format::format = *it;
		for_each(begin(split), it, [this](const string &s)
		{
			opcode += s + " ";
		});
		
		vector<string> operands;		
		set<string> hop = { "AL", "CL", "AX", "DX", "EAX", "RAX", "0", "1", "3", 
			"CR0-CR7", "CR8", "CS", "SS", "DS", "ES", "FS", "GS", "DR0-DR7", "ST(0)", 
			"ST(i)", "XMM0"};
		
		++it;
		for (size_t i = 0;
			it->at(it->size() -1) == ',' ||	// , at the end
			islower(it->at(0)) ||	// lower case
			hop.find(*it) != end(hop);	// hard coded
		++it, ++i)
		{
			operands.push_back(*it);
			Format::format += " " + *it;
			if (i == 4)
				break;
		}

		op_en = *it++;	// operand encoding

		if (it->find('/') != string::npos)	// 64/32 mode
		{
			bit64 = *begin(*it) == 'V';
			bit32 = *rbegin(*it) == 'V';
			bit16 = false;
			++it;
		}
		else
		{
			bit64 = *it == "Valid" || *it == "V";
			++it;
			bit16 = bit32 = *it == "Valid" || *it == "V";
			++it;
		}

		for_each(it, end(split), [this](const string &s)
		{
			description += " " + s;
		});
	}
	/*
	Format(set<string> names, string format, set<string> operands)
	{
		try {
		vector<string> split = split_string(format, ' ');

		int i=0;
		string buff = "";
		for(; i < split.size(); ++i)
		{
			if (names.find(split.at(i)) != names.end())
			{
				break;
			}
			
			buff += split.at(i) + " ";
		}
		opcode = buff;

		buff = "";
		for(; i < split.size(); ++i)
		{
			if (operands.find(split.at(i)) != operands.end() || operands.empty() && split.at(i) == "Valid")
			{
				if (split.at(i) != "Valid")
				{
					op_en = split.at(i);
				}
				break;
			}

			buff += split.at(i) + " ";
		}
		Format::format = buff;

 		vector<string> modes = split_string(split.at(++i), '/');
		bit64 = modes[0];
		gpr = true;
		if (modes.size() > 1)
		{
			gpr = false;
			bit32 = modes[1];
		}

		if (gpr)
		{
			cl_mode = split.at(++i);
			
			description = "";
			for (++i; i < split.size(); ++i)
			{
				description += split.at(i) + " ";
			}
		}
		else
		{
			cf_flag = split.at(++i);

			for (++i; i < split.size(); ++i)
			{
				description += split.at(i) + " ";
			}
		}
		} catch (exception ex)
		{
			std::cout << *names.begin() << ex.what() << std::endl;
		}
	}
	*/
	string to_xml()
	{
		stringstream res;

		res << "<format form=\"" << format << "\" ";
		
		res << "opcode=\"" << opcode << "\" ";
		if (!op_en.empty())
		{
			res << "op_enc=\"" << op_en << "\" ";
		}
		res << "bit64=\"" << bit64 << "\" ";
		res << "bit32=\"" << bit32 << "\" ";
		res << "bit16=\"" << bit16 << "\" ";		
//		res << "cpuid=\"" << cf_flag << "\" ";
		res << "desc=\"" << description << "\" />";

		return res.str();
	}

	inline const string &get_original()
	{
		return original;
	}
private:
	bool gpr;
	string opcode;
	string format;
	bool bit64;
	bool bit32;
	bool bit16;
	string description;
	string cf_flag;
	string op_en;
	string original;
};

class Instruction
{
public:
	Instruction(string names, vector<string> formats)
	{
		vector<string> vnames;
		if (names.find('/') == std::string::npos)
			vnames = split_string(names, ' ');
		else
			vnames = split_string(names, '/');

		for(auto it = vnames.begin(); it != vnames.end(); ++it)
		{
			Instruction::names.insert(*it);
		}


		for (auto it = formats.begin(); it != formats.end(); ++it)
		{
			Instruction::formats.push_back(Format(vnames, *it));
		}
	}
	string to_xml()
	{
		stringstream buff;
		string tmp;

		buff << "\t<instruction name=\"";
		for_each(names.begin(), names.end(), [&buff](string name)
		{
			buff << name << "/";
		});
		tmp = buff.str();
		buff = stringstream();
		buff << tmp.erase(tmp.size() - 1);	// erase last /
		buff << "\" >\n";

		buff << "\t\t<formats>\n";
		for_each(formats.begin(), formats.end(), [&buff](Format & f)
		{
			buff << "\t\t\t" << f.to_xml() << "\n";
		});
		buff << "\t\t</formats>\n";
		/*
		buff << "\t\t<operands>\n";
		for_each(operands.begin(), operands.end(), [&buff](Operand & o)
		{
			buff << "\t\t\t" << o.to_xml() << "\n";
		});
		buff << "\t\t</operands>\n";
		*/
		buff << "\t</instruction>\n";	

		return buff.str();
	}
//private:
	set<string> names;
//	vector<Operand> operands;
	vector<Format> formats;
	set<string> operands_names;
	vector<string> notes;
};

const string input_file = "C:\\Users\\Tom�\\Projects\\disasmBW\\instructions.txt";
const string output_file = "C:\\Users\\Tom�\\Projects\\disasmBW\\instructions_from_xmlgen.xml";

int main()
{
	ifstream ifile(input_file, ifstream::in);
	
	vector<Instruction> instructions;
	string line;
	while(ifile.good())
	{
		while (ifile.good() && line == "")	// empty lines
			getline(ifile, line);

		string names = line;	// save name

		vector<string> formats;
		while (ifile.good())
		{
			getline(ifile, line);			
			if (line == "")
				break;

			formats.push_back(line);
		}

		try 
		{
			instructions.push_back(Instruction(names, formats));
		}
		catch (exception ex)
		{
			std::cout << "Error in instruction: " << names << std::endl;
		}
	}	
	
	ifile.close();

	std::cout << "Filled internal tables, continue? (y/n)" << std::endl;

	std::ofstream ofile(output_file, std::ofstream::out);	// WARNING!!!! The file could be changed
	ofile << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
	ofile << "<instructions>\n";
	for_each(instructions.begin(), instructions.end(), [&ofile](Instruction & i)
	{
		try
		{
			ofile << i.to_xml();
		}
		catch (exception ex)
		{
			std::cout << "Can't convert to xml instruction with first name: " << *i.names.begin() << std::endl;
		}
	});
	ofile << "</instructions>\n";
	ofile.close();

	std::cout << "Created xml file" << std::endl;
	
		
	int x;
	std::cin >> x;
}