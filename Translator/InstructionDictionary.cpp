#include "InstructionDictionary.h"

#include <sstream>

using std::string;


InstructionDictionary::InstructionDictionary(void)
{
}

InstructionDictionary::~InstructionDictionary(void)
{
}

void InstructionDictionary::insert_instruction(const std::string & name, const std::string & encoding, const std::string & format, std::string & help)
{
	InstructionDictionary::_imap[get_encoding(encoding)] = new Instruction(name, encoding, format, help);
}

void InstructionDictionary::insert_displacement(const std::string & name, const std::string & value, const std::string & help)
{
	InstructionDictionary::_displacements[name] = value;
}

std::string InstructionDictionary::get_encoding(const std::string & encoding)
{
	std::string enc = "";
	std::string buff = "";
	
	for(string::const_iterator it = encoding.begin(); it != encoding.end(); ++it)
	{
		if (*it == ' ' || *it == ':')	// whitespace
		{
			if (buff != "")
			{
				enc += parse_buff(buff);
				buff = "";
			}
		}
		else if (*it == '0' || *it == '1')	// 0 or 1
		{
			if (buff != "")
			{
				buff += *it;
			}
			else
			{
				enc += *it;
			}
		}
		else if (isupper(*it))	// uppercase - 1 bit value
		{
			enc += _displacements[*it+""];
		}
		else 
		{
			buff += *it;
		}
	}

	if (buff != "")
	{
		enc += parse_buff(buff);
	}
}

std::string InstructionDictionary::parse_buff(std::string & buff)
{
	std::string res = "";
	if (_displacements.find(buff) != _displacements.end() )	// buff exists
	{
		res = _displacements[buff];
	}
	else  // buff contains single character displacementss
	{
		for(auto c = buff.begin(); c != buff.end(); ++c)
		{
			res += _displacements[*c+""];
		}
	}

	return res;
}
