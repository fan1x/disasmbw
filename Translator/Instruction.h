#pragma once

#include <string>

class Instruction
{
public:
	Instruction(void);
	Instruction(const std::string & name, const std::string & encoding, const std::string & format, const std::string & help);
	~Instruction(void);

	std::string name;
	std::string format;
	std::string encoding;
	std::string help;
};

