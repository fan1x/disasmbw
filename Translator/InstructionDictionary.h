#pragma once

#include <map>
#include <string>

#include "Instruction.h"

class InstructionDictionary
{
public:
	InstructionDictionary(void);
	~InstructionDictionary(void);

	void insert_instruction(const std::string & name, const std::string & encoding, const std::string & format, std::string & help);
	void insert_displacement(const std::string & name, const std::string & value, const std::string & help);
private:
	std::string get_encoding(const std::string & encoding);
	std::string parse_buff(std::string & buff);

	typedef std::map<std::string, std::string> map_str_t;
	typedef std::map<std::string, Instruction *> map_str_instr_t;


	map_str_instr_t _imap;	// instruction mapping
	map_str_t _displacements;	// displacements used in instrucition mapping
};

