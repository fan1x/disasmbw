#include "Debugger.h"
#include "PEUndecorator.h"

#include <Windows.h>
#include <thread>
#include <iostream>

void Dbg::Debugger::Context::fill(const CONTEXT &c)
{
	registers.rip = c.Rip;
	registers.rax = c.Rax;
	registers.rbx = c.Rbx;
	registers.rcx = c.Rcx;
	registers.rdx = c.Rdx;
	registers.rbp = c.Rbp;
	registers.rsp = c.Rsp;
	registers.rsi = c.Rsi;
	registers.rdi = c.Rdi;
	registers.r8 = c.R8;
	registers.r9 = c.R9;
	registers.r10 = c.R10;
	registers.r11 = c.R11;
	registers.r12 = c.R12;
	registers.r13 = c.R13;
	registers.r14 = c.R14;
	registers.r15 = c.R15;
}

Dbg::Debugger::_Context::_Context()
{
}

Dbg::Debugger::Debugger(breakpoint_clbk_t breakpoint_clbk,
	const create_thread_clbk_t &create_thread_clbk,
	const create_process_clbk_t &create_process_clbk,
	const exit_thread_clbk_t &exit_thread_clbk,
	const exit_process_clbk_t &exit_process_clbk,
	const load_dll_clbk_t &load_dll_clbk,
	const unload_dll_clbk_t &unload_dll_clbk,
	const debug_string_clbk_t &debug_string_clbk) :
//	Debugger(), 
	breakpoint_clbk(breakpoint_clbk),
	create_thread_clbk(create_thread_clbk),
	create_process_clbk(create_process_clbk),
	exit_thread_clbk(exit_thread_clbk),
	exit_process_clbk(exit_process_clbk),
	load_dll_clbk(load_dll_clbk),
	unload_dll_clbk(unload_dll_clbk),
	debug_string_clbk(debug_string_clbk)
{
}

Dbg::Debugger::Debugger()
{
}

Dbg::Debugger::~Debugger()
{
}

void Dbg::Debugger::run(const std::string &name, bool pause)
{
	reset();
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DEBUG_EVENT debug_event;
	CONTEXT main_thread_context;
	main_thread_context.ContextFlags = CONTEXT_ALL;
	BYTE old_byte;
	
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if (!CreateProcess(name.c_str(), NULL, NULL, NULL, FALSE, DEBUG_PROCESS, NULL, NULL, &si, &pi))	// could not create process
		return;	

	dbg_pause = false;
	dbg_continue = true;
	dbg_state = State::RUNNING;
	dbg_stop = false;
	dbg_stepping = false;

	CONTEXT context;
	context.ContextFlags = CONTEXT_ALL;
	bool exit;
	while (true)
	{
		if (WaitForDebugEvent(&debug_event, (DWORD)WAIT_TIME))
		{
			continue_status = DBG_CONTINUE;
			dbg_state = State::PAUSED;

			switch (debug_event.dwDebugEventCode)
			{
			case EXCEPTION_DEBUG_EVENT:				
				switch (debug_event.u.Exception.ExceptionRecord.ExceptionCode)
				{
				case EXCEPTION_BREAKPOINT: 
					process_breakpoint(debug_event.dwThreadId);
					dbg_continue = breakpoint_clbk(dbg_context);					
					break;
					/*
				case EXCEPTION_ACCESS_VIOLATION:
					break;
				case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
					break;
				case EXCEPTION_DATATYPE_MISALIGNMENT:
					break;
				case EXCEPTION_FLT_DENORMAL_OPERAND:
					break;
				case EXCEPTION_FLT_DIVIDE_BY_ZERO:
					break;
				case EXCEPTION_FLT_INEXACT_RESULT:
					break;
				case EXCEPTION_FLT_INVALID_OPERATION:
					break;
				case EXCEPTION_FLT_OVERFLOW:
					break;
				case EXCEPTION_FLT_STACK_CHECK:
					break;
				case EXCEPTION_FLT_UNDERFLOW:
					break;
				case EXCEPTION_ILLEGAL_INSTRUCTION:
					break;
				case EXCEPTION_IN_PAGE_ERROR:
					break;
				case EXCEPTION_INT_DIVIDE_BY_ZERO:
					break;
				case EXCEPTION_INT_OVERFLOW:
					break;
				case EXCEPTION_INVALID_DISPOSITION:
					break;
				case EXCEPTION_NONCONTINUABLE_EXCEPTION:
					break;
				case EXCEPTION_PRIV_INSTRUCTION:
					break;
				case EXCEPTION_SINGLE_STEP:
					std::cout << "ess" << std::endl;
					break;
				case EXCEPTION_STACK_OVERFLOW:
					break;
					*/
				default: 
					process_other_exceptions(debug_event);
					break;
				}				
				break;
			case CREATE_THREAD_DEBUG_EVENT:
				process_create_thread(debug_event);
				create_thread_clbk(dbg_context); 
				break;
			case CREATE_PROCESS_DEBUG_EVENT:
				process_create_process(debug_event, pause);
				create_process_clbk(dbg_context);
				GetThreadContext(debug_event.u.CreateProcessInfo.hThread, &main_thread_context);
				break;
			case EXIT_THREAD_DEBUG_EVENT:
				process_exit_thread(debug_event);
				exit_thread_clbk(dbg_context); 				
				break;
			case EXIT_PROCESS_DEBUG_EVENT:
				exit = process_exit_process(debug_event);
				exit_process_clbk(dbg_context); 
				if (exit)
					return;
			case LOAD_DLL_DEBUG_EVENT:
				process_load_dll(debug_event.u.LoadDll);
				load_dll_clbk(dbg_context); 
				break;
			case UNLOAD_DLL_DEBUG_EVENT:
				process_unload_dll(debug_event.u.UnloadDll);
				unload_dll_clbk(dbg_context); 
				break;
			case OUTPUT_DEBUG_STRING_EVENT:
				process_debugstring(debug_event);
				debug_string_clbk(dbg_context); break;
			default:
				break;
			}
		}	

		while (!dbg_continue)	// if not continue than sleep and try again
			std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
		
		if (dbg_stepping)	// stepping over isntructions
		{
			context = get_context(debug_event.dwThreadId);
			context.EFlags |= 0x100;	// set trap flag
			set_context(debug_event.dwThreadId, context);
		}

		if (dbg_pause)	// if want pause the debugger on next instruction
		{
			if (dbg_stop)
			{
				TerminateProcess(dbg_context.main_process, dbg_context.last_exit_code);
				DebugActiveProcessStop(dbg_context.main_process_id);
				/*
				for (const auto &number_handle : dbg_context.threads)
					TerminateThread(number_handle.second, 1);

				GetExitCodeProcess(dbg_context.main_process, &dbg_context.last_exit_code);
				TerminateProcess(dbg_context.main_process, dbg_context.last_exit_code);*/
				exit_process_clbk(dbg_context);
				process_exit_process(debug_event);
				break;
			}

			old_byte = toggle_breakpoint(main_thread_context.Rip);
			dbg_pause = false;
		}

		dbg_state = State::RUNNING;
		ContinueDebugEvent(debug_event.dwProcessId, debug_event.dwThreadId, continue_status);
	}
	
	dbg_state = State::STOPPED;
}

BYTE Dbg::Debugger::toggle_breakpoint(std::size_t offset)
{
	BYTE old_byte;
	BYTE bytes[10];
	ReadProcessMemory(dbg_context.main_process, (LPCVOID)(dbg_context.base_address + offset), &bytes, 10*sizeof(old_byte), NULL);
	old_byte = bytes[0];
	WriteProcessMemory(dbg_context.main_process, (LPVOID)(dbg_context.base_address + offset), &BREAKPOINT, sizeof(BREAKPOINT), NULL);
	FlushInstructionCache(dbg_context.main_process, (LPCVOID)(dbg_context.base_address + offset), sizeof(old_byte));

	return old_byte;
}

void Dbg::Debugger::untoggle_breakpoint(std::size_t offset, BYTE old_byte)
{
	WriteProcessMemory(dbg_context.main_process, (LPVOID)(dbg_context.base_address + offset), &old_byte, sizeof(old_byte), NULL);
	FlushInstructionCache(dbg_context.main_process, (LPCVOID)(dbg_context.base_address + offset), sizeof(old_byte));
}

void Dbg::Debugger::add_breakpoint(std::size_t offset)
{
	BYTE old_byte = toggle_breakpoint(offset);
	breakpoints.insert(std::make_pair(offset, old_byte));
}

bool Dbg::Debugger::remove_breakpoint(std::size_t offset)
{
	auto it = breakpoints.find(offset);
	if (it == end(breakpoints))
		return false;
	
	BYTE old_byte = it->second;
	breakpoints.erase(it);
	untoggle_breakpoint(offset, old_byte);
	
	return true;
}

void Dbg::Debugger::process_create_process(const DEBUG_EVENT &debug_event, bool pause)
{
	const BYTE BYTE_SIZE = 4;
	auto debug_info = debug_event.u.CreateProcessInfo;

	if (processes.empty())
	{
		DWORD64 base = reinterpret_cast<DWORD64>(debug_info.lpBaseOfImage);
		dbg_context.base_address = base;

		MemoryReader::base = base;
		MemoryReader::process = debug_info.hProcess;
		PEMemoryUndecorator pemu(true);

		dbg_context.main_process_id = debug_event.dwProcessId;
		dbg_context.entry_point_address = pemu.optheader().AddressOfEntryPoint;
		dbg_context.main_process = debug_info.hProcess;
		dbg_context.main_thread = debug_info.hThread;
		dbg_context.threads.insert(std::make_pair(debug_event.dwThreadId, dbg_context.main_thread));

		if (pause)
			add_breakpoint(dbg_context.entry_point_address);
	}

	processes.emplace(debug_event.dwProcessId, debug_info.hProcess);
}

void Dbg::Debugger::reset()
{
	dbg_context.threads.clear();	// clear threads
	dbg_context.loaded_dlls.clear();
	dbg_context.breakpoints.clear();
}

void Dbg::Debugger::process_other_exceptions(const DEBUG_EVENT &de)
{
	if (dbg_stepping)
	{
		CONTEXT c = get_context(de.dwThreadId);
		dbg_context.fill(c);

		dbg_continue = breakpoint_clbk(dbg_context);
		return;
	}

	if (breakpoints.find((size_t)de.u.Exception.ExceptionRecord.ExceptionAddress - dbg_context.base_address) != end(breakpoints))
	{
		process_breakpoint(de.dwThreadId);
		dbg_continue = breakpoint_clbk(dbg_context);
		return;
	}

	std::cout << "Exception in thread: " << de.dwThreadId << " on address: " << de.u.Exception.ExceptionRecord.ExceptionAddress << std::endl;
	continue_status = DBG_EXCEPTION_NOT_HANDLED;
}

void Dbg::Debugger::process_breakpoint(DWORD thread_id)
{
	CONTEXT context = get_context(thread_id);
	--context.Rip;
	dbg_context.instruction_offset = context.Rip;
	if (remove_breakpoint(context.Rip - dbg_context.base_address))	// its users breakpoint
	{
		set_context(thread_id, context);
		dbg_context.user_breakpoint = true;
	}
	else
		dbg_context.user_breakpoint = false;

	dbg_context.fill(context);
	dbg_context.curr_thread = thread_id;
}

void Dbg::Debugger::process_load_dll(const LOAD_DLL_DEBUG_INFO &load_dll_info)
{
	dbg_context.last_dll = load_dll_info.lpBaseOfDll;
	dbg_context.loaded_dlls.insert(dbg_context.last_dll);
}

void Dbg::Debugger::process_unload_dll(const UNLOAD_DLL_DEBUG_INFO &unload_dll_info)
{
	dbg_context.last_dll = unload_dll_info.lpBaseOfDll;
	dbg_context.loaded_dlls.erase(dbg_context.last_dll);
}

void Dbg::Debugger::process_create_thread(const DEBUG_EVENT &de)
{
	dbg_context.curr_thread = de.dwThreadId;
	dbg_context.threads.insert(std::make_pair(dbg_context.curr_thread, de.u.CreateThread.hThread));
	get_thread_context(de.dwThreadId);
}

void Dbg::Debugger::process_exit_thread(const DEBUG_EVENT &de)
{
	dbg_context.curr_thread = de.dwThreadId;
	dbg_context.threads.erase(dbg_context.curr_thread);
	dbg_context.last_exit_code = de.u.ExitThread.dwExitCode;
}

bool Dbg::Debugger::process_exit_process(const DEBUG_EVENT &de)
{
	dbg_context.curr_process = de.dwProcessId;
	dbg_context.last_exit_code = de.u.ExitProcess.dwExitCode;
	processes.erase(de.dwProcessId);
	if (processes.empty())
	{
		dbg_state = State::STOPPED;
		reset();
		return true;
	}

	return false;
}

void Dbg::Debugger::process_debugstring(const DEBUG_EVENT &de)
{
	auto info = de.u.DebugString;
	if (!info.fUnicode)
	{
		char *buff = new char[info.nDebugStringLength];
		ReadProcessMemory(process_handle(de.dwProcessId), info.lpDebugStringData, buff, info.nDebugStringLength, nullptr);
		dbg_context.debug_string = buff;
		delete[] buff;
	}	
}

const Dbg::Debugger::Context &Dbg::Debugger::get_thread_context(size_t thread)
{
	auto found = dbg_context.threads.find(thread);
	if (found == end(dbg_context.threads))
		return dbg_context;

	CONTEXT context;
	context.ContextFlags = CONTEXT_ALL;
	GetThreadContext(found->second, &context);
	dbg_context.fill(context); 
	return dbg_context;
}

CONTEXT Dbg::Debugger::get_context(DWORD threadid)
{
	auto found = dbg_context.threads.find(threadid);
	CONTEXT c;
	c.ContextFlags = CONTEXT_ALL;
	if (found != end(dbg_context.threads))
		GetThreadContext(found->second, &c);
	return c;
}

void Dbg::Debugger::set_context(DWORD threadid, const CONTEXT &c)
{
	auto found = dbg_context.threads.find(threadid);
	if (found == end(dbg_context.threads))
		return;

	SetThreadContext(found->second, &c);
}

Dbg::Debugger::breakpoint_clbk_t Dbg::Debugger::empty_breakpoint_clbk = [](const Dbg::Debugger::Context &context)->bool{ return true; };	// must return false because of dbg_continue
Dbg::Debugger::create_thread_clbk_t Dbg::Debugger::empty_create_thread_clbk = [](const Dbg::Debugger::Context &){};
Dbg::Debugger::create_process_clbk_t Dbg::Debugger::empty_create_process_clbk = [](const Dbg::Debugger::Context &){};
Dbg::Debugger::exit_thread_clbk_t Dbg::Debugger::empty_exit_thread_clbk = [](const Dbg::Debugger::Context &){};
Dbg::Debugger::exit_process_clbk_t Dbg::Debugger::empty_exit_process_clbk = [](const Dbg::Debugger::Context &){};
Dbg::Debugger::load_dll_clbk_t Dbg::Debugger::empty_load_dll_clbk = [](const Dbg::Debugger::Context &){};
Dbg::Debugger::unload_dll_clbk_t Dbg::Debugger::empty_unload_dll_clbk = [](const Dbg::Debugger::Context  &){};
Dbg::Debugger::debug_string_clbk_t Dbg::Debugger::empty_debug_string_clbk = [](const Dbg::Debugger::Context &){};