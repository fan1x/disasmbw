#pragma once

#include <unordered_map>
#include <map>
#include <string>
#include <vector>
#include <utility>
#include <bitset>
#include <unordered_set>
#include <set>

// stores graph of call
class Graph
{
public:
	typedef size_t vertex_t;
	typedef std::pair<vertex_t, vertex_t > edge_t;
	typedef std::vector<vertex_t >::const_iterator edges_iterator_t;
	typedef std::pair<size_t, char > faddress_t;	// <vertex, kind>, 1 = label, 2 = jmp, 3 = both
	typedef std::pair<faddress_t, faddress_t > faddresses_t;	// addresses in file
	typedef std::pair<vertex_t, faddress_t > block_t;
	
	// return iterators to successors [first, second) for given vertex
	inline std::pair<edges_iterator_t, edges_iterator_t > succ(const vertex_t &vertex)
	{
		vertex_t block_end = blocks.ebasm(vertex);	// find end of block where is vertex placed
		auto it = edges.find(block_end);	// find block

		if (it == end(edges))	// block split only as target of jump
		{
			blocks.asm_insert(block_end, { 0, 0x1 | 0x2 });	// block_end exists -> mark as start and end
			insert(block_end, block_end);
			it = edges.find(block_end);
		}	

		return std::make_pair(std::begin(it->second), std::end(it->second));
	}

	// set block address for labels
	inline void set_faddress(vertex_t v, size_t address)
	{
		blocks.asm_insert(v, std::make_pair(address, 1));
	}

	// inserts no edges from address
	inline void insert_empty_start(vertex_t address, size_t faddress)
	{
		blocks.asm_insert(address, std::make_pair(faddress, 2));
		edges.insert({ address, {} });	// insert no edges
	}

	// insert into blocks
	inline void insert_empty_end(vertex_t address, size_t faddress)
	{
		blocks.asm_insert(address, std::make_pair(faddress, 1));
	}

	// insert into blocks and also creates edges
	void insert(vertex_t start_vertex, size_t start_faddress, vertex_t end_vertex, size_t end_faddress);
	// insert into blocks and create edge
	void insert(vertex_t start_vertex, size_t start_faddress, vertex_t end_vertex);
	// return start of block where is start placed
	inline vertex_t get_start()
	{
		return start;
	}
	
	// set starting point of starting analysis
	inline void set_start(size_t start)
	{
		Graph::start = start;
	}
	// <start, end> of block in fstart_end
	// return false if block_start not found, than returns blocks that contains it
	inline bool get_fblock(vertex_t block_start, std::pair<faddress_t, faddress_t > &fstart_end, bool change = true)
	{
		if (!inrange(block_start))
		{
			fstart_end = { { 0, 1 }, { 0, 1 } };
			return false;
		}

		if (!change)
			return const_cast<const Block &>(blocks).get_fblock(block_start, fstart_end);	// call const method

		return blocks.get_fblock(block_start, fstart_end);
	}
	// is address in range
	inline bool inrange(size_t address) const
	{
		return blocks.inrange(address);
	}
	// return ranges in file of next_block
	inline faddresses_t next_block()
	{
		return blocks.next_fblock();
	}
	// return range in file of previous block
	inline faddresses_t previous_block()
	{
		return blocks.previous_fblock();
	}

	
	class Block
	{
	public:
		Block():
			asm_block(true)
		{}

		// insert into hlasm blocks
		inline void hlasm_insert(Graph::vertex_t v, const Graph::faddress_t &addr)
		{
			hlasmaddresses.emplace(v, hlasmblocks.size());
			hlasmblocks.emplace_back(addr);
		}
		// insert into asm blocks
		inline void asm_insert(Graph::vertex_t v, const Graph::faddress_t &addr)
		{
			auto it = asmblocks.find(v);
			if (it != end(asmblocks))
			{
				it->second.second |= addr.second;
				return;
			}

			asmblocks.insert({ v, addr });
		}

		// switch between given blocks
		void switch_block()
		{
			asm_block = !asm_block;
		}
		// is using asmblock
		bool using_asmblock() const
		{
			return asm_block;
		}
		// begin block address
		inline vertex_t bbasm(vertex_t address) const
		{
			auto start = --asmblocks.upper_bound(address);
			return start->first;
		}
		// end block address
		inline vertex_t ebasm(vertex_t address) const
		{
			return asmblocks.upper_bound(address)->first;
		}
		// return range of address in file
		inline bool get_fblock(vertex_t address, faddresses_t &fblock)
		{
			if (asm_block)
			{
				auto start = asmblocks.upper_bound(address);
				bool res = start->first == address;
				auto end = start--;
				last_asmblock = start;

				fblock = std::move(std::make_pair(start->second, end->second));
				return res;
			}

			bool res = true;
			auto start = hlasmaddresses.lower_bound(address);
			if (start->first != address)	// found address block after 
			{
				--start;
				res = false;
			}
				
			last_hlasmblock = start->second;
			auto bblock = hlasmblocks.at(start->second);
			auto eblock = hlasmblocks.at(start->second + 1);

			fblock = std::move(std::make_pair(bblock, eblock));
			return res;
		}
		// return range of address in file
		inline bool get_fblock(vertex_t address, faddresses_t &fblock) const
		{
			if (asm_block)
			{
				auto start = asmblocks.upper_bound(address);
				auto end = start--;
				bool res = start->first == address;

				fblock = std::move(std::make_pair(start->second, end->second));
				return res;
			}

			bool res = true;
			auto start = hlasmaddresses.lower_bound(address);
			if (start->first != address)	// found address block after 
			{
				--start;
				res = false;
			}

			auto bblock = hlasmblocks.at(start->second);
			auto eblock = hlasmblocks.at(start->second + 1);

			fblock = std::move(std::make_pair(bblock, eblock));
			return res;
		}
		//returns addresses in file of next block
		inline faddresses_t next_fblock()
		{
			if (asm_block)
			{
				auto new_end = last_asmblock;
				++++new_end;	// +2
				if (new_end == cend(asmblocks))	// at the end
					--new_end;
				else
					++last_asmblock;

				return std::move(std::make_pair(last_asmblock->second, new_end->second));
			}

			if (last_hlasmblock + 2 < hlasmblocks.size())	// we are at the end
				++last_hlasmblock;

			return std::move(std::make_pair(hlasmblocks.at(last_hlasmblock), hlasmblocks.at(last_hlasmblock+1)));
		}
		// returns addresses in file of previous block
		inline faddresses_t previous_fblock()
		{
			if (asm_block)
			{
				if (last_asmblock != cbegin(asmblocks))
					--last_asmblock;

				auto end = last_asmblock;
				return std::move(std::make_pair(last_asmblock->second, (++end)->second));
			}

			if (last_hlasmblock > 0)	// not begin
				--last_hlasmblock;

			return std::move(std::make_pair(hlasmblocks.at(last_hlasmblock), hlasmblocks.at(last_hlasmblock + 1)));
		}
		// is address in range
		inline bool inrange(vertex_t address) const
		{
			return cbegin(asmblocks)->first <= address && address <= crbegin(asmblocks)->first;
		}

	private:
		typedef std::map<vertex_t, faddress_t > block_t;
		
		block_t::const_iterator last_asmblock;
		size_t last_hlasmblock;	// index to hlasmblocks

		std::vector<faddress_t > hlasmblocks;
		block_t asmblocks;
		std::map<vertex_t, size_t > hlasmaddresses;

		volatile bool asm_block;
	} blocks;
private:
	// insert edge
	inline void insert(vertex_t start_vertex, vertex_t end_vertex)
	{
		auto e = edges.find(start_vertex);
		if (e == end(edges))
			edges.insert({ start_vertex, { end_vertex } });
		else
			e->second.push_back(end_vertex);
	}

	// block represents vertex, second parameter is its address in file
	// map[vertex, <faddress, start>]	
	std::unordered_map<vertex_t, std::vector<vertex_t > > edges;
	vertex_t start;
};

class CodeAnalyser
{
public:
	CodeAnalyser(const std::string &input = "program.asm", const std::string &output = "program.hlasm");
	~CodeAnalyser();

	// load program from asm
	void load(size_t start);
	// run analysis
	void analyse();

	inline std::pair<Graph::faddress_t, Graph::faddress_t > address_block(size_t address)
	{
		std::pair<Graph::faddress_t, Graph::faddress_t > res;
		graph.get_fblock(address, res);
		return std::move(res);
	}
	inline Graph::faddresses_t next_block()
	{
		return std::move(graph.next_block());
	}
	inline Graph::faddresses_t previous_block()
	{
		return std::move(graph.previous_block());
	}
	inline bool inrange(size_t address) const
	{
		return graph.inrange(address);
	}

	inline void kill()
	{
		exit = true;
	}

	inline void swich_block()
	{
		graph.blocks.switch_block();
	}

	inline bool asm_block() const
	{
		return graph.blocks.using_asmblock();
	}
private:
	// cycle
	typedef Graph::vertex_t cycle_t;
	// <nontaken_jmp_address, taken_jmp_address>
	typedef std::pair<Graph::vertex_t, Graph::vertex_t > jmps_t;
	// [relative address, direct jump, equal, less, above]
	typedef std::bitset<7> flags_t;	

	// process cycle (first, ..., last, first)
	void process_cycle(Graph::vertex_t first, Graph::vertex_t last, const std::map<Graph::vertex_t, Graph::vertex_t > &parent);
	void process_ifstatement(Graph::vertex_t vertex, Graph::vertex_t jmp1, Graph::vertex_t jmp2);

	// return 1 = jmp, 2 = call, 3 = ret, 4 = test instruction
	int CodeAnalyser::found(const std::string &line, size_t &address, long long &offset, bool &cond);
	// if not found jmp, returns address in edge.first
	bool found_jmp(const std::string &name, const std::string &address, long long &offset, bool &cond);
	bool found_call(const std::string &name, const std::string &address, long long &offset) const;
	bool found_ret(const std::string &line) const;
	bool found_test(const std::string &name, const std::string &op1, const std::string &op2);
	

	// prints block [start, finish)
	void print_block(std::unordered_set<size_t > &functions, std::unordered_set<size_t > &printed_functions, std::ofstream &ofs, std::ifstream &ifs, const std::pair<Graph::faddress_t, Graph::faddress_t > &fstart_end);
	
	Graph graph;
	std::unordered_map<size_t, size_t > found_calls;
	// saves last edge from cycle
	std::map<Graph::vertex_t, cycle_t> cycles;
	std::map<Graph::vertex_t, jmps_t > if_statements;
	// map<name, flags>
	std::unordered_map<std::string, flags_t > jmps, calls;
	std::unordered_set<std::string > rets;
	std::unordered_set<std::string > tests;

	std::pair<std::string, std::string > test_operands;	// last test operands
	bool test_instruction;	// test instruction
	flags_t jmp_flags;	// last jmp flags

	bool exit;
	std::string ifile, ofile;
	size_t startpoint;	// startpoint from which is starts addresses
};

