#pragma once

#include "Debugger.h"
#include "Translator.h"

#include <utility>
#include <vector>
#include <string>

typedef std::string instruction_t;
typedef std::pair<size_t, instruction_t > line_t;
typedef std::vector<line_t >::const_iterator line_it_t;

class UI
{
public:
	UI();
	virtual ~UI();
	virtual void run() = 0;	// is used to run program

	virtual bool process_breakpoint(const Dbg::Debugger::Context &) = 0;
	virtual void process_unloaddll(const Dbg::Debugger::Context &) = 0;
	virtual void process_loaddll(const Dbg::Debugger::Context &) = 0;
	virtual void process_create_thread(const Dbg::Debugger::Context &) = 0;
	virtual void process_exit_thread(const Dbg::Debugger::Context &) = 0;
	virtual void process_create_process(const Dbg::Debugger::Context &) = 0;
	virtual void process_exit_process(const Dbg::Debugger::Context &) = 0;
	virtual void process_debug_string(const Dbg::Debugger::Context &) = 0;
	virtual void process_hlanalysis() = 0;
protected:
	Translator translator;
	Dbg::Debugger dbg;	
};



