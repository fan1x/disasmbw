#include <iostream>
#include <string>
#include <thread>
#include <memory>
#include <fstream>

#include "Debugger.h"
#include "ConsoleUI.h"

#include "Translator.h"

#include "../Dictionary/Dictionary.h"

using namespace Dbg;

const char* instruction_file = ".\\..\\instructions.xml";
const char* format_file = ".\\..\\instruction_format.xml";

int main()
{
	std::unique_ptr<UI> ui(new ConsoleUI());
	ui->run();
}