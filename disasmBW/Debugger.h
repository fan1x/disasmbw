#pragma once

#include <Windows.h>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <map>

// forward declaration
namespace Dbg
{
	class Debugger
	{
	public:
		enum State { RUNNING, PAUSED, STOPPED };

		typedef struct _Context
		{
			_Context();
			void fill(const CONTEXT &c);

			size_t instruction_offset;
			HANDLE main_process;
			DWORD main_process_id;
			HANDLE main_thread;
			size_t base_address;
			size_t entry_point_address;
			struct
			{
				size_t rip;
				size_t rax;
				size_t rcx;
				size_t rdx;
				size_t rbx;
				size_t rsp;
				size_t rbp;
				size_t rsi;
				size_t rdi;
				size_t r8;
				size_t r9;
				size_t r10;
				size_t r11;
				size_t r12;
				size_t r13;
				size_t r14;
				size_t r15;
			} registers;
			bool user_breakpoint;
			std::string debug_string;

			LPVOID last_dll;
			DWORD curr_thread;	// current thread
			DWORD curr_process;	// current process
			DWORD last_exit_code;
			std::unordered_set<LPVOID > loaded_dlls;
			std::map<size_t, HANDLE > threads;
			std::unordered_set<size_t > breakpoints;	// address of toggled breakpoint
		} Context;	// context of whole debugger

		typedef std::function<bool(const Context &)> breakpoint_clbk_t;	// function is called if breakpoint occurs
		typedef std::function<void(const Context &)> create_thread_clbk_t;
		typedef std::function<void(const Context &)> create_process_clbk_t;
		typedef std::function<void(const Context &)> exit_thread_clbk_t;
		typedef std::function<void(const Context &)> exit_process_clbk_t;
		typedef std::function<void(const Context &)> load_dll_clbk_t;
		typedef std::function<void(const Context &)> unload_dll_clbk_t;
		typedef std::function<void(const Context &)> debug_string_clbk_t;

		static breakpoint_clbk_t empty_breakpoint_clbk;// = [](EXCEPTION_RECORD &){ return true; };	// must return true because of dbg_continue
		static create_thread_clbk_t empty_create_thread_clbk;// = [](CREATE_THREAD_DEBUG_INFO &){};
		static create_process_clbk_t empty_create_process_clbk;// = [](CREATE_PROCESS_DEBUG_INFO &){};
		static exit_thread_clbk_t empty_exit_thread_clbk;// = [](EXIT_THREAD_DEBUG_INFO &){};
		static exit_process_clbk_t empty_exit_process_clbk;// = [](EXIT_PROCESS_DEBUG_INFO &){};
		static load_dll_clbk_t empty_load_dll_clbk; // = [](LOAD_DLL_DEBUG_INFO &){};
		static unload_dll_clbk_t empty_unload_dll_clbk;// = [](UNLOAD_DLL_DEBUG_INFO &){};
		static debug_string_clbk_t empty_debug_string_clbk;// = [](OUTPUT_DEBUG_STRING_INFO &){};

		Debugger(breakpoint_clbk_t breakpoint_clbk,
			const create_thread_clbk_t &create_thread_clbk,
			const create_process_clbk_t &create_process_clbk,
			const exit_thread_clbk_t &exit_thread_clbk,
			const exit_process_clbk_t &exit_process_clbk,
			const load_dll_clbk_t &load_dll_clbk,
			const unload_dll_clbk_t &unload_dll_clbk,
			const debug_string_clbk_t &debug_string_clbk);
		Debugger();
		
		~Debugger();

		void run(const std::string &program_name, bool pause = false);

		// resume debugging
		inline void start()
		{
			dbg_stepping = false;
			dbg_continue = true;
		}
		
		inline void pause()
		{ 
			dbg_pause = true;
		}

		inline void next()
		{
			dbg_stepping = true;
			dbg_continue = true;
		}

		inline State state()
		{
			return dbg_state;
		}

		inline const Context &context()
		{
			return dbg_context;
		}

		inline void stop()
		{
			// need this order
			dbg_stop = true;
			dbg_pause = true;
			dbg_continue = true;
		}

		const Context &get_thread_context(size_t thread);

		void add_breakpoint(std::size_t offset);	
		bool remove_breakpoint(std::size_t offset);
	private:
		typedef std::unordered_map<std::size_t, BYTE > breakpoint_map_t;

		const int WAIT_TIME = 100;
		const BYTE BREAKPOINT = 0xCC;

		BYTE toggle_breakpoint(std::size_t offset);
		void untoggle_breakpoint(std::size_t offset, BYTE old_byte);
		CONTEXT get_context(DWORD threadid);
		void set_context(DWORD threadid, const CONTEXT &c);
		void reset();
		
		void process_create_process(const DEBUG_EVENT &devent, bool pause);
		void process_breakpoint(DWORD thread_id);
		void process_load_dll(const LOAD_DLL_DEBUG_INFO &load_dll_info);
		void process_unload_dll(const UNLOAD_DLL_DEBUG_INFO &unload_dll_info);
		void process_create_thread(const DEBUG_EVENT &devent);
		void process_exit_thread(const DEBUG_EVENT &devent);
		// returns false if no process running
		bool process_exit_process(const DEBUG_EVENT &devent);
		void process_other_exceptions(const DEBUG_EVENT &);
		void process_debugstring(const DEBUG_EVENT &de);

		inline HANDLE process_handle(DWORD id) const
		{
			return processes.find(id)->second;
		}

		breakpoint_clbk_t breakpoint_clbk;
		create_thread_clbk_t create_thread_clbk;
		create_process_clbk_t create_process_clbk;
		exit_thread_clbk_t exit_thread_clbk;
		exit_process_clbk_t exit_process_clbk;
		load_dll_clbk_t load_dll_clbk;
		unload_dll_clbk_t unload_dll_clbk;
		debug_string_clbk_t debug_string_clbk;

		DWORD continue_status;
		breakpoint_map_t breakpoints;		
		State dbg_state;	// debugger state
		volatile bool dbg_stop;	// set to true, if stop
		volatile bool dbg_pause;	// set to true if pause needed
		volatile bool dbg_continue;	// set to true if start
		volatile bool dbg_stepping;	// set to true if stepping
		volatile bool dbg_context_ready;	// set to true, if context structure is ready for 
		Context dbg_context;	// context of debugger		
		std::unordered_map<DWORD, HANDLE > processes;	// running processes
	};
}