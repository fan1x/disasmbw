#include "PEUndecorator.h"

#include <exception>
#include <stdexcept>
#include <algorithm>
#include <fstream>


size_t FileReader::base;
std::string FileReader::file_name;
std::ifstream FileReader::input;

void FileReader::get(char *buffer, size_t start, size_t buffer_size)
{
	auto issfs = input.good();
	input.seekg(start);
	input.read(buffer, buffer_size);
}

size_t MemoryReader::base;
HANDLE MemoryReader::process;

void MemoryReader::get(char *buffer, size_t start, size_t buffer_size)
{
	ReadProcessMemory(process, reinterpret_cast<LPCVOID>(start), buffer, buffer_size, NULL);
}