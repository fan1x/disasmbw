#include "UI.h"

#include <random>
#include <chrono>
#include <algorithm>


UI::UI() :
dbg([&](const Dbg::Debugger::Context &c) { return process_breakpoint(c); },
[&](const Dbg::Debugger::Context &c) {	process_create_thread(c); },
[&](const Dbg::Debugger::Context &c) {	process_create_process(c); },
[&](const Dbg::Debugger::Context &c) { 	process_exit_thread(c); },
[&](const Dbg::Debugger::Context &c) { 	process_exit_process(c); },
[&](const Dbg::Debugger::Context &c) {	process_loaddll(c); },
[&](const Dbg::Debugger::Context &c) {	process_unloaddll(c); },
[&](const Dbg::Debugger::Context &c) {	process_debug_string(c); })
{
}

UI::~UI()
{
}

