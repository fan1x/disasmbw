#pragma once

#include <windows.h>
#include <ImageHlp.h>
#include <winnt.h>

#include <string>
#include <fstream>

// undecorator of PE file or PE in memory
template<class T>
class PEUndecorator
{
public:
	enum { ExReadProcessMemory, ExUnknownFile };	// exceptions types

	static const size_t BUFFER_SIZE = 0x1;
	typedef char buffer_t[BUFFER_SIZE];

	PEUndecorator()
	{}

	PEUndecorator(bool)	// fake parameter to resolve ctors
	{
		T::open();
		T::get((char*)&dos_header, T::base, sizeof(dos_header));
		T::get((char*)&signature, T::base + dos_header.e_lfanew, sizeof(signature));
		T::get((char*)&file_header, T::base + dos_header.e_lfanew + sizeof(signature), sizeof(file_header));
		T::get((char*)&optional_header,T::base + dos_header.e_lfanew + sizeof(file_header) + sizeof(signature), sizeof(optional_header));

		curr = T::base + optional_header.BaseOfCode / 4;
	}

	const PEUndecorator<T> &operator=(PEUndecorator<T> &peu)
	{
		dos_header = peu.dos_header;
		file_header = peu.file_header;
		optional_header = peu.optional_header;

		curr = peu.curr;
		peu.copied = true;	// set so its not closed by destructor
		return *this;
	}

	~PEUndecorator() 
	{
		if (!copied)
			T::close();	
	};

	// returns buffer filled with instructions
	buffer_t &operator>>(buffer_t &buffer)
	{
		T::get(buffer, curr, BUFFER_SIZE);
		curr += BUFFER_SIZE;

		return buffer;
	}

	inline bool good() const
	{
		return curr <= optional_header.SizeOfCode + optional_header.BaseOfCode/4;
	}
	inline void reset()
	{
		curr = optional_header.BaseOfCode;
	}
	inline const IMAGE_DOS_HEADER &dheader() const
	{
		return dos_header;
	}
	inline const IMAGE_FILE_HEADER &fheader() const
	{
		return file_header;
	}
	inline const IMAGE_OPTIONAL_HEADER &optheader() const
	{
		return optional_header;
	}
	inline bool pe() const
	{
		return signature == "PE";
	}
private:
	IMAGE_DOS_HEADER dos_header;
	IMAGE_FILE_HEADER file_header;	
	IMAGE_OPTIONAL_HEADER optional_header;
	char signature[4];

	size_t curr;
	bool copied;

	const char BYTE_SIZE = 4 * sizeof(char);
	const char MAGIC_NUMBER_SIZE = 2;
};

// trait for PE in file
struct FileReader
{
	static size_t base;
	static std::string file_name;
	static void get(char *buffer, size_t start, size_t buffer_size);
	inline static void close()
	{
		input.close();
	}
	inline static void open()
	{
		input.open(file_name, std::ios::in | std::ios::binary);
	}

	static std::ifstream input;
};

// trait for PE in memory
struct MemoryReader
{
	static size_t base;
	static HANDLE process;
	static void get(char *buffer, size_t start, size_t buffer_size);
	inline static void close() {}
	inline static void open() {}
};

typedef PEUndecorator<FileReader> PEFileUndecorator;
typedef PEUndecorator<MemoryReader> PEMemoryUndecorator;

