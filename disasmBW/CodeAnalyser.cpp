#include "CodeAnalyser.h"
#include "utils.h"

#include <stack>
#include <set>
#include <map>
#include <fstream>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <cassert>

using std::ifstream;
using std::ofstream;
using std::hex;
using std::endl;
using std::map;
using std::stack;
using std::vector;
using std::set;
using std::string;
using std::getline;
using std::unordered_map;
using std::make_pair;
using std::pair;

#define LABEL ("LABEL_")
#define LBRACKET ('(')
#define RBRACKET (')')
#define TRUE ("true")
#define IF ("IF")
#define SPACE (' ')
#define GOTO ("GOTO")
#define TAB ('\t')
#define LOOP ("LOOP START")
#define COLON (':')
#define LINE ("--------------------")
#define FUNCTION ("FUNCTION")
#define AND ('&')
#define ZERO ('0')

#define OP_A (68)	//static const size_t OP_A =  flags_t("10001").to_ulong();	// >
#define OP_B (72)	//static const size_t OP_B =  flags_t("10010").to_ulong();	// <
#define OP_NE (76)	//static const size_t OP_NE = flags_t("10011").to_ulong();	// !=
#define OP_E (80)	//static const size_t OP_E =  flags_t("10100").to_ulong();	// ==
#define OP_AE (84)	//static const size_t OP_AE = flags_t("10101").to_ulong();	// >=
#define OP_BE (88)	//static const size_t OP_BE = flags_t("10110").to_ulong();	// <=

// Graph

void Graph::insert(vertex_t start_vertex, size_t start_faddress, vertex_t end_vertex, size_t end_faddress)
{
	blocks.asm_insert(start_vertex, make_pair(start_faddress, 2));
	blocks.asm_insert(end_vertex, make_pair(end_faddress, 1));

	auto jt = edges.find(start_vertex);
	if (jt == end(edges))
		edges.insert({ start_vertex, { end_vertex } });
	else
		jt->second.push_back(end_vertex);
}

void Graph::insert(vertex_t start_vertex, size_t start_faddress, vertex_t end_vertex)
{
	blocks.asm_insert(start_vertex, make_pair(start_faddress, 2));

	auto it = edges.find(start_vertex);
	if (it == end(edges))
		edges.insert({ start_vertex, { end_vertex } });
	else
		it->second.push_back(end_vertex);
}

// CodeAnalyser

// [relative_address, direct_jump, equal, less, above, registerbit, registerbit ]
// found_jmp, print_cycle, print_ifstmnt, operator defines use exact bits to jmp
typedef std::bitset<7> flags_t;	

CodeAnalyser::CodeAnalyser(const std::string &input, const std::string &output):
ifile(input), ofile(output), exit(false),
jmps
({
	{ "JMP",   flags_t("0100000") },
	{ "JA",    flags_t("1000100") },
	{ "JAE",   flags_t("1010100") },
	{ "JB",    flags_t("1001000") },
	{ "JBE",   flags_t("1011000") },
	{ "JE",    flags_t("1010000") },
	{ "JG",    flags_t("1000100") },
	{ "JGE",   flags_t("1010100") },
	{ "JL",    flags_t("1001000") },
	{ "JLE",   flags_t("1011000") },
	{ "JNE",   flags_t("1001100") },
	{ "JNO",   flags_t("1000000") },	// overflow
	{ "JNP",   flags_t("1000000") },	// parity
	{ "JNS",   flags_t("1000000") },	// sign
	{ "JO",    flags_t("1000000") },	
	{ "JP",    flags_t("1000000") },
	{ "JS",    flags_t("1000000") },
	{ "JZ",    flags_t("1010000") },
	{ "JCXZ",  flags_t("1010001") },	
	{ "JECXZ", flags_t("1010010") },	
	{ "JRCXZ", flags_t("1010011") }	
}), 
rets({ "RET" }),
tests(
	{
		{ "CMP" },
		{ "TEST" }
	}),
calls
(
	{
		{ "CALL", flags_t("01") }
	}
)
{}

CodeAnalyser::~CodeAnalyser()
{
}

int CodeAnalyser::found(const std::string &line, size_t &address, long long &offset, bool &cond)
{
	auto i = line.find(' ');
	string add = line.substr(0, i);
	auto j = line.find(' ', i + 1);
	string instr = line.substr(i + 1, j - i -1);

	auto ops = myutils::str_split(line.substr(j + 1), ",");

	address = std::stoll(add, nullptr, 16);
	if (found_ret(instr))
		return 3;
	if (ops.size() < 1)
		return 0;

	if (found_jmp(instr, ops[0], offset, cond))
		return 1;
	else if (found_call(instr, ops[0], offset))
		return 2;
	else if (ops.size() > 1 && found_test(instr, ops[0], ops[1]))
		return 4;

	return 0;
}

bool CodeAnalyser::found_test(const std::string &name, const std::string &op1, const std::string &op2)
{
	auto it = tests.find(name);
	if (it == cend(tests))
		return false;

	test_instruction = name == "TEST";

	test_operands = make_pair(op1, op2);
	return true;
}

bool CodeAnalyser::found_jmp(const std::string &name, const std::string &address,long long &offset, bool &cond)
{
	auto it = jmps.find(name);
	if (it == end(jmps))
	{
		return false;
	}

	if (address.find(':') != string::npos || address.find('[') != string::npos || address.find("0x") == string::npos)	// not relative
		return false;	

	cond = !it->second.at(5);	// change it, if changed size of flags_t
	offset = myutils::hstosi(address);
	jmp_flags = it->second;
	return true;
}

bool CodeAnalyser::found_ret(const std::string &name) const
{
	auto it = rets.find(name);
	if (it == cend(rets))
		return false;

	return true;
}

bool CodeAnalyser::found_call(const std::string &name, const std::string &address, long long &offset) const
{
	auto it = calls.find(name);
	if (it == cend(calls))
	{
		return false;
	}

	if (address.find(':') != string::npos || address.find('[') != string::npos || address.find("0x") == string::npos)	// not relative
		return false;

	offset = myutils::hstosi(address);
	return true;
}

void CodeAnalyser::load(size_t start)
{
	startpoint = start;
	std::ifstream ifs(ifile);
	bool previous_jmp = false;
	bool previous_call = false;
	size_t previous_address;
	size_t previous_faddress;	// address in file
	long long previous_offset;
	set<size_t > addresses{ start };		// not known addresses found as target of jmp
	unordered_map<size_t, size_t > map_address_faddress;	// mapping all addresses
	bool condition_jmp;
	while (ifs.good())
	{
		string line;
		size_t faddress = ifs.tellg();
		getline(ifs, line);		
		if (line == "")
			continue;
		size_t address;
		long long offset;
		bool cond;
		int f = found(line, address, offset, cond);
		bool fjmp = f == 1;
		bool fcall = f == 2;
		bool fret = f == 3;

		if (address == 0x52af)
		{
			auto i = 2;
			i += 2;
		}
		map_address_faddress[address] = faddress;

		auto it = addresses.find(address);
		if (it != addresses.cend())
		{
			graph.set_faddress(address, faddress);
		}

		if (previous_call)
		{
			size_t cadd = address + previous_offset;
			found_calls.emplace(previous_address, cadd);	
			auto jt = map_address_faddress.find(cadd);
			if (jt == end(map_address_faddress))	// jmp forward
			{
				addresses.insert(cadd);
			}
			else  // jmp back
			{
				graph.set_faddress(cadd, jt->second);
			}
			
			previous_call = false;
		}

		if (previous_jmp)
		{
			size_t jadd = address + previous_offset;
			auto jt = map_address_faddress.find(jadd);
			if (jt == end(map_address_faddress))	// jmp forward
			{
				graph.insert(previous_address, previous_faddress, jadd);	
				addresses.insert(jadd);
			}
			else  // jmp backward
			{
				graph.insert(previous_address, previous_faddress, jadd, jt->second);
			}

			// second stores not taken jmp - on stack is taken first
			if (condition_jmp)
				graph.insert(previous_address, previous_faddress, address, faddress);	// not taken jmp
			else
				graph.insert_empty_end(address, faddress);

			previous_jmp = false;
		}

		if (fret)
		{
			graph.insert_empty_start(address, faddress);
		}

		if (fcall)
		{
			previous_offset = offset;
			previous_call = true;
		}

		if (fjmp)
		{
			previous_offset = offset;
			condition_jmp = cond;
			previous_jmp = true;
		}

		previous_address = address;
		previous_faddress = faddress;
	}

	assert(!previous_jmp);	// nowhere to jmp
	graph.set_faddress(previous_address, previous_faddress);	// add stopper at the end	
	graph.set_start(start);

	ifs.close();
}

inline void print_line(std::ofstream &ofs, const std::string &str)
{
	auto ispace = str.find(' ');
	ofs << TAB << str.substr(0, ispace) << TAB << str.substr(ispace + 1) << endl;
}

inline void print_label(std::ofstream &ofs, Graph::vertex_t address)
{
	ofs << LABEL << hex << address << COLON << endl;
}

inline std::string boperator(const flags_t &jmp_flags)
{
	size_t val = jmp_flags.to_ulong();
	switch (val)
	{
	case OP_E:
		return " == ";
	case OP_A:
		return " > ";
	case OP_B:
		return " < ";
	case OP_AE:
		return " >= ";
	case OP_BE:
		return " <= ";
	case OP_NE:
		return " != ";
	default:
		return " ? ";
	}
}

inline void print_cycle(std::ofstream &ofs, Graph::vertex_t goto_address, const flags_t &jmp_flags, bool test_instruction, const std::pair<std::string, std::string> &operands)
{
	if (test_instruction)
		ofs << TAB << IF << LBRACKET << LBRACKET << operands.first << AND << operands.second << RBRACKET << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (!jmp_flags.at(1) && jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "CX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (jmp_flags.at(1) && !jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "ECX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (jmp_flags.at(1) && jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "RCX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (!jmp_flags.at(5))	// not direct jump
		ofs << TAB << IF << LBRACKET << operands.first << boperator(jmp_flags) << operands.second << RBRACKET << endl;

	ofs << TAB << TAB << GOTO << SPACE << LOOP << SPACE << LABEL << hex << goto_address << endl << endl;
}

inline void print_ifstatement(std::ofstream &ofs, Graph::vertex_t goto_address, const flags_t &jmp_flags, bool test_instruction, const std::pair<std::string, std::string> &operands)
{
	if (test_instruction)
		ofs << TAB << IF << LBRACKET << LBRACKET << operands.first << AND << operands.second << RBRACKET << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (!jmp_flags.at(1) && jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "CX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (jmp_flags.at(1) && !jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "ECX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else if (jmp_flags.at(1) && jmp_flags.at(0))
		ofs << TAB << IF << LBRACKET << "RCX" << boperator(jmp_flags) << ZERO << RBRACKET << endl;
	else
		ofs << TAB << IF << LBRACKET << operands.first << boperator(jmp_flags) << operands.second << RBRACKET << endl;

	ofs << TAB << TAB << GOTO << SPACE << LABEL << hex << goto_address << endl << endl;
}

inline void print_lastin_block(std::ofstream &ofs, std::ifstream &ifs, size_t address)
{
	ifs.seekg(address);
	string line;
	getline(ifs, line);
	auto ispace = line.find(' ');
	ofs << TAB << line.substr(0, ispace) << TAB << line.substr(ispace + 1) <<  endl;
}



inline void print_line(std::ofstream &ofs)
{
	ofs << LINE << endl;
}

inline void print_function(std::ofstream &ofs, size_t address)
{
	ofs << endl << endl << FUNCTION << SPACE << LABEL << address << endl;
}

inline void print_goto(std::ofstream &ofs, size_t address)
{
	ofs << TAB << GOTO << SPACE << LABEL << hex << address << endl;
}

inline void print_lastjmpin_block(std::ofstream &ofs, std::ifstream &ifs, const std::string &line)
{
	auto ispace = line.find(' ');
	ofs << TAB << line.substr(0, ispace) << TAB << line.substr(ispace + 1) << endl;
}

void CodeAnalyser::print_block(std::unordered_set<size_t > &functions, std::unordered_set<size_t > &printed_functions, std::ofstream &ofs, std::ifstream &ifs, const std::pair<Graph::faddress_t, Graph::faddress_t > &fstart_end)
{
	const Graph::faddress_t &fstart = fstart_end.first;
	const Graph::faddress_t &fend = fstart_end.second;

	ifs.seekg(fstart.first);
	string line;
	if (!(fstart.second & 0x1))	// is  not label
		getline(ifs, line);
	while (static_cast<size_t>(ifs.tellg()) != fend.first)
	{
		getline(ifs, line);
		if (line == "")
			continue;
		size_t address;
		long long offset;
		bool dummy;
		if (found(line, address, offset, dummy) == 2)	// found call
		{
			auto cadd = found_calls.find(address)->second;
			if (printed_functions.find(cadd) == end(printed_functions))
				functions.insert(cadd);
		}
		print_line(ofs, line);
	}
	if (fend.second & 0x2)	// is "jmp"
	{
		getline(ifs, line);
		size_t address;
		long long offset;
		bool cond;
		int res = found(line, address, offset, cond);
		print_lastjmpin_block(ofs, ifs, line);
		if (res == 1 && !cond)	// JMP
			print_goto(ofs, address + offset);
		// no need check for call, because call is not "jmp"
	}
}

void CodeAnalyser::process_cycle(Graph::vertex_t first, Graph::vertex_t last, const std::map<Graph::vertex_t, Graph::vertex_t > &parent)
{
//	cycles[last] = first;
}

void CodeAnalyser::process_ifstatement(Graph::vertex_t vertex, Graph::vertex_t not_jmp, Graph::vertex_t jmp)
{
//	if_statements.insert({ vertex, jmps_t(not_jmp, jmp) });
}

void CodeAnalyser::analyse()
{
	ofstream ofs(ofile);
	ifstream ifs(ifile);

	// dfs and find structures
	
	map<Graph::vertex_t, Graph::vertex_t > parents;
	std::unordered_set<Graph::vertex_t > printed_functions, functions{ graph.get_start() };

	
	while (!functions.empty() && !exit)
	{
		set<Graph::vertex_t > closed_vertices, open_vertices;
		stack<Graph::vertex_t > vertices;
		auto func_addr = *functions.cbegin();
		print_function(ofs, func_addr);
		vertices.push(func_addr);
		functions.erase(functions.cbegin());
		printed_functions.insert(func_addr);		
		while (!vertices.empty() && !exit)
		{
			Graph::vertex_t vertex = vertices.top();
			if (!graph.inrange(vertex))
			{
				vertices.pop();
				continue;
			}

			auto it = open_vertices.find(vertex);
			if (it != cend(open_vertices))	// we returned to vertex, close it
			{
				open_vertices.erase(it);
				closed_vertices.insert(vertex);
				vertices.pop();
				continue;
			}

			it = closed_vertices.find(vertex);
			if (it != std::end(closed_vertices))	// we have found paths from this vertex
			{
				vertices.pop();
				continue;
			}

			open_vertices.insert(vertex);	// first time in vertex
			pair<Graph::faddress_t, Graph::faddress_t > fstart_fend;
			graph.blocks.hlasm_insert(graph.blocks.bbasm(vertex), { ofs.tellp(), 1 });
			print_label(ofs, vertex);
			if (!graph.get_fblock(vertex, fstart_fend, false))
				ofs << "WARNING: In this block is mistake. There should be instruction at address " << vertex << endl;

			print_block(functions, printed_functions, ofs, ifs, fstart_fend);

			auto range = graph.succ(vertex);
			bool cycle = false;
			for (auto succ = range.first; succ != range.second; ++succ)
			{
				if (!graph.inrange(*succ))
					continue;

				if (closed_vertices.find(*succ) != end(closed_vertices))	// if closed, skip
					continue;

				if (open_vertices.find(*succ) != end(open_vertices))	// we found cycle
				{
					process_cycle(*succ, vertex, parents);
					print_cycle(ofs, *succ, jmp_flags, test_instruction, test_operands);
					cycle = true;
					continue;
				}

				vertices.push(*succ);	// save vertex
				parents[*succ] = vertex;	// save its parent
			}

			if (!cycle && range.second - range.first == 2)
			{
				print_ifstatement(ofs, *range.first, jmp_flags, test_instruction, test_operands);
			}
			
		}
	}


	ifs.close();	// check file + blocks
	ofs.close();
}


