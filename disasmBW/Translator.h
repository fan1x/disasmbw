#pragma once

#include "../Dictionary/Dictionary.h"
#include "CodeAnalyser.h"
#include "PEUndecorator.h"

#include <thread>
#include <utility>
#include <string>

// facade to disassembler and code analyzer
class Translator
{
public:
	typedef std::pair<std::size_t, std::string > address_instruction_t;
	typedef std::vector<address_instruction_t >::const_iterator block_it_t;

	Translator(const std::string &file);
	Translator();
	~Translator();

	inline void switch_block_level()
	{
		canal.swich_block();
		block.last = block.first - 1;	// erase ranges to load again block
	}

	inline bool asm_ready() const
	{
		return inasm;
	}
	inline bool hlasm_ready() const
	{
		return inhlasm;
	}
	// return true if address in address range
	inline bool inrange(size_t address) const
	{
		return canal.inrange(address);
	}
	
	void to_hlasm();	// do code analysis 
	void to_asm();	// converts program to asm
	
	inline void kill()
	{
		canal.kill();
	}

	// return next block for relative address
	std::pair<block_it_t, block_it_t > get_next();
	// return previous block for relative address
	std::pair<block_it_t, block_it_t > get_previous();
	std::pair<block_it_t, block_it_t > get_block(size_t address);
	// as address is used start
	std::pair<block_it_t, block_it_t > get_block(bool new_start = true);
private:
	static const std::string format_file;
	static const std::string instruction_file;
	static const std::string asm_file;
	static const std::string hlasm_file;
	static Dictionary dict;

	mutable struct
	{
		inline std::pair<block_it_t, block_it_t > block() const
		{
			return std::move(std::make_pair(std::cbegin(last_block), std::cend(last_block)));
		}
		inline void insert(std::vector<address_instruction_t > &&block)
		{
			last_block = std::move(block);
			auto cb = cbegin(last_block);
			do
			{
				first = cb->first;
				++cb;
			} while (!first);
			auto crb = crbegin(last_block);
			do
			{
				last = crb->first;
				++crb;
			} while (!last);
		}

		std::vector<address_instruction_t > last_block;
		size_t first;	// first address
		size_t last;	// last address
	} block;	

	std::vector<address_instruction_t > get_block(const std::pair<Graph::faddress_t, Graph::faddress_t > &frange) const;

	std::string file_name;
	PEFileUndecorator pefu;
	CodeAnalyser canal;
	bool inasm;		// in asm
	bool inhlasm;	// in high-level asm
	size_t start;
};
