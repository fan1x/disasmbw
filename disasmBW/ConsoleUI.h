#pragma once
#include "UI.h"
#include "Debugger.h"

#include <thread>

class ConsoleUI :
	public UI
{
public:
	bool process_breakpoint(const Dbg::Debugger::Context &) override;
	void process_unloaddll(const Dbg::Debugger::Context &) override;
	void process_loaddll(const Dbg::Debugger::Context &) override;
	void process_create_thread(const Dbg::Debugger::Context &) override;
	void process_exit_thread(const Dbg::Debugger::Context &) override;
	void process_create_process(const Dbg::Debugger::Context &) override;
	void process_exit_process(const Dbg::Debugger::Context &) override;
	void process_debug_string(const Dbg::Debugger::Context &) override;
	inline void process_hlanalysis() override
	{
		//	std::cout << "high-level analysis completed";
	}

	void debugger_key_loop();
	// 1- up, 2-down, 3-goto, 0-esc
	int presenter_key_loop();
	void start_loop();
	void disassembly_key_loop();

	void run() override;

	void show_registers() const;
	void show_threads() const;
	inline void show_breakpoints() const
	{
		std::cout << std::endl;
		std::cout << "Breakpoints:" << std::endl;
		for (size_t address : dbg_context.breakpoints)
		{
			std::cout << " 0x" << dbg_context.base_address + address << std::endl;
		}
	}
	void show_block(const std::pair<Translator::block_it_t, Translator::block_it_t > &range) const;

	void switch_thread();
	void add_breakpoint();
	void remove_breakpoint();

	void start_menu() const;
	void debug_menu() const;
	void presenter_menu() const;
	void disassembly_menu() const;

	void debug(bool pause);
	void disassembly();

	void presenter_mode(size_t address);
	void disassembly_mode();

private:
	inline void reset()
	{
		created_process = false;
	}

	Dbg::Debugger::Context dbg_context;
	std::string pname;	// name of program to process
	std::thread key_handler;
	std::thread old_handler;
	std::thread hlanal_handler;
	bool created_process;
	static const size_t WAIT_TIME;
};


struct ProgressBar
{
	ProgressBar():
		stop(false), paused(false)
	{
		std::thread thr(&ProgressBar::run, this);
		thr.detach();
	}
	~ProgressBar()
	{
		destroy();
	}
	inline void destroy()
	{
		stop = true;
	}
	inline void run()
	{
		while (!stop)
		{
			std::cout << '.';
			do
			{
				std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
			} while (paused);
		}
	}
	inline void pause()
	{
		paused = true;
	}
	inline void unpause()
	{
		paused = false;
	}

	bool paused;
	bool stop;
	const size_t WAIT_TIME = 3;
};