#include "ConsoleUI.h"

#include <iostream>
#include <sstream>
#include <thread>
#include <cstring>
#include <chrono>
#include <limits>

#include <Windows.h>
#include <Shlwapi.h>


using std::cin;
using std::cout;
using std::endl;

#define VK_KEY0 (0x30)
#define VK_KEY1 (0x31)
#define VK_KEY2 (0x32)
#define VK_KEY3 (0x33)
#define VK_KEY4 (0x34)
#define VK_KEY5 (0x35)
#define VK_KEY6 (0x36)
#define VK_KEY7 (0x37)
#define VK_KEY8 (0x38)
#define VK_KEY9 (0x39)

#define LINE ("------------------------------")
#define TAB ('\t')

inline void flush_cin()
{
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
}

inline void flush_key_state()
{
	GetAsyncKeyState(VK_KEY0);
	GetAsyncKeyState(VK_KEY1);
	GetAsyncKeyState(VK_KEY2);
	GetAsyncKeyState(VK_KEY3);
	GetAsyncKeyState(VK_KEY4);
	GetAsyncKeyState(VK_KEY5);
	GetAsyncKeyState(VK_KEY6);
	GetAsyncKeyState(VK_KEY7);
	GetAsyncKeyState(VK_KEY8);
	GetAsyncKeyState(VK_KEY9);
	GetAsyncKeyState(VK_ESCAPE);
	GetAsyncKeyState(VK_UP);
	GetAsyncKeyState(VK_DOWN);
}

inline bool read_number(size_t &res)
{
	string str;
	flush_cin();
	cin >> str;
	try
	{
		res = std::stoull(str, nullptr, 16);
		return true;
	}
	catch (...)
	{
		return false;
	}
}

inline bool read_number(size_t lower_bound, size_t upper_bound, size_t &res)
{
	string line;
	flush_cin();
	cin >> line;
	try
	{
		res = std::stoul(line);
	}
	catch (...)
	{
		return false;
	}

	return lower_bound <= res && res <= upper_bound;
}

template<class T>
T check_input(const std::set<T > &choices, const T &default_choice)
{
	const char MAX_TRIES = 2;
	T val;
	for (char i = 0; i < MAX_TRIES; ++i)
	{
		flush_cin();
		cin >> val;
		if (choices.find(val) != std::cend(choices))
			return val;

		cout << endl << "Bad input, try again: ";
	}

	return default_choice;
}

const size_t ConsoleUI::WAIT_TIME = 100;

void ConsoleUI::run()
{
	if (!Dictionary::initialized)
	{
		cout << "There is a problem with configurations files" << endl;
		cout << "Good bye!" << endl;
		return;
	}

	std::cout << "Welcome to disasmBW" << std::endl;
	cout << "Press F1 to help" << endl;
	start_loop();
}

// LOOPS

void ConsoleUI::start_loop()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));

		if (GetAsyncKeyState(VK_F1))
			start_menu();
		else if (GetAsyncKeyState(VK_KEY1))
			disassembly();
		else if (GetAsyncKeyState(VK_ESCAPE))
		{
			if (!translator.hlasm_ready())	// analysis still running
			{
				translator.kill();

				while (!translator.hlasm_ready())
					std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
			}
				
			cout << "Good bye!" << endl;
			break;
		}				
	}
}

void ConsoleUI::debugger_key_loop()
{
	while (dbg.state() != Dbg::Debugger::State::STOPPED)
	{
		if (GetAsyncKeyState(VK_F1))
			debug_menu();
		else if (GetAsyncKeyState(VK_KEY5))
			dbg.start();
		else if (GetAsyncKeyState(VK_KEY6))
			dbg.pause();
		else if (GetAsyncKeyState(VK_KEY8))
			show_threads();
		else if (GetAsyncKeyState(VK_KEY9))
			switch_thread();
		else if (GetAsyncKeyState(VK_KEY0))
			dbg.next();		
		else if (GetAsyncKeyState(VK_ESCAPE))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			break;
		}

		flush_key_state();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	
	dbg.stop();
}

int ConsoleUI::presenter_key_loop()
{
	flush_key_state();
	while (true)
	{
		if (GetAsyncKeyState(VK_F1))
			presenter_menu();
		else if (GetAsyncKeyState(VK_KEY4) && translator.hlasm_ready())
			return 4;
		else if (GetAsyncKeyState(VK_KEY5))
			show_registers();
		else if (GetAsyncKeyState(VK_KEY6))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
			return 3;
		}
		else if (GetAsyncKeyState(VK_KEY7))
			show_breakpoints();
		else if (GetAsyncKeyState(VK_KEY8))
			remove_breakpoint();
		else if (GetAsyncKeyState(VK_KEY9))
			add_breakpoint();
		else if (GetAsyncKeyState(VK_UP))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
			return 1;
		}
		else if (GetAsyncKeyState(VK_DOWN))
		{ 
			std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
			return 2;
		}
		else if (GetAsyncKeyState(VK_ESCAPE))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
			return 0;
		}

		flush_key_state();
		std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
	}
	return 0;
}

void ConsoleUI::disassembly_key_loop()
{
	pair<Translator::block_it_t, Translator::block_it_t > range;
	while (true)
	{
		flush_cin();
		if (GetAsyncKeyState(VK_F1))
		{
			disassembly_menu();
		}			
		else if (GetAsyncKeyState(VK_KEY1))
		{
			debug(false);
		}			
		else if (GetAsyncKeyState(VK_KEY2))
		{
			debug(true);
		}
		else if (GetAsyncKeyState(VK_KEY3))
		{
			flush_cin();
			cout << "Write address to go to: ";
			size_t res;
			if (read_number(res))
			{
				range = translator.get_block(res);
				show_block(range);
			}
			else
			{
				cout << "Can't go to given address" << endl;
			}
		}
		else if (GetAsyncKeyState(VK_KEY4))
		{
			if (translator.hlasm_ready() && translator.asm_ready())
			{
				translator.switch_block_level();
				range = translator.get_block(false);
				show_block(range);
			}
		}
		else if (GetAsyncKeyState(VK_UP))
		{
			range = translator.get_previous();
			show_block(range);
		}
		else if (GetAsyncKeyState(VK_DOWN))
		{
			range = translator.get_next();
			show_block(range);
		}
		else if (GetAsyncKeyState(VK_ESCAPE))
		{
			break;
		}

		flush_key_state();
		std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME));
	}
}

// MENUS

void ConsoleUI::disassembly_menu() const
{
	cout << endl;
	cout << "Help:" << endl;
	cout << "Press ESC to end presenter" << endl;
	cout << "Press up and down arrows to navigate between blocks" << endl;
	cout << "Press 1 to debug" << endl;
	cout << "Press 2 to debug with pause" << endl;
	cout << "Press 3 to goto address" << endl;
	if (translator.hlasm_ready())
		cout << "Press 4 to switch mode" << endl;
	cout << endl;
}

void ConsoleUI::start_menu() const
{
	cout << endl;
	cout << "Help:" << endl;
	cout << "Press ESC to end" << endl;
	cout << "Press 1 to disassembly" << endl;
	cout << endl;
}

void ConsoleUI::debug_menu() const
{
	std::cout << std::endl;
	cout << "Help:" << endl;
	cout << "Press ESC to end debugger" << endl;
	cout << "Press 5 to continue" << std::endl;
	cout << "Press 6 to pause debugger" << endl;
	cout << "Press 8 to show threads" << endl;
	cout << "Press 9 to show thread content" << endl;
	cout << "Press 0 to step" << endl;
	cout << endl;
}

void ConsoleUI::presenter_menu() const
{
	std::cout << std::endl;
	cout << "Help:" << endl;
	cout << "Press ESC to end presenter" << endl;
	cout << "Press up and down arrows to navigate between blocks" << std::endl;
	if (translator.hlasm_ready())
		cout << "Press 4 to switch mode" << endl;
	cout << "Press 5 to show registers" << endl;
	cout << "Press 6 to goto address" << endl;
	cout << "Press 7 to show breakpoints" << endl;
	cout << "Press 8 to delete breakpoint" << endl;
	cout << "Press 9 to add breakpoint" << endl;
	cout << endl;
}

void ConsoleUI::disassembly()
{
	std::cout << "Write name of program: ";
	flush_cin();
//	std::cin >> pname;
	pname = "C:\\Users\\Tom�\\Projects\\disasmBW\\tests\\MFCApplication1_x86.exe";
//	pname = "C:\\Users\\tfaltin\\Projects\\disasmBW\\tests\\test_op32.obj";
	if (!PathFileExists(pname.c_str()))
	{
		cout << "Given file doesn't exist!" << endl;
		return;
	}
		
	cout << "Disassembling";
	ProgressBar pb;
	try
	{
		translator = Translator(pname);	
	}
	catch (...)	// can't translate file
	{
		pb.destroy();
		cout << endl << "Given file is not supported!" << endl;
		return;
	}	
	translator.to_asm();
	pb.destroy();
	cout << endl << "Program is disassembled" << endl << endl;;
	hlanal_handler = std::thread([this]
	{
		translator.to_hlasm();
	});
	hlanal_handler.detach();

	disassembly_mode();
}

void ConsoleUI::debug(bool pause)
{
	reset();
	if (pname.empty())
		disassembly();

	dbg.run(pname, pause);
}

void ConsoleUI::disassembly_mode()
{
	auto range = translator.get_block();
	show_block(range);
	disassembly_key_loop();
}

void ConsoleUI::presenter_mode(size_t address)
{
	size_t reladd = address - dbg_context.base_address;	// relative address
	auto bounds = translator.get_block(reladd);
	while (true)
	{
		cout << LINE << endl;
		for (auto it = bounds.first; it != bounds.second; ++it)
		{
			if (it->first == 0)	// label
			{
				cout << it->second << endl;
				continue;
			}

			if (dbg_context.breakpoints.find(it->first) == cend(dbg_context.breakpoints))
				cout << "  ";
			else
				cout << "B ";

			if (it->first == reladd)
				cout << "->0x" << it->first + dbg_context.base_address << ": " << it->second << endl;
			else
				cout << "  0x" << it->first + dbg_context.base_address << ": " << it->second << endl;
		}

		int res = presenter_key_loop();
		if (!res)
			break;
		else if (res == 1)
		{
			bounds = translator.get_previous();
		}
		else if (res == 2)
		{
			bounds = translator.get_next();
		}
		else if (res == 3)
		{
			flush_cin();
			cout << "Write address to go to: ";
			size_t res;
			if (read_number(res))
				bounds = translator.get_block(res - dbg_context.base_address);
			else
				cout << "Can't go to given address." << endl;
		}
		else if (res == 4)
		{
			translator.switch_block_level();
			bounds = translator.get_block(false);
		}
	}
}

void ConsoleUI::show_block(const std::pair<Translator::block_it_t, Translator::block_it_t > &range) const
{
	cout << LINE << endl;
	for (auto line = range.first; line != range.second; ++line)
	{
		if (line->first == 0x0)	// f.e. label
			cout << line->second << endl;
		else
			cout << "0x" << std::hex << line->first << ": " << line->second << endl;
	}
}

void ConsoleUI::show_registers() const
{
	cout << "Registers: " << endl;
	cout << " RIP=" << dbg_context.registers.rip << endl;
	cout << " RAX=" << dbg_context.registers.rax << endl;
	cout << " RBX=" << dbg_context.registers.rbx << endl;
	cout << " RCX=" << dbg_context.registers.rcx << endl;
	cout << " RDX=" << dbg_context.registers.rdx << endl;
	cout << " RSP=" << dbg_context.registers.rsp << endl;
	cout << " RBP=" << dbg_context.registers.rbp << endl;
	cout << " RSI=" << dbg_context.registers.rsi << endl;
	cout << " RDI=" << dbg_context.registers.rdi << endl;
	cout << "  R8=" << dbg_context.registers.r8 << endl;
	cout << "  R9=" << dbg_context.registers.r9 << endl;
	cout << " R10=" << dbg_context.registers.r10 << endl;
	cout << " R11=" << dbg_context.registers.r11 << endl;
	cout << " R12=" << dbg_context.registers.r12 << endl;
	cout << " R13=" << dbg_context.registers.r13 << endl;
	cout << " R14=" << dbg_context.registers.r14 << endl;
	cout << " R15=" << dbg_context.registers.r15 << endl;
	cout << endl;
}

void ConsoleUI::show_threads() const
{
	cout << "Threads:" << endl;
	for (const auto &name_handle_pair : dbg_context.threads)
		cout << name_handle_pair.first << endl;

	cout << endl;
}

void ConsoleUI::switch_thread()
{
	if (dbg_context.threads.size() > 1)
	{
		cout << endl;
		cout << "Threads:" << endl;
		size_t i = 0;
		for (const auto &name_handle_pair : dbg_context.threads)
			cout  << name_handle_pair.first << endl;

		cout << "Write a thread to switch to: ";
		size_t res;
		string num;
		flush_cin();
		cin >> num;
		try
		{
			res = std::stoul(num, nullptr, 16);
		}
		catch (...)
		{
			cout << "Can't switch to thread: " << num << endl << endl;
			return;
		}

		auto it = dbg_context.threads.find(res);
		if (it == cend(dbg_context.threads))
		{
			cout << "Can't switch to thread: " << res << endl << endl;
			return;
		}			
		else
		{
			dbg_context = dbg.get_thread_context(it->first);
			cout << "Switched to thread: " << res << endl << endl;
		}
	}

	presenter_mode(dbg_context.registers.rip);
}

void ConsoleUI::add_breakpoint()
{
	std::string str;
	std::cout << "Write address of breakpoint: ";
	flush_cin();
	std::cin >> str;
	size_t address;
	try
	{
		address = std::stoull(str, nullptr, 16);
	}
	catch (...)
	{
		std::cout << "Bad address of breakpoint: " << str << endl;
		return;
	}
	
	size_t rel_addr = address - dbg_context.base_address;
	if (translator.inrange(rel_addr))
	{
		dbg_context.breakpoints.insert(rel_addr);
		dbg.add_breakpoint(rel_addr);
		std::cout << "Added breakpoint at " << address << endl;
	}
	else
		std::cout << "Can't add breakpoint at " << address << endl;

	cout << endl;
}

void ConsoleUI::remove_breakpoint()
{
	std::string str;
	std::cout << "Write address of breakpoint: ";
	flush_cin();
	std::cin >> str;
	size_t address;
	try
	{
		address = std::stoull(str, nullptr, 16);
	}
	catch (...)
	{
		std::cout << "Bad address of breakpoint: " << str << endl;
		return;
	}

	size_t rel_addr = address - dbg_context.base_address;
	if (dbg_context.breakpoints.find(rel_addr) != std::cend(dbg_context.breakpoints))
	{
		dbg_context.breakpoints.erase(rel_addr);
		dbg.remove_breakpoint(rel_addr);
		std::cout << "Remove breakpoint at " << address << endl;
	}
	else
		std::cout << "Can't remove breakpoint at " << address << endl;

	cout << endl;
}

// CALLBACKS

bool ConsoleUI::process_breakpoint(const Dbg::Debugger::Context &context)
{
	dbg_context = context;
	if (dbg_context.user_breakpoint)
		cout << "User breakpoint in thread: " << dbg_context.curr_thread << " at address: " << dbg_context.registers.rip << endl;
	else
		std::cout << "Breakpoint in thread: " << dbg_context.curr_thread << " at address: " << dbg_context.registers.rip << std::endl;
	return false;
}

void ConsoleUI::process_create_process(const Dbg::Debugger::Context &context)
{
	if (!created_process)
	{
		std::cout << std::hex << "Application is beeing debugged" << std::endl;
		dbg_context = context;

		std::thread key_loop(&ConsoleUI::debugger_key_loop, this);	// create thread for key taking
		key_loop.detach();
		created_process = true;
		
		return;
	}
	dbg_context = context;
	std::cout << "Next process with id: " << context.curr_process << std::endl;
	cout << endl;
}

void ConsoleUI::process_exit_process(const Dbg::Debugger::Context &context)
{
	std::cout << "Debugged aplication has exited with code: " << context.last_exit_code << endl;
	dbg_context = context;
}

void ConsoleUI::process_create_thread(const Dbg::Debugger::Context &context)
{
	std::cout << "Create new thread with id: " << context.curr_thread << std::endl;
	dbg_context = context;
}

void ConsoleUI::process_exit_thread(const Dbg::Debugger::Context &context)
{
	std::cout << "Exit thread: " << context.curr_thread << " with exit code: " << context.last_exit_code << std::endl;
	dbg_context = context;
}

void ConsoleUI::process_loaddll(const Dbg::Debugger::Context &context)
{
//	std::cout << "Load DLL: " << (size_t)context.last_dll << std::endl;
}

void ConsoleUI::process_unloaddll(const Dbg::Debugger::Context &context)
{
//	std::cout << "Unload DLL: " << (size_t) context.last_dll << std::endl;
}

void ConsoleUI::process_debug_string(const Dbg::Debugger::Context &context)
{
	std::cout << "Debug string: \"" << context.debug_string << "\"" << endl;
}




