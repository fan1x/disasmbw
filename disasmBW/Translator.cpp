#include "Translator.h"
#include "PEUndecorator.h"

#include <fstream>
#include <string>
#include <utility>
#include <vector>

using std::string;
using std::pair;

#define PE32 (0x10b)
#define PE32plus (0x20b)


const std::string Translator::format_file = ".\\..\\instruction_format.xml";
const std::string Translator::instruction_file = ".\\..\\instructions.xml";
const std::string Translator::asm_file = "program.asm";
const std::string Translator::hlasm_file = "program.hlasm";

Dictionary Translator::dict(Translator::format_file, Translator::instruction_file);

Translator::Translator(const std::string &file_path):
file_name(file_path), canal(asm_file, hlasm_file), inhlasm(true), inasm(false)
{
	FileReader::file_name = file_name;
	pefu = PEFileUndecorator(true);
	const auto &ch = pefu.fheader().Characteristics;

	if (!(ch & IMAGE_FILE_EXECUTABLE_IMAGE) || (ch & IMAGE_FILE_DLL))	// is not supported type
		throw std::runtime_error("File not supported.");

	if (pefu.optheader().Magic == PE32)	// 32bit
		dict.set_flags(Info(Info::IA32));
	else if (pefu.optheader().Magic == PE32plus)	// 64bit
		dict.set_flags(Info(Info::IA64));
}

Translator::Translator():
inhlasm(true), inasm(false)
{}

Translator::~Translator()
{
}

void Translator::to_hlasm()
{
//	if (!inasm)
//		to_asm();

	inhlasm = false;
	canal.analyse();
	inhlasm = true;
}

void Translator::to_asm()
{
	inasm = false;
	dict.start(asm_file, pefu.optheader().BaseOfCode);
	PEFileUndecorator::buffer_t byte;
	while (pefu.good())
	{
		pefu >> byte;
		dict.find(byte[0]);
	}
	dict.end();
	std::cout << endl << "Analysing code";
	canal.load(pefu.optheader().AddressOfEntryPoint);
	start = pefu.optheader().AddressOfEntryPoint;
	inasm = true;
}

inline std::pair<size_t, std::string > split_asmline(const std::string &line)
{
	auto itspace = line.find(' ');
	size_t a = std::stoull(line.substr(0, itspace - 1), nullptr, 16);
	return std::move(std::make_pair(a, line.substr(itspace + 1)));
}

std::pair<size_t, std::string > split_hlasmline(const std::string &line)
{
	if (line.empty())
		return {};
	auto cit = cbegin(line);

	if (*cit != '\t')
		return std::move(std::make_pair(0, line));

	++cit;
	while (*cit == '\t')
		++cit;

	if (*cit == 'I')	// if 
	{
		return std::move(std::make_pair(0, line));// .substr(cit - cbegin(line) - 1)));
	}
	else if (*cit == 'G')	// goto
	{
		return std::move(std::make_pair(0, line));// .substr(cit - cbegin(line) - 1)));	// let 1 tab before
	}
	
	auto str_after = line.substr(cit - cbegin(line));
	size_t a = std::stoull(str_after, nullptr, 16);
	auto tpos = str_after.find('\t');
	
	return std::move(std::make_pair(a, str_after.substr(++tpos)));
}

std::vector<Translator::address_instruction_t > Translator::get_block(const std::pair<Graph::faddress_t, Graph::faddress_t > &range) const
{
	std::vector<Translator::address_instruction_t > new_block;
	std::ifstream ifs(canal.asm_block() ? asm_file : hlasm_file);
	std::pair<size_t, std::string >(&splitter)(const std::string &) = canal.asm_block() ? split_asmline : split_hlasmline;
	string line;
	ifs.seekg(range.first.first);
	if (!(range.first.second & 0x1))	// not a label
		std::getline(ifs, line);
	while (static_cast<size_t>(ifs.tellg()) != range.second.first)
	{
		std::getline(ifs, line);
		new_block.emplace_back(splitter(line));
	}
	if (range.second.second & 0x2)	// is "jmp"
	{
		std::getline(ifs, line);
		new_block.emplace_back(splitter(line));
	}	

	return std::move(new_block);
}

std::pair<Translator::block_it_t, Translator::block_it_t > Translator::get_block(size_t address)
{
	if (block.first > address || address > block.last)
	{
		auto frange = canal.address_block(address);
		if (frange.first.first || frange.second.first)	// in range
			block.insert(get_block(frange));
		else
			block.insert(std::move(std::vector<Translator::address_instruction_t >(1, { address, "??????????" })));
	}

	return std::move(block.block());
}

std::pair<Translator::block_it_t, Translator::block_it_t > Translator::get_block(bool new_start)
{
	size_t address = new_start ? start : block.first;
	if (block.first > address || address > block.last)
	{
		auto frange = canal.address_block(address);
		if (frange.first.first || frange.second.first)	// in range
			block.insert(get_block(frange));
		else
			block.insert(std::move(std::vector<Translator::address_instruction_t >(1, { address, "??????????" })));
	}

	return std::move(block.block());
}

std::pair<Translator::block_it_t, Translator::block_it_t > Translator::get_next()
{
	auto frange = canal.next_block();
	auto asm_block = get_block(frange);
	while (asm_block.empty())
	{
		frange = canal.next_block();
		asm_block = get_block(frange);
	}
	
	block.insert(std::move(asm_block));
	return std::move(block.block());
}

std::pair<Translator::block_it_t, Translator::block_it_t > Translator::get_previous()
{
	auto frange = canal.previous_block();
	auto asm_block = get_block(frange);
	while (asm_block.empty())
	{
		frange = canal.previous_block();
		asm_block = get_block(frange);
	}

	block.insert(std::move(asm_block));
	return std::move(block.block());
}
