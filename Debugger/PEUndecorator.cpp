#include "PEUndecorator.h"

#include <exception>
#include <stdexcept>
#include <algorithm>
#include <fstream>


size_t FileReader::base;
std::string FileReader::file_name;

void FileReader::get(char *buffer, size_t start, size_t buffer_size)
{
	static std::ifstream input(file_name, std::ios::in | std::ios::binary);
	input.seekg(start);
	input.read(buffer, buffer_size);
}

size_t MemoryReader::base;
HANDLE MemoryReader::process;

void MemoryReader::get(char *buffer, size_t start, size_t buffer_size)
{
	ReadProcessMemory(process, reinterpret_cast<LPCVOID>(start), buffer, buffer_size, NULL);
}