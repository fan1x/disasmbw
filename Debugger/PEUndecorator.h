#pragma once

#include <windows.h>
#include <ImageHlp.h>
#include <winnt.h>

#include <string>
#include <fstream>

template<class T>
class PEUndecorator
{
public:
	enum { ExReadProcessMemory, ExUnknownFile };	// exceptions types

	static const size_t BUFFER_SIZE = 0xff;
	typedef char buffer_t[BUFFER_SIZE];

	PEUndecorator()
	{
		T::get((char*)&dos_header, T::base, sizeof(dos_header));
		T::get((char*)&file_header, T::base + dos_header.e_lfanew, sizeof(file_header));
		T::get((char*)&optional_header,T::base + dos_header.e_lfanew + sizeof(file_header) + BYTE_SIZE, sizeof(optional_header));
	}
	~PEUndecorator() {};

	// returns buffer filled with instructions
	buffer_t &operator>>(buffer_t &buffer)
	{
		if (optional_header.SizeOfCode < curr)
			return buffer;

		T::get(buffer, BUFFER_SIZE, curr);
		curr += BUFFER_SIZE;

		return buffer;
	}

	inline void reset()
	{
		curr = optional_header.BaseOfCode;
	}
	inline const IMAGE_DOS_HEADER &dheader() const
	{
		return dos_header;
	}
	inline const IMAGE_FILE_HEADER &fheader() const
	{
		return file_header;
	}
	inline const IMAGE_OPTIONAL_HEADER &optheader() const
	{
		return optional_header;
	}

private:
	IMAGE_DOS_HEADER dos_header;
	IMAGE_FILE_HEADER file_header;	
	IMAGE_OPTIONAL_HEADER optional_header;
	
	size_t curr;

	const char BYTE_SIZE = 4 * sizeof(char);
	const char MAGIC_NUMBER_SIZE = 2;
};

struct FileReader
{
	static size_t base;
	static std::string file_name;
	static void get(char *buffer, size_t start, size_t buffer_size);
};

struct MemoryReader
{
	static size_t base;
	static HANDLE process;
	static void get(char *buffer, size_t start, size_t buffer_size);
};

typedef PEUndecorator<FileReader> PEFileUndecorator;
typedef PEUndecorator<MemoryReader> PEMemoryUndecorator;

