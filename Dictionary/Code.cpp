#include "Code.h"
#include "Info.h"

Code::Code(const std::string &name, const codings_values_t &codings_values, size_t mode) : name(name), mode(mode), codings_values(codings_values)
{
}

Code::Code():
mode(Info::Architecture::none)
{}

Code::~Code(void)
{
}

// insert this code into codes on path given in opcode
void Code::insert(trie_codes_t &codes, const opcode_t &opcode)
{
	std::bitset<Info::arch_size> bmode(mode);
	for (size_t i = 0; i < Info::arch_size; ++i)
	{
		if (bmode[i])
		{
			CodeTrie *ctrie = codes.at(i);
			if (ctrie->find(begin(opcode), end(opcode)))
			{
				if (!ctrie->curr_data())
				{
					ctrie->curr_data() = this;
					continue;
				}
				ctrie->curr_data() = ctrie->curr_data()->merge(this, i);
			}
			else
			{
				ctrie->insert(this, begin(opcode), end(opcode));
			}
				
		}
	}
}