#include "Instruction.h"
#include "utils.h"
#include "Operand.h"
#include "Dictionary.h"
#include "Info.h"
#include "RulesTable.h"

#include <bitset>
#include <set>
#include <algorithm>
#include <cassert>
#include <iostream>

using std::vector;
using std::string;
using std::for_each;

// Instruction

Instruction::Instruction(const std::string &form, size_t mode, const operands_array_t &operands, const codings_values_t &codings_values, size_t prefixes_rules, size_t modul_opcode) :
Code(myutils::str_split(form)[0], codings_values, mode), prefixes_rules(prefixes_rules), operands(operands), form(form),
modul_opcode(modul_opcode)
{
}

Instruction::~Instruction(void)
{
}

bool Instruction::finish(Dictionary &dictionary)
{
	dictionary.process_result(str());
	clear();
	dictionary.reset();
	return true;
}

Code *Instruction::merge(Code *code, size_t architecture)
{
	if (auto *i = dynamic_cast<Instruction *>(code))
	{
		std::cerr << "Found instructions with same opcode in architecture " << architecture << " : " << i->name << " and " << name << ". ";
		if (i->prefixes_rules > prefixes_rules)
		{
			std::cerr << "Left first instruction." << std::endl;
			mode -= (1 << architecture);
			if (!mode)
				delete this;
			return i;
		}
		else if (i->prefixes_rules < prefixes_rules)
		{
			std::cerr << "Left second instruction." << std::endl;
			i->mode -= (1 << architecture);
			if (!i->mode)
				delete i;
			return this;
		}
		else
		{
			std::cerr << "Couldn't decide, left second instruction." << std::endl;
			i->mode -= (1 << architecture);
			--i->modul_opcode;
			if (!i->mode && !i->modul_opcode)
				delete i;
			return this;
		}
	}

	return code->merge(this, architecture);	// merge modul+instruction
}

std::string Instruction::str() const
{
	std::string res(name);
	for (auto *op : operands)
		if (op == nullptr)
			break;
		else
		{
			res += " ";
			res += op->str();
			res += ",";
		}

	if (res.back() == ',')
		res.pop_back();
	return res;
}

void Instruction::clear()
{
	// do nothing, operands are clean after calling str
}

bool Instruction::eval_operands(const codings_values_t &codings_values, codings_names_t &codings)
{
	bool res = true;
	std::find_if(begin(operands), end(operands), [&res, &codings_values, &codings](AbstractOperand *ao)
	{
		if (!ao)
			return true;

		res &= ao->eval(codings_values, codings);
		return false;
	});

	return res;
}

void Instruction::reset()
{
	std::all_of(begin(operands), end(operands), [](AbstractOperand *ao)
	{
		if (!ao)
			return false;

		ao->reset();
		return true;
	});
	clear();
}

inline arr_modul_t::const_iterator next_modul(const arr_modul_t::const_iterator &bmoduls, const arr_modul_t::const_iterator &emoduls, const codings_names_t &codings_names)
{
	return std::find_if(bmoduls, emoduls, [&codings_names](Modul *m)
	{
		auto it = codings_names.find(m->name);
		if (it == end(codings_names))
			return false;

		auto coding = std::find_if(begin(it->second), end(it->second), [m](const string &cname)	// try find coding
		{
			return m->find_coding(cname);
		});

		return coding != end(it->second);	// coding found
	});
}

bool Instruction::find(Dictionary &dict, unsigned char byte)
{
	if (dict.found)	// seen for a first time
	{
		codings_names_t codings;
		bool evaluated;
		try
		{
			evaluated = eval_operands(dict.codings_values, codings);
		}
		catch (std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			reset();
			dict.codings_values.clear();	// clear coding values to erease first byte
			dict.rollback();
			return false;
		}

		if (evaluated)
		{
			finish(dict);
			return true;
		}

		dict.found = false;
		dict.curr_modul_ = next_modul(dict.curr_modul_, end(dict.moduls), codings);
		
		if (!dict.piece && !modul_opcode)
			return false;
	}

	if ((*dict.curr_modul_)->eval(dict.codings_values, byte))	// modul is evaluated
	{
		codings_names_t moduls;	// next needed moduls
		bool evaluated;
		try
		{
			evaluated = eval_operands(dict.codings_values, moduls);
		}
		catch (std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			reset();
			dict.codings_values.clear();	// clear coding values to erease first byte
			dict.rollback();
			return false;
		}

		if (evaluated)	// all operands evaluated
		{
			finish(dict);
			return true;
		}

		dict.curr_modul_ = next_modul(dict.curr_modul_, end(dict.moduls), moduls);

		if (dict.curr_modul_ == end(dict.moduls))
		{
			finish(dict);
			return true;
		}
	}

	return false;
}

