#pragma once

#include <string>
#include <vector>
#include <array>
#include <set>
#include <map>

#include "Code.h"
#include "Condition.h"

// fwd decl
struct Info;
class MultiOperand;
class AbstractOperand;
class Dictionary;

// pair<coding, value> 
typedef std::pair<char *, char *> opcode_condition_t;	
// <target, source, cop1, cop2, cop3, cop4>
typedef std::tuple<char *, char *, char *, char *, char *, char *> opcode_rule_t;
// <mode, condition, opcode_rules[]>
typedef std::pair<opcode_condition_t, std::vector<opcode_rule_t> > opcode_rules_t;
typedef std::map<std::string, std::vector<opcode_rules_t> > opcode_rules_names_t;
typedef std::pair<CodeTrie::placement_t, size_t> opcode_data_t;
typedef std::vector<opcode_data_t> opcode_t;

typedef std::array<AbstractOperand *, 4> operands_array_t;
typedef std::array<std::pair<std::string, AbstractOperand * >, 4 > operands_t;
//typedef std::pair<std::string, std::string > full_coding_name_t;
typedef std::unordered_map<std::string, std::vector<std::string > > codings_names_t;	// map <modul_name, set<coding_name > >

class Instruction: 
	public Code
{
public:
	// create instruction and insert multioperand to first empty place with no operand pointer
	Instruction(const std::string &form, size_t mode, const operands_array_t &operands, const codings_values_t &codings_values, size_t prefixes_rules, size_t modul_opcode = 0);
	~Instruction(void);

	std::string str() const override;
	// merge this instruction with abstract instruction and returns result
	Code *merge(Code *code, size_t architecture) override;
	// clear operands
	void clear() override;
	void reset();	// if exception
	// find given byte in instruction
	bool find(Dictionary &dict, unsigned char byte) override;
	bool finish(Dictionary &dictionary) override;
	// evalueate operands
	bool eval_operands(const codings_values_t &codings_values, codings_names_t &codings);


	// compare prefixes with prefixes withing instruction and return result
	// cares only about conditions not rules
	bool match_prefixes(const arr_modul_t &prefixes, const arr_modul_t::const_iterator &prefixes_end, const arr_modul_t::const_iterator &moduls_end) const;

	operands_array_t operands;
	size_t modul_opcode;
private:	
	size_t prefixes_rules;
	std::string form;
};

typedef std::vector<Instruction *> instructions_t;
