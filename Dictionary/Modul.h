#pragma once

#include <string>
#include <vector>
#include <map>
#include <list>

#include "Code.h"

const unsigned char BYTE_SIZE = 8;

// fwd decl
struct Coding;

typedef std::vector<Coding *> ArrCoding;
typedef std::map<std::string, Coding *> codings_name_t;
typedef std::unordered_map<std::string, size_t> codings_values_t;
typedef std::pair<opcode_t, codings_values_t > opcode_codings_pair;

class Modul :
	public Code
{
public:
	~Modul(void);
	Modul();
	Modul(const std::string &name, size_t mode, const std::vector<bool> &enter_code, const codings_values_t &codings_value, const ArrCoding &codings);

	// return true if can move to next modul
	// if true, reset pointers inside
	bool find(Dictionary &dict, byte_t byte) override;
	bool finish(Dictionary &dictionary) override;
	// merge this instruction with abstract instruction and returns result
	Code *merge(Code *code, size_t architecture) override;
	// clear operands
	void clear() override;
	// returns value of found coding with given name
	size_t find(const std::string &name);
	// find coding with given name or return nullptr
	bool find_coding(const std::string &name);
	CodeTrie::placement_t placement(Coding *coding) const;
	inline std::string str() const override { return ""; }

	// return true if modul is evaluated
	bool eval(codings_values_t &coding_values, byte_t byte);

	std::vector<size_t > gen_values(const std::vector<Condition> &conditions) const;

	std::vector<opcode_codings_pair> get_opcodes(const std::vector<std::pair<std::string, Condition> > &vc = {});
	codings_name_t codings_name_;	//todo: make it private + make friend to Loader
private:
	std::vector<bool> m_enter_code;
	ArrCoding m_codings;	// TODO add it to Code -> instruciton also need ???
	ArrCoding::iterator m_curr_coding;	
	size_t codings_start;	// start of last coding
	size_t enter_coding;
};

typedef std::unordered_multimap<std::string, Modul *> moduls_names_t;
typedef std::vector<Modul *> arr_modul_t;

// return whether modul (or moduls with same name that goes after modul!) is in prefixes
// if true, returns iterator to modul within prefixes
bool modul_match_prefixes(arr_modul_t::const_iterator &modul, const arr_modul_t &prefixes);
// returns coding or nullptr if not found
// name must be full name
bool find_coding(const moduls_names_t &modul_names, const std::string &name);
CodeTrie::placement_t placement(Coding *coding);
void add_value_to_all(byte_t start, byte_t size, bool new_byte, const std::string &name, size_t value, std::vector<opcode_codings_pair> &voc);


