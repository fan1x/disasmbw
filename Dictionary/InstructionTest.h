#pragma once

#include "Dictionary.h"
#include "Info.h"

// class used for testing and creating tests
class InstructionTest
{
public:
	InstructionTest(const char *instruction_file, const char *format_file);
	~InstructionTest();

	bool start();
private:
	std::ofstream fb16;
	std::ofstream fb32;
	std::ofstream fb64;

	std::ofstream fa16;
	std::ofstream fa32;
	std::ofstream fa64;

	bool operand_test(Dictionary &d);
	bool instruction_test(Dictionary &d);

	inline bool testing_unit(const std::string &instruction, const std::vector<unsigned char> &bytes, const Info::InstructionArchitecture &ia)
	{
		//d.set_flags(Info(ia));
		static size_t c16 = 0;
		static size_t c32 = 0;
		static size_t c64 = 0;

		if (ia == Info::InstructionArchitecture::IA16)
		{
			fa16 << std::hex << c16 << ": " << instruction << std::endl;
			c16 += bytes.size();
			return testing_unit(instruction, bytes, fb16);
		}
			
		if (ia == Info::InstructionArchitecture::IA32)
		{
			fa32 << std::hex << c32 << ": " << instruction << std::endl;
			c32 += bytes.size();
			return testing_unit(instruction, bytes, fb32);
		}
			
		if (ia == Info::InstructionArchitecture::IA64)
		{
			fa64 << std::hex << c64 << ": " << instruction << std::endl;
			c64 += bytes.size();
			return testing_unit(instruction, bytes, fb64);
		}
		
		return true;
	}

	bool testing_unit(const std::string &instruction, const std::vector<unsigned char> &bytes, std::ofstream &ofs);

	bool test_rel();
	bool test_ptr();
	bool test_r();
	bool test_sreg();
	bool test_imm();
	bool test_mm_xmm_ymm();

	bool test_rm8_ia16();
	bool test_rm8_ia32();
	bool test_rm8_ia64_without_prefix();
	bool test_rm8_ia64_with_prefix();

	bool test_rm16_ia16();
	bool test_rm16_ia32();
	bool test_rm16_ia64_without_prefix();
	bool test_rm16_ia64_with_prefix();

	bool test_rm32_ia32();
	bool test_rm32_ia64_without_prefix();
	bool test_rm32_ia64_with_prefix();

	bool test_rm64_ia64_with_prefix();

	bool test_m8_ia16();
	bool test_m8_ia32();
	bool test_m8_ia64();

	bool test_m16_ia16();
	bool test_m16_ia32();
	bool test_m16_ia64();

	bool test_m32_ia32();
	bool test_m32_ia64();

	bool test_m64_ia64();

	bool test_m128_ia32();
	bool test_m128_ia64();

	bool test_m256_ia32();
	bool test_m256_ia64();

	bool test_xmmm32_ia32();
	bool test_xmmm32_ia64();

	bool test_xmmm64_ia32();
	bool test_xmmm64_ia64();

	bool test_xmmm128_ia32();
	bool test_xmmm128_ia64();

	bool test_ymmm256_ia32();
	bool test_ymmm256_ia64();

	bool test_m1616_ia16();
	bool test_m1616_ia32();
	bool test_m1616_ia64();

	bool test_m1632_ia16();
	bool test_m1632_ia32();
	bool test_m1632_ia64();

	bool test_m1664_ia64();

	bool test_moffs();

	bool test_m32fp_ia16();
	bool test_m32fp_ia32();
	bool test_m32fp_ia64();

	bool test_m64fp_ia16();
	bool test_m64fp_ia32();
	bool test_m64fp_ia64();

	bool test_m80fp_ia16();
	bool test_m80fp_ia32();
	bool test_m80fp_ia64();

	bool test_m16int_ia16();
	bool test_m16int_ia32();
	bool test_m16int_ia64();

	bool test_m32int_ia16();
	bool test_m32int_ia32();
	bool test_m32int_ia64();

	bool test_m64int_ia16();
	bool test_m64int_ia32();
	bool test_m64int_ia64();

	bool test_st();

	bool test_mmm32_ia32();
	bool test_mmm32_ia64_without_prefix();
	bool test_mmm32_ia64_with_prefix();

	bool test_mmm64_ia64_with_prefix();

	bool test_al_xax();

	Dictionary d;	

	const char *instruction_file;
	const char *format_file;
};

