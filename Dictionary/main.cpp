#include "Dictionary.h"
#include "Info.h"

#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <array>
#include <exception>
#include <map>
#include <unordered_map>
#include <cassert>
#include <fstream>

using namespace std;

const char* instruction_file = ".\\..\\instructions.xml";
const char* format_file = ".\\..\\instruction_format.xml";
const char *err_file = ".\\..\\cerr.txt";
const std::string input_file = ".\\..\\test_op16.obj";

int main()
{
	std::ofstream out(err_file, std::ofstream::out);
	auto cerrbuf = std::cerr.rdbuf();
	std::cerr.rdbuf(out.rdbuf());
	// error
	
	Dictionary d(Info(Info::InstructionArchitecture::IA16), format_file, instruction_file);
	cout << "dictionary loaded" << endl;
	
	ifstream ifs(input_file, fstream::in | fstream::binary);
	byte_t byte;
	size_t count = 0;
	while (ifs.good())
	{
		try
		{
			if (count == 0xac)
			{
				std::cout << "here";
			}
			ifs.read((char *)&byte, 1);
			d.find(byte);
			++count;
		}
		catch (int)
		{
			cerr << "rollback: " << count << std::endl;
		}
		catch (std::exception ex)
		{
			cerr << ex.what();
		}
	}
	
	auto xxx = ifs.eof();
	auto xx = ifs.rdstate();
	ifs.close();

	std::cout << "Program converted" << std::endl;

	// error
	std::cerr.rdbuf(cerrbuf);

	int x;
	cin >> x;
}