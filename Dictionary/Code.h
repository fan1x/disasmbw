#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include "Condition.h"
#include "CodeTrie.h"

class Modul;
class Code;
class Dictionary;
struct Info;
// fwd decl

typedef std::vector<Modul *> arr_modul_t;
typedef std::vector<Code *> ArrCode;
typedef std::vector<CodeTrie *> trie_codes_t;
typedef std::unordered_map<std::string, Code *> codes_name_t;
typedef std::pair<CodeTrie::placement_t, size_t> opcode_data_t;
typedef std::vector<opcode_data_t> opcode_t;
typedef unsigned char byte_t;
typedef std::unordered_map<std::string, std::size_t> codings_values_t;

class Code
{
public:
	static const unsigned char BYTE_SIZE = 8;
	static const size_t OPCODE_BASE = 16;

	Code(const std::string &name, const codings_values_t &codings_values, size_t mode);
	Code();
	virtual ~Code(void);

	// insert code into codes trie
	void insert(trie_codes_t &codes, const opcode_t &opcode);
	// send byte to data and can we move
	// moved says whether we already moved or not
	virtual bool find(Dictionary &dict, byte_t byte) = 0;
	// returns true if whole instruction found
	// fills operands
	virtual bool finish(Dictionary &dictionary) = 0;
	// merging codes together
	virtual Code *merge(Code *code, size_t architecture) = 0;
	virtual void clear() = 0;
	// convert code to string
	virtual std::string str() const = 0;

	// name of code
	Condition opcode_condition;
	std::string name;
	codings_values_t codings_values;
	size_t mode;	// mode of processor
	const arr_modul_t::const_iterator start_modul;
};

