#pragma once

#include <cmath>

// hold informations about processor architecture
struct Info
{
	enum Architecture { A32 = 1, A64 = 2, A16 = 4, universal = 1 + 2 + 4, none = 0 };
	enum InstructionArchitecture { IA32 = 0, IA64 = 1, IA16 = 2 };

	Info(const InstructionArchitecture &ia) : arch(ia) {}
	Info() {}

	static const size_t arch_size = 3;

	InstructionArchitecture arch;
};

inline size_t to_architecture(size_t iarch)
{
	return static_cast<int>(std::pow(2, iarch));
}
