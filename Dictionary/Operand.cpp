#include "Operand.h"
#include "utils.h"
#include "Info.h"
#include "RulesTable.h"
#include "Modul.h"

#include <cassert>
#include <algorithm>
#include <iostream>

using myutils::to_hexstring;

// AbstractOperand

AbstractOperand::AbstractOperand(size_t mode, const Condition &cond1, const Condition &cond2) : mode(mode), condition(cond1), condition2(cond2) {}

AbstractOperand *AbstractOperand::merge(AbstractOperand *ao)
{
	MultiOperand *mo = new MultiOperand(mode, ao);
	mo->merge(this);
	return mo;
}

// Operand

Operand::~Operand(void) {}
Operand::Operand(size_t mode, const abstract_operands_t &aoperands, const Condition &condition, const Condition &condition2) :
	AbstractOperand(mode, condition, condition2), operands(aoperands), curr_op(std::begin(operands)) 
{}

bool Operand::eval(const codings_values_t &codings_values, codings_names_t &codings)
{
	for (; curr_op != end(operands); ++curr_op)
		if (!(*curr_op)->eval(codings_values, codings))
			break;

	if (curr_op == end(operands))
	{
		curr_op = begin(operands);
		return true;
	}

	std::for_each(curr_op, operands.cend(), [&codings_values, &codings](AbstractOperand *op)
	{
		op->eval(codings_values, codings);
	});

	return false;
}

std::string Operand::str() const
{
	std::string res;
	std::for_each(begin(operands), end(operands),[&res](AbstractOperand *ao)
	{
		res += ao->str();
	});

	return res;
}

// RulesOperand 

RulesOperand::RulesOperand(size_t mode) : 
	AbstractOperand(mode), rules_table(new RulesTable())
{
	rules_table->reset();
}

RulesOperand::RulesOperand(size_t mode, AbstractOperand *aoperand, const std::vector<Condition> &conditions) :
	RulesOperand(mode)
{
	rules_table->insert(conditions, aoperand);
}

bool RulesOperand::eval(const codings_values_t &codings_values, codings_names_t &codings)
{
	if (cached_operand)
		return cached_operand->eval(codings_values, codings);

	while (rules_table->curr_coding != end(rules_table->codings))
	{
		auto it = codings_values.find(rules_table->curr_coding->name());
		if (it == end(codings_values))
			break;

		rules_table->curr_node = rules_table->curr_node->child(it->second);
		if (!rules_table->curr_node)
			throw std::runtime_error("Not exists operand with value " + rules_table->curr_coding->name() + "=" + to_string(it->second));
		
		++rules_table->curr_coding;
	}

	if (!rules_table->curr_node)
		throw std::runtime_error("Not exists operand with value " + rbegin(rules_table->codings)->name());

	if (rules_table->curr_coding == end(rules_table->codings))
	{
		cached_operand = rules_table->curr_node->m_data;
		rules_table->reset();
		return cached_operand->eval(codings_values, codings);
	}

	const auto &c = rules_table->curr_coding;
	auto it = codings.find(c->modul);
	
	if (it == end(codings))
		codings.emplace( c->modul, std::move(std::vector<string>(1,c->coding)));
	else
		it->second.push_back(c->coding);

	return false;
}

void RulesOperand::insert(const std::vector<Condition> &conditions, AbstractOperand *ao)
{
	rules_table->insert(conditions, ao);
}

void RulesOperand::reset()
{
	if (cached_operand)
	{
		cached_operand->reset();
		cached_operand = nullptr;
	}
	else
		rules_table->reset();
}

// MultiOperand

MultiOperand::MultiOperand(size_t mode):AbstractOperand(mode), operands() {}
MultiOperand::MultiOperand(size_t mode, AbstractOperand *operand) : AbstractOperand(mode), operands(1, operand)
{
}

bool MultiOperand::eval(const codings_values_t &codings_values, codings_names_t &codings)
{
	if (cached_operand)
		return cached_operand->eval(codings_values, codings);

//	codings_names_t lmoduls;
//	(*begin(operands))->eval(codings_values, lmoduls);	// why?
	auto it = std::find_if(begin(operands), end(operands), [&codings_values](AbstractOperand *op)
	{
		return op->condition.satisfied(codings_values) && op->condition2.satisfied(codings_values);
	});

	if (it != end(operands))
	{
		cached_operand = *it;
		return cached_operand->eval(codings_values, codings);
	}

	throw std::runtime_error("Don't found right rule in multioperand");
}

// compares number of conditions
bool operator<(const AbstractOperand &ao1, const AbstractOperand &ao2)
{
	if (ao1.condition.coding.empty())
		return true;

	if (ao2.condition.coding.empty())
		return false;
	// both have at least 1 condition

	if (ao1.condition2.coding.empty())
		return true;

	if (ao2.condition2.coding.empty())
		return false;
	// both have 2 conditions
	return true;

}

AbstractOperand *MultiOperand::merge(AbstractOperand *ao)
{
	auto right_place = std::find_if(begin(operands), end(operands), [ao](AbstractOperand *operand)
	{
		return *operand < *ao;
	});

	operands.insert(right_place, ao);
	return this;
}

// VaribleOperand

VariableOperand::VariableOperand(AbstractOperand *operand, size_t mode, const Condition &cond1, const Condition &cond2) : operand(operand), AbstractOperand(mode, cond1, cond2) {}

// NameOperand

NameOperand::NameOperand(const std::string &str, size_t mode, const Condition &cond1, const Condition &cond2) : AbstractOperand(mode, cond1, cond2), value(str) {}

// CodingOperand

CodingOperand::CodingOperand(const moduls_names_t &moduls_names, const std::string &name, size_t mode, const Condition &cond1, const Condition &cond2) :
AbstractOperand(mode, cond1, cond2), name(name.substr(1)), value(0)
{
	if (name[0] != 'S' && name[0] != 'U')
		throw std::runtime_error("Undefined value specifier in operand: " + name);	

	bool sign = name[0] == 'S';

	auto strs = myutils::str_split(CodingOperand::name, ".");
	modul = strs[0];
	coding = strs[1];

	auto range = moduls_names.equal_range(modul);
	Coding *c;
	auto it = std::find_if(range.first, range.second, [&c, this](const std::pair<string, Modul * > &name_modul)
	{
		auto found = name_modul.second->codings_name_.find(coding);
		if (found != end(name_modul.second->codings_name_))
		{
			c = found->second;
			return true;
		}
		return false;
	});

	if (it == range.second)
		throw std::runtime_error("Not found coding with name " + coding + " in modul " + modul);
	
	switch (c->m_size)
	{
	case 8:
		size = sign ? 0 : 1;
		break;
	case 16:
		size = sign ? 2 : 3;
		break;
	case 32:
		size = sign ? 4 : 5;
		break;
	case 64:
		size = sign ? 6 : 7;
		break;
	default:
		throw std::runtime_error("Unsupported size of coding " + name);
	}

}

bool CodingOperand::eval(const codings_values_t &codings_values, codings_names_t &codings)
{
	auto it = codings_values.find(name);
	if (it != end(codings_values))
	{
		value = it->second;
		return true;
	}

	auto jt = codings.find(modul);
	if (jt == end(codings))
		codings.insert({ modul, { coding } });
	else
		jt->second.push_back(coding);

	return false;
}

std::string CodingOperand::str() const
{
	switch (size)
	{
	case 0:
//		return to_hexstring(static_cast<__int8>(value));
	case 1:	
		return to_hexstring(static_cast<unsigned __int8>(value));
	case 2:
//		return to_hexstring(static_cast<__int16>(value));
	case 3:
		return to_hexstring(static_cast<unsigned __int16>(value));
	case 4:
//		return to_hexstring(static_cast<__int32>(value));
	case 5:
		return to_hexstring(static_cast<unsigned __int32>(value));
	case 6:
//		return to_hexstring(static_cast<__int64>(value));
	default:
		return to_hexstring(value);
	}
}