#pragma once

#include <vector>
#include <bitset>
#include <map>
#include <string>
#include <unordered_map>

#include "utils.h"

// fwd decl
class Modul;
struct Coding;

typedef std::vector<Modul *> arr_modul_t;
typedef std::unordered_multimap<std::string, Modul *> moduls_names_t;
typedef std::unordered_map<std::string, size_t> codings_values_t;

// coding of one byte
struct ByteCoding
{
	ByteCoding();
	~ByteCoding();
	Coding *coding;
};

struct Coding
{
	Coding(void);
	Coding(const std::string &name, const std::string &byte, unsigned char size, char *def_val, char *opcode);

	Coding(const std::string &name, bool byte = true, unsigned char size = 1, char *def_val = nullptr, char *opcode = nullptr);
	Coding(const std::string &name, const std::string &byte, const std::string &size, char *def_val, char *opcode);
	Coding(const std::string &name, bool byte, const std::string &size, char *def_val, char *opcode);
	~Coding(void);

	inline size_t val() const { return myutils::to_number(m_value); }

	// return true if coding evaluated
	// returns value offset
	bool eval(const std::bitset<myutils::BYTE_SIZE> &byte, size_t byte_offset, size_t &value_offset);

	static inline Coding* byte_coding()
	{
		static ByteCoding bytecoding;
		return bytecoding.coding;
	}
	// moduls owner
	inline Modul *owner() const
	{
		return *owner_modul;
	}

	Coding *next;	// pointer to coding with same name in next modul
	std::string m_name;
	bool m_byte;
	unsigned char m_size;
	std::vector<bool> m_value;
	arr_modul_t::const_iterator owner_modul;
	char *default_value;
	char opcode;	// 0 - not in opcode, 1 - in opcode, 2 - invalid, at start ->generate
	bool code;
};

typedef std::pair<Coding *, size_t> condition_t;
typedef std::vector<Coding *> ArrCoding;
typedef std::map<std::string, Coding *> codings_name_t;

struct Condition
{
	struct CCoding
	{
		CCoding(const std::string &modul, const std::string &coding);
		CCoding(Coding *coding);
		CCoding(const std::string &name);
		CCoding();

		inline const std::string &name() const
		{
			return full_name;
		}

		inline bool empty() const
		{
			return modul == "";
		}

		std::string full_name;
		std::string modul;
		std::string coding;
	};


	Condition(void);
	Condition(Coding *coding, size_t value);
//	Condition(const std::string &name, size_t op2, const moduls_names_t &moduls);
	Condition(char *op1, char *op2);
	~Condition();

//	bool satisfied(const arr_modul_t &prefixes, const arr_modul_t::const_iterator &prefix_moduls_end, const arr_modul_t::const_iterator &next_modul) const;
	inline bool satisfied(const codings_values_t &codings_values) const
	{
		if (coding.empty())
			return true;

		auto it = codings_values.find(coding.name());
		if (it != end(codings_values))
			return it->second == value;

		return false;
	}

	CCoding coding;	// name
	size_t value;	// codings required value
	size_t size;	// size of value
};

typedef std::vector<Condition> condition_list_t;
