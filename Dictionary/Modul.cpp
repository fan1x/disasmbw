#include "Modul.h"
#include "Code.h"
#include "Dictionary.h"
#include "utils.h"
#include "CodeTrie.h"

#include <bitset>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <string>
#include <utility>
#include <cassert>


using myutils::to_number;
using std::bitset;
using std::vector;
using std::pair;

typedef vector<vector<size_t > > vvs_t;

Modul::Modul() : Code(), codings_start(0), enter_coding(0) {}

Modul::~Modul(void)
{
}

Modul::Modul(const std::string &name, size_t mode, const std::vector<bool> &enter_code, const codings_values_t &codings_value, const ArrCoding& codings):
Code(name, codings_value, mode), m_enter_code(enter_code), codings_start(0), enter_coding(0)
{
	unsigned char byte = 0;
	vector<Coding *> byte_coding;

	Coding *empty_coding = nullptr;
	for (Coding * coding : codings)
	{
		if (!coding->m_size)	// empty coding
		{
			m_codings.insert(begin(m_codings), coding);	// insert empty coding to front
			codings_name_[coding->m_name] = coding;
			continue;
		}
		codings_name_[coding->m_name] = coding;	// save name
		byte_coding.push_back(coding);	// save coding
		byte += coding->m_size;
		if (byte >= BYTE_SIZE)	// enough for byte
		{
			std::copy(rbegin(byte_coding), rend(byte_coding), std::back_inserter(m_codings));
			byte_coding.clear();
			byte = 0;
		}
	}
	if (!byte_coding.empty())
		std::copy(rbegin(byte_coding), rend(byte_coding), std::back_inserter(m_codings));

	m_curr_coding = begin(m_codings);
}

bool Modul::finish(Dictionary &dictionary)
{
	return true;
}

// merge this instruction with abstract instruction and returns result
Code *Modul::merge(Code *code, size_t architecture)
{
	if (auto *i = dynamic_cast<Instruction *>(code))
	{
		throw std::runtime_error("Can't merge modul and instruction.");
	}

	throw std::runtime_error("Can't merge modul and modul.");	
}

// clear operands
void Modul::clear()
{
	m_curr_coding = begin(m_codings);
}

bool Modul::find(Dictionary &dict, byte_t byte)
{
	if (dict.found)
	{
		while (m_curr_coding != end(m_codings) && ((*m_curr_coding)->m_size == 0 || codings_values.find(name + "." + (*m_curr_coding)->m_name) != end(codings_values)))
		{
			if (!(*m_curr_coding)->m_size)
				dict.codings_values.insert({ name + "." + (*m_curr_coding)->m_name, 0 });
			++m_curr_coding;
		}

		dict.codings_values.insert(begin(codings_values), end(codings_values));			

		if (m_curr_coding == end(m_codings))
		{
			clear();
			dict.curr_code = nullptr;
			dict.old_bytes.clear();	// clear bytes that used modul
			
			return false;
		}

		dict.found = false;
		return false;
	}
	byte_t counter = 0;
	bool trie_moving = true;
	byte_t value = byte;
	while (m_curr_coding != end(m_codings) && counter < BYTE_SIZE)
	{
		value = myutils::get_lower(byte, (*m_curr_coding)->m_size, counter);
		dict.codings_values.insert({ name + "." + (*m_curr_coding)->m_name, value });

		if (trie_moving && counter == dict.curr_trie->curr_placement().start)	// same as requrired start
		{
			if ((*m_curr_coding)->m_size != dict.curr_trie->curr_placement().offset ||	// moving but not same size
				!dict.curr_trie->unprocess_find(value))	// value is dead end
			{
				dict.codings_values.clear();	// clear codings value don't found this modul again
				dict.rollback();
				return false;
			}

			trie_moving = !dict.curr_trie->curr_placement().new_byte;
			if (dict.curr_trie->curr_data())	// if found data, change it
			{
				dict.curr_code = dict.curr_trie->curr_data();
				break;
			}
		}
		counter += (*m_curr_coding)->m_size;
		++m_curr_coding;
	}
	
	assert(counter == BYTE_SIZE);

	if (m_curr_coding == end(m_codings))	// all codings found -> can finish modul
	{
		clear();
		dict.old_bytes.clear();	// clear old_bytes for find this modul
		if (dict.curr_code == this)	// maybe we changed it
			dict.curr_code = nullptr;
		return false;
	}

	return false;	
}

bool Modul::eval(codings_values_t &cv, byte_t byte)
{
	byte_t counter = 0;
	while (counter < BYTE_SIZE && m_curr_coding != end(m_codings))
	{
		if (!(*m_curr_coding)->eval(byte, counter, codings_start))
			break;

		string str(name);
		str += ".";
		str += (*m_curr_coding)->m_name;

		cv.emplace(std::move(str), std::move((*m_curr_coding)->val()));
		counter += (*m_curr_coding++)->m_size;
	}

	if (m_curr_coding == end(m_codings))
	{
		m_curr_coding = begin(m_codings);
		codings_start = 0;
		return true;
	}

	return false;
}

size_t Modul::find(const std::string &name)
{
	return codings_name_.find(name)->second->val();
}
// returns coding or nullptr if not found
// name is only name of coding

bool Modul::find_coding(const std::string &name)
{
	auto it = codings_name_.find(name);
	if (it == codings_name_.end())
	{
		auto jt = codings_values.find(Modul::name + "." + name);
		return jt != end(codings_values);
	}
	return true;
}

ArrCoding revert_coding(const ArrCoding &coding, size_t move)
{
	vector<Coding *> buff;
	ArrCoding res;
	std::for_each(begin(coding), end(coding), [&](Coding *c)
	{
		buff.push_back(c);
		move += c->m_size;
		if (move == BYTE_SIZE)
		{
			std::reverse_copy(begin(buff), end(buff), std::back_inserter(res));
			move = 0;
			buff.clear();
		}
	});

	if (!buff.empty())
		std::reverse_copy(begin(buff), end(buff), std::back_inserter(res));

	return std::move(res);
}

std::vector<bool> revert_bvector(const std::vector<bool> &ec)
{
	vector<bool> buff, res;
	for (size_t i = 0; i < ec.size(); ++i)
	{
		buff.push_back(ec[i]);
		if (buff.size() == BYTE_SIZE)
		{
			std::reverse_copy(begin(buff), end(buff), std::back_inserter(res));
			buff.clear();
		}
	}

	if (!buff.empty())
		std::reverse_copy(begin(buff), end(buff), std::back_inserter(res));

	return std::move(res);
}

std::vector<std::pair<opcode_t, codings_values_t > > byte_to_opcode_t(const std::vector<std::pair<std::vector<bool >, codings_values_t > > &vvb)
{
	vector<pair<opcode_t, codings_values_t > > res(vvb.size());
	std::transform(begin(vvb), end(vvb), begin(res), [](const pair<std::vector<bool>, codings_values_t > &vb)
	{
		auto bytes = myutils::to_bytes(vb.first);
		opcode_t ops(bytes.size());
		std::transform(begin(bytes), end(bytes), begin(ops), [](myutils::BYTE byte)
		{
			return opcode_data_t(CodeTrie::placement_t(0, BYTE_SIZE, true), byte);
		});
		return make_pair(ops, vb.second);
	});
	
	return std::move(res);
}



// gen all values with cond
// currently only for 1 byte modul
std::vector<size_t > Modul::gen_values(const std::vector<Condition> &conditions) const
{
	vector<size_t > res(1, 0);

	size_t shift = 0;
	for (Coding *coding : m_codings)
	{
		auto it = std::find_if(begin(conditions), end(conditions), [coding](const Condition &cond)
		{
			return cond.coding.coding == coding->m_name;
		});

		if (it == end(conditions))	// not found
		{
			vector<size_t > tmp(res.size() * (1 << coding->m_size));
			auto jt = begin(res);
			size_t i = 0;
			std::generate(begin(tmp), end(tmp), [&jt, &res, shift, &i]()
			{
				if (jt == end(res))
				{
					jt = begin(res);
					++i;
				}

				return *jt++ + (i << shift);
			});

			res = std::move(tmp);			
		}
		else
		{
			vector<size_t > tmp;
			auto jt = begin(res);
			while (it != end(conditions) && it->coding.coding == coding->m_name)
			{
				size_t last_size = tmp.size();
				tmp.resize(tmp.size() + res.size());
				std::generate(begin(tmp) + last_size, end(tmp), [&jt, shift, &it]()
				{
					return *jt++ + (it->value << shift);
				});
				++it;	
				jt = begin(res);
			}

			res = std::move(tmp);
		}		
		shift += coding->m_size;
	}

	return std::move(res);
}

void generate_all(size_t size, const std::string &name, std::vector<std::pair<std::vector<bool >, codings_values_t > > &vvb)
{
	typedef vector<pair<vector<bool >, codings_values_t > > vvb_t;

	vector<bool> vb(size, false);	// new value, that would be added
	size_t vb_value = 0;
	vvb_t cvvb(vvb);	// copy of old vvb			
	vvb.clear();

	size_t max = static_cast<size_t>(std::pow(2, size));
	vvb.resize(cvvb.size() * max);	// resize, that all values fit

	for (size_t i = 0; i < max; ++i, myutils::inc_bvector(vb), ++vb_value)
		for (size_t j = 0; j < cvvb.size(); ++j)
		{
			std::copy(begin(cvvb.at(j).first), end(cvvb.at(j).first), std::back_inserter(vvb.at(cvvb.size() * i + j).first));	// copy old
			std::copy(begin(vb), end(vb), std::back_inserter(vvb.at(cvvb.size() * i + j).first));
			vvb.at(cvvb.size() * i + j).second.insert(begin(cvvb.at(j).second), end(cvvb.at(j).second));	// copy old value
			vvb.at(cvvb.size() * i + j).second.insert({ name, vb_value });	// insert new coding name and its value into
		}
}

void generate_all(byte_t start, byte_t size, bool new_byte, std::string &name, std::vector<opcode_codings_pair> &voc)
{
	typedef std::vector<opcode_codings_pair> voc_t;

	voc_t cvoc(voc);	// copy of old vvs			
	voc.clear();

	size_t max = static_cast<size_t>(std::pow(2, size));
	voc.resize(cvoc.size() * max);	// resize, that all values fit

	for (size_t i = 0; i < max; ++i)
	{
		opcode_data_t op_data(CodeTrie::placement_t(start, size, new_byte), i);
		for (size_t j = 0; j < cvoc.size(); ++j)
		{
			auto &roc = voc.at(cvoc.size() * i + j);
			roc = cvoc.at(j);	// copy whole element
			roc.first.push_back(op_data);	// add new number
			roc.second.insert({ name, i });	// insert name of value into set
		}
	}
}

void add_value_to_all(byte_t start, byte_t size, bool new_byte, const std::string &name, size_t value, std::vector<opcode_codings_pair> &voc)
{
	opcode_data_t op_data(CodeTrie::placement_t(start, size, new_byte), value);
	std::for_each(begin(voc), end(voc), [&op_data, &name, value](opcode_codings_pair &oc)
	{
		oc.first.push_back(op_data);
		oc.second.insert({ name, value });
	});
}

void add_value_to_all(size_t value, byte_t size, const std::string &name, std::vector<std::pair<std::vector<bool >, codings_values_t > > &vvb)
{
	auto val = myutils::to_bvector(value, static_cast<size_t>(size));
	std::for_each(begin(vvb), end(vvb), [&val, value, &name](pair<vector<bool >, codings_values_t > &vb)
	{
		std::copy(begin(val), end(val), std::back_inserter(vb.first));	// copy bools
		vb.second.insert({ name, value });	// insert value
	});
}

typedef std::vector<std::pair<std::string, Condition> > vname_condition_t;

inline vname_condition_t::const_iterator find_condition(const vname_condition_t &vc, const std::string &modul, const std::string &coding)
{
	return std::find_if(std::begin(vc), std::end(vc), [&modul, &coding](const std::pair<string, Condition> &name_condition)
	{
		const Condition::CCoding &c = name_condition.second.coding;
		return c.coding == coding && c.modul == modul;
	});
}

std::vector<opcode_codings_pair> Modul::get_opcodes(const std::vector<std::pair<std::string, Condition> > &vc)
{
	using std::vector;
	using std::set;
	using std::string;
	using std::pair;


	typedef vector<pair<vector<myutils::BYTE >, codings_values_t > > vvbyte_t;
	typedef vector<pair<vector<bool >, codings_values_t > > vvb_t;

	int generate = BYTE_SIZE - m_enter_code.size();	// generate only smaller than byte
	vvb_t vvb(1, { {}, {} });// (1, { revert_bvector(m_enter_code), {} });
	std::vector<opcode_codings_pair> res(1, { {}, {} });
	if (!generate && m_enter_code.size())	// enter coding is exactly byte
	{
		res = byte_to_opcode_t(vvb_t(1, { { m_enter_code }, {} }));
	}

	bool new_byte = true;
	byte_t start = 0;
	Modul * const this_modul = this;
	auto ecode(m_enter_code);
	std::for_each(begin(m_codings), end(m_codings), [&vc, &vvb, &res, &generate, &start, this_modul, &new_byte, &ecode](Coding *coding)
	{
		if (!coding->m_size)
			return;

		if (generate > 0)
		{
			if (coding->opcode == 1)
			{
				auto it = find_condition(vc, this_modul->name, coding->m_name);
				
				if (it != end(vc))
				{
					size_t value = it->second.value;
					add_value_to_all(value, coding->m_size, this_modul->name + "." + coding->m_name, vvb);
				}
				else if (coding->default_value)
				{
					size_t value = myutils::to_number(coding->default_value);
					add_value_to_all(value, coding->m_size, this_modul->name + "." + coding->m_name, vvb);
				}
				else
				{
					generate_all(coding->m_size, this_modul->name + "." + coding->m_name, vvb);
				}
			}
			else
			{
				generate_all(coding->m_size, this_modul->name + "." + coding->m_name, vvb);
			}

			if (--generate == 0)
			{
				// copy at hightest bytes enter codes for all vals
				std::for_each(begin(vvb), end(vvb), [&ecode](vvb_t::value_type &val)
				{
					std::for_each(cbegin(ecode), cend(ecode), [&val](bool b)
					{
						val.first.push_back(b);
					});
				});
				res = byte_to_opcode_t(vvb);
			}

			return;	// for_each
		}

		if (coding->opcode == 1)
		{
			auto it = find_condition(vc, this_modul->name, coding->m_name);

			if (it != end(vc) || coding->default_value)
			{
				size_t value = it != end(vc) ? it->second.value : myutils::to_number(coding->default_value);
				add_value_to_all(start, coding->m_size, new_byte, this_modul->name + "." + coding->m_name, value, res);
			}
			else
			{
				generate_all(start, coding->m_size, new_byte, this_modul->name + "." + coding->m_name, res);
			}
		}
		else if (coding->opcode == 2)
		{
			generate_all(start, coding->m_size, new_byte, this_modul->name + "." + coding->m_name, res);
		}

		start += coding->m_size;
		new_byte = false;
		if (start >= BYTE_SIZE)
		{
			new_byte = true;
			start %= BYTE_SIZE;
		}
	});

	return std::move(res);
} 

CodeTrie::placement_t Modul::placement(Coding *coding) const
{
	size_t coding_end = 0;
	std::find_if(begin(m_codings), end(m_codings), [&coding_end, coding](Coding *c)
	{
		coding_end += c->m_size;
		return c == coding;
	});

	byte_t start = (coding_end - coding->m_size) % BYTE_SIZE;

	return std::move(CodeTrie::placement_t(start, coding->m_size, true));
}

CodeTrie::placement_t placement(Coding *coding)
{
	return (*coding->owner_modul)->placement(coding);
}

bool modul_match_prefixes(arr_modul_t::const_iterator &modul, const arr_modul_t &prefixes)
{
	for (std::string modul_name = (*modul)->name; (*modul)->name == modul_name; ++modul)
	{
		auto found = std::find(begin(prefixes), end(prefixes), *modul);
		if (found != end(prefixes))
			return true;
	}
	return false;
}

bool find_coding(const moduls_names_t &modul_names, const std::string &name)
{
	vector<string> names = myutils::str_split(name, ".");
	auto modul_range = modul_names.equal_range(names[0]);
	auto modul_it = modul_range.first;

	return std::any_of(modul_range.first, modul_range.second, [&names](const std::pair<string, Modul * > &name_modul)
	{
		return name_modul.second->find_coding(names[1]);
	});
}

