#include <algorithm>
#include <cmath>

#include "RulesTable.h"
#include "utils.h"
#include "Operand.h"
#include "Modul.h"

RulesTable::RulesTable(void):
	root_(nullptr)
{
}

RulesTable::~RulesTable(void)
{
	delete root_;
}

void RulesTable::insert(const std::vector<Condition> &condition_list, AbstractOperand *operand)	
{
	auto cit_c = condition_list.cbegin();

	if (root_ == nullptr)
	{
		root_ = new operand_node_t(cit_c == condition_list.cend() ? 0 : static_cast<size_t>(std::pow(2, cit_c->size)));		
		for (const auto &condition : condition_list)	// copy codings
			codings.push_back(condition.coding);

		reset();	// reset structure pointers
	}

	if (cit_c == condition_list.cend())	// empty condition list
	{
		root_->m_data = operand;
		return;
	}

	operand_node_t *node = root_;
	operand_node_t *prev = root_;	
	while(cit_c != condition_list.cend() && node) // go on existing path
	{
		prev = node;
		node = node->child(cit_c->value);
		++cit_c;
	}

	if (cit_c != condition_list.cend() || !node)	// not at the end or for last condition not exists node
	{
		--cit_c;	// coding is 1 step ahead
		while (cit_c != condition_list.cend())
		{
			prev = prev->insertNew(cit_c->value, cit_c == end(condition_list) - 1 ? static_cast<size_t>(0) : static_cast<size_t>(std::pow(2, (cit_c + 1)->size)));
			++cit_c;
		}
	}
	else
		prev = node;

	if (prev->m_data)
		prev->m_data = prev->m_data->merge(operand);
	else
		prev->m_data = operand;
}

void RulesTable::reset()
{
	curr_node = root_;
	curr_coding = codings.begin();
}
