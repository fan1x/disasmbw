#pragma once

#include "Condition.h"

#include <string>
#include <list>
#include <array>
#include <set>
#include <map>
#include <unordered_map>
#include <utility>
#include <cassert>
#include <algorithm>

// fwd decl
class Modul;
class RulesTable;
struct Info;

typedef std::vector<Modul *> arr_modul_t;
typedef std::unordered_map<std::string, std::vector<std::string > > codings_names_t;
typedef std::unordered_map<std::string, size_t > codings_values_t;

using std::to_string;

class AbstractOperand
{
public:
	AbstractOperand(size_t mode = 7, const Condition &cond1 = Condition(), const Condition &cond2 = Condition());//Info::Architecture::universal);

	virtual std::string str() const = 0;
	virtual bool eval(const codings_values_t &codings_values, codings_names_t &codings) = 0;
	virtual AbstractOperand  *merge(AbstractOperand *ao);
	virtual void reset() = 0;	// for exception 

	size_t mode;	// mode in which could be operand used
	Condition condition;	// 1st condition for operand
	Condition condition2;	// 2nd condition for operand
	static const size_t COPERAND = 4;
};

typedef std::vector<AbstractOperand *> abstract_operands_t;

class Operand: public AbstractOperand
{
public:
	Operand(size_t mode, const abstract_operands_t &aoperands, const Condition &condition, const Condition &condition2);
	~Operand();

	bool eval(const codings_values_t &codings_values, codings_names_t &codings) override;
	std::string str() const override;
	inline void reset() override
	{
		std::for_each(std::cbegin(operands), curr_op, [](AbstractOperand *ao)
		{
			ao->reset();
		});
		assert(curr_op != std::cend(operands));
		(*curr_op)->reset();	
		curr_op = begin(operands);
	}
private:
	abstract_operands_t operands;	// operand is composed from these operand units
	mutable abstract_operands_t::const_iterator curr_op;	// current operand which waits for evaluating
	mutable std::string value;	// value of whole operand - set after full evaluation
};

// operand that stores ruletable with AbstractOperand at the end
// operands differs in coding
class RulesOperand : public AbstractOperand
{
public:
	RulesOperand(size_t mode);
	RulesOperand(size_t mode, AbstractOperand *aoperand, const std::vector<Condition> &conditions);

	bool eval(const codings_values_t &codings_values, codings_names_t &codings) override;
	inline std::string str() const override
	{
		std::string res = cached_operand->str();
		cached_operand = nullptr;
		return res;
	}
	void reset() override;
	void insert(const std::vector<Condition> &conditions, AbstractOperand *ao);
private:
	void merge(RulesOperand *);
	RulesTable * rules_table;	// tables for every mode
	mutable AbstractOperand *cached_operand;
};

typedef std::vector<Operand * > arr_operands_t;
typedef std::tuple<std::string, std::string, char *, char *, char *, char *> operand_rules_t;

// MultiOperand contains multiple operand that differ in target
class MultiOperand: public AbstractOperand
{
public:
	MultiOperand(size_t mode);
	MultiOperand(size_t mode, AbstractOperand *operand);

	bool eval(const codings_values_t &codings_values, codings_names_t &codings) override;
	inline std::string str() const override
	{
		std::string res = cached_operand->str();
		cached_operand = nullptr;
		return res;
	}
	inline void reset() override
	{
		if (cached_operand)
		{
			cached_operand->reset();
			cached_operand = nullptr;
		}
	}

	AbstractOperand *merge(AbstractOperand *ao) override;	// insert operand at the right place sorted after number of coding
private:
	// operands for all targets
	abstract_operands_t operands;	// target operands, for every mode
	mutable AbstractOperand *cached_operand;
};

// operand that stores pointer other operand
// is not used
class VariableOperand: public AbstractOperand
{
public:
	VariableOperand(AbstractOperand *operand, size_t mode, const Condition &cond1, const Condition &cond2);

	inline bool eval(const codings_values_t &codings_values, codings_names_t &codings) override
	{
		return operand->eval(codings_values, codings);
	}
	inline std::string str() const override
	{
		return operand->str();
	}
	inline void reset() override
	{
		operand->reset();
	}
private:
	AbstractOperand *operand;
};

// operand that stores string
class NameOperand: public AbstractOperand
{
public:
	NameOperand(const std::string &str, size_t mode, const Condition &cond1, const Condition &cond2);
	
	inline bool eval(const codings_values_t &codings_values, codings_names_t &codings) override
	{
		return true;
	}

	inline std::string str() const override
	{
		return value;
	}

	inline void reset() override
	{}
private:
	std::string value;
};

typedef unsigned char byte_t;

// operand that stores pointer to coding
// used for exact value of some coding (immediate)
class CodingOperand: public AbstractOperand
{
public:
	CodingOperand(const moduls_names_t &moduls_names, const std::string &coding, size_t mode, const Condition &cond1, const Condition &cond2);

	bool eval(const codings_values_t &codings_values, codings_names_t &codings) override;
	std::string str() const override;
	inline void reset() override
	{}
private:
	std::string coding;	
	std::string modul;
	std::string name;
	byte_t size;	
	size_t value;
};

typedef std::array<std::pair<std::string, AbstractOperand *>, AbstractOperand::COPERAND> operands_t;
typedef std::array<AbstractOperand *, AbstractOperand::COPERAND> operands_array_t;
typedef std::unordered_map<std::string, AbstractOperand *> operands_names_t;

