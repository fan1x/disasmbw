#include "Condition.h"

#include <algorithm>
#include <string>

using std::vector;
using std::string;

#include "Modul.h"

Coding::Coding(void) {}

Coding::~Coding(void) {}

Coding::Coding(const std::string &name, bool byte, unsigned char size, char *def_val, char *opcode) :
m_byte(byte), m_name(name), m_size(byte ? size*myutils::BYTE_SIZE : size), m_value(vector<bool>(m_size)), next(nullptr), default_value(def_val), opcode(opcode ? (opcode[0] == '1' ? 1 : 0) : 2)
{}

Coding::Coding(const std::string &name, bool byte, const std::string &size, char *def_val, char *opcode) :
Coding(name, byte, std::stoi(size), def_val, opcode)
{}

Coding::Coding(const std::string &name, const std::string &byte, unsigned char size, char *def_val, char *opcode) :
Coding(name, byte == "byte", size, def_val, opcode)
{}


Coding::Coding(const std::string &name, const std::string &byte, const std::string &size, char *def_val, char *opcode) :
Coding(name, byte, std::stoi(size), def_val, opcode)
{}

bool Coding::eval(const std::bitset<myutils::BYTE_SIZE> &byte, size_t byte_offset, size_t &value_offset)
{
	size_t up_border = byte_offset + std::min<size_t>(m_size, myutils::BYTE_SIZE);
	for (; byte_offset < up_border; ++value_offset, ++byte_offset)
	{
		m_value[value_offset] = byte[byte_offset];
	}

	if (m_size == value_offset)	// we filled whole size
	{
		value_offset = 0;
		return true;
	}

	return false;
}

// ByteCoding

ByteCoding::ByteCoding() :coding(new Coding("BYTE"))
{}

ByteCoding::~ByteCoding()
{
	delete coding;
}

// CCoding

Condition::CCoding::CCoding(const std::string &modul, const std::string &coding):
modul(modul), coding(coding), full_name(modul)
{
	full_name += ".";
	full_name += coding;
}

Condition::CCoding::CCoding(const std::string &name)
{
	auto strs = myutils::str_split(name, ".");
	if (strs.size() == 2)
	{
		modul = strs[0];
		coding = strs[1];
		full_name = strs[0];
		full_name += ".";
		full_name += strs[1];
	}
	else
		full_name = modul = coding = "";
}

Condition::CCoding::CCoding():
CCoding("", "")
{}

Condition::CCoding::CCoding(Coding *coding) :
CCoding((*coding->owner_modul)->name, coding->m_name)
{}

// Condition


Condition::Condition(void) : coding(), value(0), size(0) {}

Condition::Condition(Coding *coding, size_t value) :
coding((*coding->owner_modul)->name, coding->m_name), value(value)
{}
/*
Condition::Condition(const std::string &name, size_t op2, const moduls_names_t &moduls) :
coding(name), value(op2)
{
}
*/
Condition::Condition(char *op1, char *op2) :
coding(op1 ? CCoding(op1) : CCoding()), value(op2 ? std::stoi(op2, 0, 2) : 0), size(op2 ? std::string(op2).size() : 0)
{}

Condition::~Condition(void) {}