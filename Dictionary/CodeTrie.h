#pragma once

#include "DNode.h"

#include <vector>
#include <algorithm>
#include <cassert>

// fwd decl
class Code;

template<class T>
struct Placement
{
	static const T MAX_SIZE = sizeof(T) * 8;

	Placement(T start, T offset, bool new_byte) :
		start(start), offset(offset), new_byte(new_byte)
	{}

	Placement() :
		Placement(0, 0, false)
	{}

	inline bool operator!() const
	{
		return offset == 0;
	}

	inline bool operator==(const Placement &placement) const
	{
		return start == placement.start && offset == placement.offset && new_byte == placement.new_byte;
	}

	inline bool operator!=(const Placement &placement) const
	{
		return !operator==(placement);
	}

	// compares, whether is given placemet upperset of current
	inline bool operator<(const Placement &placement) const
	{
		return placement.start <= start && placement.offset >= offset;
	}

	inline bool max_size() const
	{
		return offset == MAX_SIZE;
	}

	static inline Placement &byte_placement()
	{
		static Placement byte(0, MAX_SIZE, true);
		return byte;
	}

	T start;
	T offset;
	bool new_byte;
};


// merge placements together
// merge until goes after
template<class T>
Placement<T> merge(const std::vector<Placement<T > > &placements)
{
	typedef Placement<T > placement_t;
	T start = placements.at(0).start;
	bool new_byte = placements.at(0).new_byte;
	T offset;
	for (const placement_t &pl : placements)
	{
		offset += pl.offset;
	}

	return std::move(placement_t(start, offset, new_byte));
}

// given placement must be smaller
// split after given placement
template<class T>
std::vector<std::pair<Placement<T >, size_t > > splite(const Placement<T > &placement, size_t value, const Placement<T > &splitter)
{
	assert(splitter < placement);
	std::vector<std::pair<Placement<T >, size_t > > res;

	T new_offset = splitter.start - placement.start;
	T mask, val;
	if (new_offset)
	{
		mask = (1 << new_offset) - 1;
		val = value & mask;
		res.push_back({ Placement<T >(placement.start, new_offset, placement.new_byte), val });
	}

	mask = (1 << splitter.offset) - 1;
	val = value & mask;
	val >>= splitter.start;
	res.push_back({ splitter, val });
	new_offset = placement.offset - splitter.offset - splitter.start;
	if (new_offset)
	{
		mask = (1 << placement.offset) - 1;
		val = value & mask;
		val >>= splitter.start + splitter.offset;
		res.push_back({ Placement<T >(splitter.start + splitter.offset, new_offset, false), val });
	}

	return std::move(res);
}

template<class Data, class PlacementType>
class Trie
{
public:
	static const size_t FIRST_SIZE = 256;
	typedef Data data_t;
	typedef Placement<PlacementType> placement_t;

	Trie(size_t size = FIRST_SIZE):
		root(new trie_node_t(node_data_t(), size)), curr(root), piece(false)
	{
	}
	~Trie()
	{
		delete root;
	}

	inline data_t &curr_data()
	{
		return curr->m_data.second;
	}

	inline const data_t &curr_data() const
	{
		return curr->m_data.second;
	}

	inline placement_t &curr_placement()
	{
		return curr->m_data.first;
	}

	inline const placement_t &curr_placement() const
	{
		return curr->m_data.first;
	}

	inline size_t curr_size() const
	{
		return curr->size();
	}

	inline void reset()
	{
		curr = root;
		piece = false;
	}

	// try find value according placement
	// if not found, return false
	bool unprocess_find(PlacementType &value) const
	{
		if (value < curr->size() && curr->child(value))
		{
			curr = curr->child(value);
			return true;
		}

		return false;
	}
	
	bool find(PlacementType &value) const
	{
		bool res;
		do
		{
			res = find_one(value);
		} while (res && curr_placement().offset && !curr_placement().new_byte);

		return res;
	}

	bool find(const placement_t &where, PlacementType value) const
	{
		if (curr_placement() != where || !curr->child(value))
			return false;

		curr = curr->child(value);
		return true;
	}

	// insert new code at position in condition + move current to new
	template<class Iterator>
	bool find(const Iterator &begin, const Iterator &end) const
	{
		curr = root;
		return std::all_of(begin, end, [this](const std::pair<placement_t, size_t> &placement_value)
		{
			return find(placement_value.first, placement_value.second);
		});
	}

	// insert new code at position in condition + move current to new
	bool insert(const Data &data, const placement_t &where, PlacementType value)
	{
		if (!curr_placement())
		{
			curr_placement() = where;
			curr->resize(std::pow(2, where.offset));
			curr = curr->insertNew(value, node_data_t(placement_t(), data));
			return true;
		}

		if (curr_placement() == where)	// same placement
		{
			if (curr->child(value))	// already exists
				return false;

			curr = curr->insertNew(value, node_data_t(placement_t(), data));
			return true;
		}

		return false;
	}
	
	template<class Iterator>
	bool insert(const Data &data, const Iterator &begin, const Iterator &end, bool reset = true)
	{
		if (reset)
			Trie<Data, PlacementType >::reset();

		auto it = std::find_if_not(begin, end, [this](const std::pair<placement_t, size_t> &placement_value)
		{
			return find(placement_value.first, placement_value.second);
		});

		if (it == end)
			return false;

		it = std::find_if_not(it, end - 1, [this](const std::pair<placement_t, size_t> &placement_value)
		{
			return insert(Data(), placement_value.first, placement_value.second);
		});

		if (it == end - 1)
		{
			if (insert(data, it->first, it->second))
				return true;
		}	

		throw std::runtime_error("Can't insert data into trie");
		// could not insert, try split
		if (it->first < curr_placement())	// given placement is inside
		{
			split(curr, it->first);			 
		}
		else if (curr_placement() < it->first)	// our placement is inside
		{
			std::vector<std::pair<placement_t, size_t > > res = splite(it->first, it->second, curr_placement());
			std::move(it + 1, end, std::back_inserter(res));	// copy left values
			insert(data, std::begin(res), std::end(res), false);	// try find again
		}
		else
			throw std::runtime_error("Can't insert data into trie");
	}
	mutable bool piece;	// take only piece of byte
private:
	typedef std::pair<placement_t, Data> node_data_t;
	typedef DNode<node_data_t> trie_node_t;

	bool find_one(PlacementType &value) const
	{
		if (!curr_placement())
			return false;

		if (curr_placement().offset == placement_t::MAX_SIZE)
		{
			if (curr->child(value))
			{
				curr = curr->child(value);
				return true;
			}
			return false;
		}

		piece = true;
		PlacementType  mask = (1 << (curr_placement().offset + curr_placement().start)) - 1;
		PlacementType real_value = value & mask;
		real_value >>= curr_placement().start;
		if (curr->child(real_value))
		{
			curr = curr->child(real_value);
			return true;
		}
		return false;
	}

	// placement must be inside node's placement
	bool split(trie_node_t *node, const placement_t &pl)
	{
		placement_t &old_pl = node->m_data.first;
		if (pl.start < old_pl.start || pl.offset > old_pl.offset || pl == old_pl || !pl)	// not inside or same or empty
			return false;

		std::vector<PlacementType> splits;
		PlacementType diff = pl.start - old_pl.start;
		if (diff)
			splits.push_back(diff);
		splits.push_back(pl.offset);
		diff = old_pl.offset - pl.offset;
		if (diff)
			splits.push_back(diff);

		std::vector<trie_node_t *> new_children(1 << splits.at(0));
		for (PlacementType i = 0; i < node->size(); ++i)
		{
			if (trie_node_t *n = node->child(i))
			{
				PlacementType mask = (1 << splits.at(0)) - 1;
				PlacementType new_value = i & mask;
				trie_node_t *new_node = new trie_node_t(node_data_t(placement_t(old_pl.start, splits.at(0), old_pl.new_byte), Data()));
				new_children.at(new_value) = new_node;				
				// first level

				PlacementType shift = splits.at(0);
				for (PlacementType j = 1; j < splits.size(); ++j)
				{
					new_node->resize(1 << splits.at(j));
					mask = (1 << (splits.at(j) + shift)) - 1;
					mask >>= shift;
					new_value = i & mask;
					new_node = new_node->insertNew(new_value, node_data_t(placement_t(shift, splits.at(j), false), Data()));
					shift += splits.at(j);
				}

				*new_node = *node;	// copy
			}
		}

		node->move_children(std::move(new_children));
		node->m_data = node_data_t(placement_t(old_pl.start, splits.at(0), old_pl.new_byte), Data());
		return true;
	}

	mutable trie_node_t *curr;
	trie_node_t *root;
};

typedef Trie<Code *, unsigned char> CodeTrie;

