#pragma once

#include "DNode.h"
#include "utils.h"

#include <vector>
#include <cassert>
#include <ostream>
#include <iostream>
#include <algorithm>
#include <functional>

using std::for_each;
using std::ostream;

template<typename Data>
class Trie
{
public:
	typedef DNode<Data> TrieNode;

	/// ctor
	Trie(size_t length = 256)
	{
		m_pRoot = new TrieNode(length);
		m_size = length;
		mp_curr_node = m_pRoot;
	}
	/// dtor
	~Trie(void)
	{
		delete m_pRoot;
		m_pRoot = nullptr;
	}

	/// insert data to Trie going through path. 
	/** If path not exists it creates proper one
	\param data any data
	\param path for finding data
	*/	
	void insert(Data data, const std::vector<myutils::BYTE> &path)
	{
		assert(m_pRoot != nullptr);

		TrieNode *pNode = m_pRoot;
		TrieNode *pPrev = m_pRoot;
		size_t i = 0;
		for(; i < path.size() && pNode; ++i)
		{
			pPrev = pNode;
			pNode = pNode->child(path[i]);	
		}
	
		for(; i < path.size(); ++i)	// not whole path
		{
			pPrev = pPrev->insertNew(path[i-1]);
			pNode = pPrev->child(path[i]);
		}

		if (pNode == 0)	// last from path was not included
		{
			pNode = pPrev->insertNew(path[i-1]);
		}
	
		pNode->m_data = data;
	}

	/// find the further node on path from pNode.
	/**
	\param path path we're trying to find
	\param pNode node from we are trying to find next, also the last node on path is returned in this parameter
	\return value whether we find the whole path
	*/
	bool find(const std::vector<myutils::BYTE> &path, TrieNode *&pNode)
	{
		m_curr_path.insert(m_curr_path.end(), path.begin(), path.end());	// save path

		TrieNode *pNext = pNode;
		for(size_t i = 0; i < path.size() && pNext; ++i)
		{
			pNode = pNext;
			pNext = pNext->child(path[i]);
		}

		if (pNext)
		{
			pNode = pNext;
			return true;
		}
	
		return false;
	}

	/* find next child from node 
	returns true if found node with data
	returns node	
	*/
	bool find(myutils::BYTE path, TrieNode *&node)
	{
		if (node == nullptr)
			return false;

		if (node->m_data != nullptr)	// we have Modul, Instruction returns true to move()
			return node->m_data->move(path);	// can we move?

		assert(node->child(path) != nullptr);
		node = node->child(path);

		if (node->m_data == nullptr)
			return false;

		return node->m_data->move(path);
	}

	// find next child from node
	// returns true if found node with data
	// returns node
	bool find(myutils::BYTE path)
	{
		if (mp_curr_node == nullptr)
			return false;

		if (mp_curr_node->m_data != nullptr)	// we have Modul, Instruction returns true to move()
			return mp_curr_node->m_data->move(path);	// can we move?

		assert(mp_curr_node->child(path) != nullptr);
		mp_curr_node = mp_curr_node->child(path);

		if (mp_curr_node->m_data == nullptr)
			return false;

		return mp_curr_node->m_data->move(path);
	}
	
	/* returns found node or nullptr if not found */
	TrieNode * find(std::vector<myutils::BYTE> path)
	{
		TrieNode *pNode = m_pRoot;
		m_curr_path.insert(m_curr_path.end(), path.begin(), path.end());	// save path

		TrieNode *pNext = pNode;
		for (size_t i = 0; i < path.size() && pNext; ++i)
		{
			pNode = pNext;
			pNext = pNext->child(path[i]);
		}
		
		return pNext;
	}
	
	/// print whole Trie to stream
	/**
	\param os output stream where function writes to
	*/
	void print(std::ostream &os = std::cout)
	{
		m_pRoot->print(os, 0);
	}
	
	/// get number of children
	inline size_t size() const
	{
		return m_size;
	}

	/// print path from stored from last reset
	inline std::vector<size_t> getPath()
	{
		return m_curr_path;
	}
	
	/// clear path
	inline void resetPath()
	{
		m_curr_path.clear();
	}

	void reset()
	{
		mp_curr_node = m_pRoot;
		resetPath();
	}

	inline Data &curr_data()
	{
		return mp_curr_node->m_data;
	}
private:
	TrieNode *mp_curr_node;
	TrieNode *m_pRoot;	// pointer to first empty node	
	size_t m_size;
	std::vector<size_t> m_curr_path;		// current path //todo: do we need this?
};


