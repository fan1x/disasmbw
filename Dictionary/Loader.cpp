#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "Loader.h"
#include "Instruction.h"
#include "utils.h"
#include "Info.h"
#include "Modul.h"
#include "CodeTrie.h"
#include "RulesTable.h"
#include "Operand.h"

#include <exception>
#include <iostream>
#include <unordered_map>
#include <cassert>
#include <set>
#include <utility>

#include <fstream>

using xercesc::DOMNodeList;
using xercesc::DOMNamedNodeMap;
using xercesc::DOMNode;
using xercesc::XMLString;

using std::vector;
using std::exception;
using std::string;
using std::for_each;

/* return attribute or nullptr */
inline char *attribute(DOMNamedNodeMap *attributes, const char *name)
{
	DOMNode *attr = attributes->getNamedItem(XMLString::transcode(name));
	if (attr == nullptr)
		return nullptr;

	return XMLString::transcode(attr->getNodeValue());
}

/* return element child nodes */
inline std::vector<DOMNode *> child_nodes(DOMNode *node)
{
	vector<DOMNode *> res;
	auto nodes = node->getChildNodes();
	for (size_t i = 0; i < nodes->getLength(); ++i)
	if (nodes->item(i)->getNodeType() == DOMNode::ELEMENT_NODE)
		res.push_back(nodes->item(i));

	return res;
}

/* no operand */

void OperandFactory::insert(char *name, char *op1, char *op2, char *op3, char *op4)
{
	auto op = op_map.find(name);
	if (op != op_map.end())
	{
		std::cerr << "Operand with name \"" << name << "\" already exists." << std::endl;
		return;
	}

	opencoding_names_t enc;
	enc[0] = op1;
	enc[1] = op2;
	enc[2] = op3;
	enc[3] = op4;
	op_map.insert({ name, enc });
}

AbstractOperand *OperandFactory::find_operand(const mode_opname_target_t &mot) const
{
	auto it = operands.find(mot);
	if (it == end(operands))
		throw std::runtime_error("Operand " + std::get<1>(mot) + " not found for mode " + std::to_string(std::get<0>(mot)) + " and target " + std::get<2>(mot));

	return it->second;
}

void OperandFactory::insert(const mode_opname_target_t &mot, AbstractOperand *ao)
{
	auto it = operands.find(mot);
	if (it == end(operands))
		operands.insert({ mot, ao });
	else
		it->second = it->second->merge(ao);
}

AbstractOperand *OperandFactory::find(const mode_opname_target_t &mot) const
{
	auto it = operands.find(mot);
	if (it == end(operands))
		return nullptr;

	return it->second;
}

operands_array_t OperandFactory::create_operand(const std::string &form, size_t mode, char *op_enc, const target_operand_map_t &target_operand) const
{
	auto strs = myutils::str_split(form);
	auto encoding = op_map.find(op_enc)->second;
	operands_array_t res;
	for (size_t i = 0; i < 4; ++i)
	{
		if (encoding.at(i) == nop())
		{
			res[i] = nullptr;
			continue;
		}

		string &str = *(begin(strs) + 1 + i);
		if (*rbegin(str) == ',')
			str.pop_back();

		if (encoding.at(i) == opcode())
		{
			res[i] = target_operand.find(str)->second;
			continue;
		}

		mode_opname_target_t mot(mode, encoding.at(i), str);
		auto it = operands.find(mot);
		if (it == end(operands))
			throw std::runtime_error("Don't exists rule for combination of operand: " + encoding.at(i) + "+" + str + "+" + std::to_string(mode));

		res[i] = it->second;
	}

	return std::move(res);
}

// Loader

Loader::Loader() : moduls(arr_modul_t()), opcodes(3), opcode_rules(opcode_rules_names_t())
{
	for (CodeTrie *&triecode : opcodes)
		triecode = new CodeTrie();
}


Loader::~Loader()
{
}


void Loader::process_operands(xercesc::DOMNodeList *nodes)
{
	auto operands = child_nodes(nodes->item(0));
	for (auto op : operands)
	{
		DOMNamedNodeMap *attr = op->getAttributes();

		operand_factory.insert(
			XMLString::transcode(attr->getNamedItem(XMLString::transcode("name"))->getNodeValue()),
			XMLString::transcode(attr->getNamedItem(XMLString::transcode("op1"))->getNodeValue()),
			XMLString::transcode(attr->getNamedItem(XMLString::transcode("op2"))->getNodeValue()),
			XMLString::transcode(attr->getNamedItem(XMLString::transcode("op3"))->getNodeValue()),
			XMLString::transcode(attr->getNamedItem(XMLString::transcode("op4"))->getNodeValue())
			);
	}
}

// make operands compatible with operand in instruction.xml
void Loader::process_instructions(xercesc::DOMNodeList *instructions)
{
	std::ofstream ofs16("i16_test");
	std::ofstream ofs32("i32_test");
	std::ofstream ofs64("i64_test");
	std::ofstream ofss16("n16_test");
	std::ofstream ofss32("n32_test");
	std::ofstream ofss64("n64_test");
	for (size_t i = 0; i < instructions->getLength(); ++i)
	{
		auto node = child_nodes(instructions->item(i))[0];	// <formats>
		auto nodes = child_nodes(node);	// <format>
		for (auto op : nodes)
		{
			char *form = "";
			char *opcode = "";
			try
			{
				DOMNamedNodeMap *attr = op->getAttributes();
				form = attribute(attr, "form");
				if (string(form) == "ADDPD xmm, xmm/m128")
				{
					auto i = 3;
				}
				opcode = attribute(attr, "opcode");
				char *op_enc = attribute(attr, "op_enc");
				char *desc = attribute(attr, "desc");
				char *bit64 = attribute(attr, "bit64");
				char *bit32 = attribute(attr, "bit32");
				char *bit16 = attribute(attr, "bit16");

				if (bit16 && *bit16 == '1')
				{
					ofs16 << opcode << endl;
					ofss16 << form << endl;
				}
					
				
				if (bit32 && *bit32 == '1')
				{
					ofs32 << opcode << endl;
					ofss32 << form << endl;
				}
					
				
				if (bit64 && *bit64 == '1')
				{
					ofs64 << opcode << endl;
					ofss64 << form << endl;
				}

				for (auto &code_opcode : create_instructions(form, opcode, op_enc, bit32, bit64, bit16, opcode_rules, moduls_names_))
					code_opcode.first->insert(opcodes, code_opcode.second);
			}
			catch (exception ex)
			{
				std::cerr << "Problem with instruction: " << form << " " << opcode << " \'" << ex.what() << std::endl;
			}
		}
	}
	ofs16.close();
	ofs32.close();
	ofs64.close();
	ofss16.close();
	ofss32.close();
	ofss64.close();
}

int fill_mode(char *bit32, char *bit64, char *bit16)
{
	std::bitset<3> res;
	assert(bit16 && bit32 && bit64);
	if (bit32[0] == '1')
		res[0] = true;
	if (bit64[0] == '1')
		res[1] = true;
	if (bit16[0] == '1')
		res[2] = true;

	return static_cast<int>(res.to_ulong());
}

int mode_to_int(char *mode)
{
	if (!mode)
		return Info::Architecture::universal;

	int res = 0;
	vector<string> modes = myutils::str_split(mode);
	for (const string &m : modes)
		if (m == "32bit")
			res += Info::Architecture::A32;
		else if (m == "64bit")
			res += Info::Architecture::A64;
		else if (m == "16bit")
			res += Info::Architecture::A16;
		else
		{
			std::cerr << "Unknown architecture: " << m << ". Set default architecture." <<  std::endl;
			return Info::Architecture::A32;
		}

	return res;
}

// returns result in s1
template<typename T>
void intersect_sets(std::set<T> &s1, const std::set<T> &s2)
{
	std::for_each(begin(s2), end(s2), [&s1](const T&ts1)
	{
		s1.erase(ts1);
	});
}

void get_owner_moduls(std::set<Modul * > &moduls, const Condition::CCoding &coding, const moduls_names_t &moduls_names)
{
	auto range = moduls_names.equal_range(coding.modul);
	for (auto it = range.first; it != range.second; ++it)
		if (it->second->find_coding(coding.coding))
			moduls.insert(it->second);
}

std::map<std::string, std::vector<Modul *> > get_used_moduls(const std::vector<std::pair<std::string, Condition> > &vc, const moduls_names_t &moduls_names)
{
	// if goes after with same name, take it like or
	using std::string;
	using std::vector;
	using std::set;

	if (vc.empty())
		return{};	// return empty map

	std::map<string, vector<Modul *> > used_moduls;	
	// init and first iteration
	Condition::CCoding c = begin(vc)->second.coding;
	string last_cond_name = begin(vc)->first;
	set<Modul *> last_moduls;
	get_owner_moduls(last_moduls, c, moduls_names);
	string modul_name = c.modul;
	for (size_t i = 1; i < vc.size(); ++i)
	{
		c = vc.at(i).second.coding;		
		if (vc.at(i).first != last_cond_name)
		{
			auto it = used_moduls.find(modul_name);
			last_cond_name = vc.at(i).first;

			if (it == end(used_moduls))	// no record, insert all coding
				used_moduls.insert(std::make_pair(modul_name, vector<Modul *>(begin(last_moduls), end(last_moduls))));
			else
			{
				auto resit = std::set_intersection(begin(it->second), end(it->second), begin(last_moduls), end(last_moduls), std::begin(it->second));
				it->second.resize(resit - begin(it->second));
			}
				
			last_moduls.clear();
		}

		get_owner_moduls(last_moduls, c, moduls_names);
		modul_name = c.modul;
	}
	if (!last_moduls.empty())
	{
		c = std::rbegin(vc)->second.coding;
		auto it = used_moduls.find(c.modul);
		if (it == end(used_moduls))
			used_moduls.insert(std::make_pair(c.modul, vector<Modul *>(begin(last_moduls), end(last_moduls))));
		else
		{
			auto resit = std::set_intersection(begin(it->second), end(it->second), begin(last_moduls), end(last_moduls), std::begin(it->second));
			it->second.resize(resit - begin(it->second));
		}
	}

	return std::move(used_moduls);
}

std::vector<opcode_codings_pair > Loader::process_prefix_condition(const std::vector<std::pair<std::string, Condition> > &vc, const std::string &hash)
{
	typedef std::map<string, vector<opcode_codings_pair > >cache_t;

	if (vc.empty())
		return vector<opcode_codings_pair>();
	
	static cache_t cache;
	auto cit = cache.find(hash);
	if (cit != end(cache))
		return cit->second;

	auto used_moduls = get_used_moduls(vc, moduls_names_);	
	vector<opcode_codings_pair> res;
	std::for_each(begin(used_moduls), end(used_moduls), [&res, &vc](const std::pair<string, std::vector<Modul *> > &name_moduls)
	{
		vector<opcode_codings_pair> same_name_moduls;
		std::for_each(begin(name_moduls.second), end(name_moduls.second), [&same_name_moduls, &vc](Modul *modul)
		{
			vector<opcode_codings_pair> modul_opcodes = modul->get_opcodes(vc);
			std::move(begin(modul_opcodes), end(modul_opcodes), std::back_inserter(same_name_moduls));
		});
		myutils::all_combination(res, same_name_moduls);
	});

	cache.insert(std::make_pair(hash, res));
	return std::move(res);
}

std::vector<std::pair<std::string, Condition> > create_prefix_conditions(const std::vector<std::string> &prefixes, const opcode_rules_names_t &opcode_rules, const moduls_names_t &moduls_names)
{
	vector<std::pair<string, Condition> > prefix_conditions;
	for (auto it_prefix = begin(prefixes) + 1; it_prefix != end(prefixes); ++it_prefix)
	{
		auto it = opcode_rules.find(*it_prefix);
		if (it == end(opcode_rules))
			continue;

		std::for_each(begin(it->second), end(it->second), [&](const opcode_rules_t &opr)
		{
			auto opcode_condition = opr.first;
			if (opcode_condition.first)
			{
				prefix_conditions.emplace_back(std::make_pair(*it_prefix, Condition(opcode_condition.first, opcode_condition.second)));
			}
		});
	}

	return std::move(prefix_conditions);
}

std::vector<std::pair<Code *, opcode_t > > Loader::create_instructions(const std::string &form, const std::string &opcode, char *op_enc, char *bit32, char *bit64, char *bit16, const opcode_rules_names_t &opcode_rules, const moduls_names_t &moduls_names)
{
	std::bitset<Info::arch_size> bmode(fill_mode(bit32, bit64, bit16));
	std::vector<std::pair<Code *, opcode_t > > res;
	for (size_t i = 0; i < bmode.size(); ++i)
		if (bmode[i])
		{
			auto instructions = create_instruction(form, opcode, op_enc, to_architecture(i), opcode_rules, moduls_names);
			res.insert(end(res), begin(instructions), end(instructions));
		}

	return res;			
}

std::vector<std::pair<Code *, opcode_t> > Loader::create_instruction(const std::string &form, const std::string &opcode, char *op_enc, size_t mode, const opcode_rules_names_t &opcode_rules, const moduls_names_t &moduls_names)
{
	vector<string> strs = myutils::str_split(opcode);
	auto it = strs.begin();

	// prefix starts with .
	vector<std::pair<opcode_t, codings_values_t > > prefix_opcodes;
	size_t pr_cond_count = 0;
	if ((*it)[0] == '.')
	{
		auto prefix_conditions = create_prefix_conditions(myutils::str_split(*it, "."), opcode_rules, moduls_names);
		pr_cond_count = prefix_conditions.size();
		prefix_opcodes = process_prefix_condition(prefix_conditions, *it);
		++it;
	}

	if (prefix_opcodes.empty())
		prefix_opcodes.emplace_back(opcode_codings_pair());	// dummy

	// opcode
	opcode_t vopcode;
	for (; it != strs.end() && (*it)[0] != '/'; ++it)
	{
		vopcode.emplace_back(opcode_data_t(CodeTrie::placement_t(0, BYTE_SIZE, true), std::stoi(*it, nullptr, Code::OPCODE_BASE)));
	}

	if (vopcode.rbegin() == vopcode.rend())
	{
		throw std::runtime_error("Problem with parsing opcode");
	}

	myutils::BYTE last_vopcode = static_cast<myutils::BYTE>(vopcode.rbegin()->second);

	// postfixes
	vector<Condition > postfixes;
	Modul *postfix_modul = nullptr;
	vector<std::pair<Code *, opcode_t> > instructions, res;
	bool plus = false;	// rule with plus exists
	bool cond = false;
	for (; it != strs.end(); ++it)
	{
		std::pair<int, string> mode_name(mode, *it);
		auto opcode_rules_range = plus_opcode_rules.equal_range(mode_name);
		for (auto rule_it = opcode_rules_range.first; rule_it != opcode_rules_range.second; ++rule_it)
		{
			target_operand_map_t &t_op_map = rule_it->second.first;
			opcode_condition_t &op_cond = rule_it->second.second;
			assert(op_cond.first);

			if (op_cond.first[0] == '+')	// add to last opcode number
			{
				*vopcode.rbegin() = opcode_data_t(CodeTrie::placement_t(0, BYTE_SIZE, true), last_vopcode + std::stoi(op_cond.first, 0, Instruction::OPCODE_BASE));
				size_t old_size = instructions.size();
				instructions.reserve(old_size + prefix_opcodes.size());
				if (cond)
					throw std::runtime_error("Postfix rules with + must go first");

				std::for_each(begin(prefix_opcodes), end(prefix_opcodes), [&, this](opcode_codings_pair opcode_codings)
				{
					auto &pbytes = opcode_codings.first;
					pbytes.reserve(pbytes.size() + vopcode.size());
					std::copy(begin(vopcode), end(vopcode), std::back_inserter(pbytes));
					auto operands = operand_factory.create_operand(form, mode, op_enc, t_op_map);
					instructions.emplace_back(std::pair<Code *, opcode_t>(new Instruction(form, mode, operands, opcode_codings.second, pr_cond_count), pbytes));
				});
				plus = true;
			}
			else if (op_cond.second)  // <coding, value>
			{
				cond = true;

				auto splits = myutils::str_split(op_cond.first, ".");
				auto owner = moduls_names.find(splits[0]);
				if (owner == end(moduls_names))
					throw std::runtime_error("Don't exists modul for given name in postfix rule");

				if (!postfix_modul)
					postfix_modul = owner->second;
				else if (postfix_modul != owner->second)
					throw std::runtime_error("Postfix rules allowed only for same modul");
									
				postfixes.push_back(Condition(op_cond.first, op_cond.second));				
			}
		}

		if (plus)
		{
			plus = false;
			vopcode.clear();
		}
		else if (!cond)
		{
			if (instructions.empty())
			{
				std::for_each(begin(prefix_opcodes), end(prefix_opcodes), [&, this](opcode_codings_pair opcode_codings)
				{
					auto &pbytes = opcode_codings.first;
					std::copy(begin(vopcode), end(vopcode), std::back_inserter(pbytes));
					auto operands = operand_factory.create_operand(form, mode, op_enc, {});
					Instruction *i = new Instruction(form, mode, operands, opcode_codings.second, pr_cond_count);
					instructions.emplace_back(std::pair<Code *, opcode_t>(i, pbytes));					
				});
				vopcode.clear();
				continue;
			}

			if (vopcode.empty())
				continue;

			vector<std::pair<Code *, opcode_t> > new_instructions(instructions.size() * vopcode.size());
			auto it_instructions = begin(instructions);
			auto it_vopcode = begin(vopcode);
			std::for_each(begin(new_instructions), end(new_instructions), [&it_instructions, &it_vopcode, &vopcode](std::pair<Code *, opcode_t> &code_opcode)
			{
				if (it_vopcode == end(vopcode))
				{
					it_vopcode = begin(vopcode);
					++it_instructions;
				}

				code_opcode = *it_instructions;
				code_opcode.second.emplace_back(*it_vopcode++);
			});
			vopcode.clear();
			std::swap(instructions, new_instructions);
		}
	}

	if (instructions.empty())
	{
		std::for_each(begin(prefix_opcodes), end(prefix_opcodes), [&, this](opcode_codings_pair &opcode_codings)
		{
			auto &pbytes = opcode_codings.first;
			std::copy(begin(vopcode), end(vopcode), std::back_inserter(pbytes));
			auto operands = operand_factory.create_operand(form, mode, op_enc, {});
			Instruction *i = new Instruction(form, mode, operands, opcode_codings.second, pr_cond_count);
			if (postfixes.empty())
				instructions.emplace_back(std::pair<Code *, opcode_t>(i, pbytes));
			else
			{
				auto pf = postfix_modul->gen_values(postfixes);
				i->modul_opcode = pf.size();
				std::for_each(begin(pf), end(pf), [&, this](size_t val)
				{
					pbytes.push_back({ CodeTrie::placement_t::byte_placement(), val });
					instructions.emplace_back(std::pair<Code *, opcode_t>(i, pbytes));
					pbytes.pop_back();
				});
			}			
		});
	}

	res.insert(end(res), begin(instructions), end(instructions));

	return res;
}

void Loader::process_opcode_rules(xercesc::DOMNodeList *rules_node)
{
	auto rules = rules_node->item(0)->getChildNodes();
	for (size_t i = 0; i < rules->getLength(); i++)
	{
		if (rules->item(i)->getNodeType() != DOMNode::ELEMENT_NODE)
			continue;
		string rule_name = attribute(rules->item(i)->getAttributes(), "name");
		int mode = mode_to_int(attribute(rules->item(i)->getAttributes(), "mode"));

		opcode_condition_t opcode_condition(nullptr, nullptr);
		vector<opcode_rule_t> opcode_rules;
		target_operand_map_t target_operand_map;
		std::vector<AbstractOperand *> operands;
		std::bitset<Info::arch_size> bmode(mode);
		
		for (DOMNode *node : child_nodes(rules->item(i)))
		{
			auto attrs = node->getAttributes();
			char *node_name = XMLString::transcode(node->getNodeName());
			if (node_name[0] == 'c')	// condition - should be max one - if not, takes the first one
			{
				opcode_condition.first = attribute(attrs, "op1");	// is coding or something special
				opcode_condition.second = attribute(attrs, "op2");
			}
			else  // rule
			{
				char *target = attribute(attrs, "t");
				char *source = attribute(attrs, "s");
				char *cop1 = attribute(attrs, "cop1");
				char *cop2 = attribute(attrs, "cop2");
				char *cop3 = attribute(attrs, "cop3");
				char *cop4 = attribute(attrs, "cop4");

				if (*std::begin(rule_name) == '/')	// postfix
				{
					AbstractOperand* op = create_abstract_operand(to_architecture(i), source, cop1, cop2, cop3, cop4);
					auto it = target_operand_map.find(target);
					if (it == end(target_operand_map))
						target_operand_map.insert({ target, op });
					else
						it->second = it->second->merge(op);
				}
				else
					opcode_rules.push_back(opcode_rule_t(target, source, cop1, cop2, cop3, cop4));
			}
		}

				
		if (*std::begin(rule_name) != '/')
		{
			auto it = Loader::opcode_rules.find(rule_name);
			if (it == end(Loader::opcode_rules))
				Loader::opcode_rules.emplace(rule_name, std::vector<opcode_rules_t>(1, std::make_pair(opcode_condition, opcode_rules)));
			else
				it->second.emplace_back(std::make_pair(opcode_condition, opcode_rules));
		}
		else
		{
			for (size_t i = 0; i < bmode.size(); ++i)
				if (bmode[i])
					plus_opcode_rules.insert({ std::make_pair(to_architecture(i), rule_name), std::make_pair(target_operand_map, opcode_condition) });
		}
	}
}

void Loader::process_moduls(DOMNodeList *moduls)
{
	bool prefix = true;
	size_t last_prefix_index;	// if no post opcode moduls
	for (size_t i = 0; i < moduls->getLength(); i++)
	{
		auto codings = child_nodes(moduls->item(i));
		string modul_name = attribute(moduls->item(i)->getAttributes(), "name");
		int mode = mode_to_int(attribute(moduls->item(i)->getAttributes(), "mode"));

		if (modul_name == "opcode")	// skip opcode	
		{
			last_prefix_index = Loader::moduls.size();
			prefix = false;
			continue;
		}

		for (auto coding : codings)
		{
			vector<Coding *> vcodings;
			vector<bool> modul_enter;
			auto codes = child_nodes(coding);
			for (DOMNode *code : codes)
			{
				DOMNamedNodeMap *attrs = code->getAttributes();
				char *enter_code = attribute(attrs, "code");
				unsigned char size = std::stoi(attribute(attrs, "size"));
				char *defval = attribute(attrs, "default");
				char *opcode = attribute(attrs, "opcode");
				string byte = XMLString::transcode(code->getNodeName());
				Coding *empty_coding = nullptr;	// empty coding -> want to add at first place in modul
				if (enter_code)
				{
					vector<bool> vb_enter = myutils::to_bvector(static_cast<size_t>(std::stoi(XMLString::transcode(enter_code), 0, byte == "bits" ? 2 : 16)), byte == "byte" ? 8 : size);
					std::copy(begin(vb_enter), end(vb_enter), std::back_inserter(modul_enter));
				}					
				else 
					vcodings.push_back(new Coding(attribute(attrs, "name"), byte, size, defval, opcode));
			}	

			Modul *modul = new Modul(modul_name, mode, modul_enter, {}, vcodings);	
			if (prefix)
			{
				opcode_data_t last_first_opdata;
				if (modul_enter.size() < BYTE_SIZE)	// we had to generate
				{
					bool first = true;
					auto mopcodes = modul->get_opcodes();					
					for (opcode_codings_pair &opcode_codings : mopcodes)
					{
						opcode_codings.second.insert({ modul_name + "." + modul_name, 0 });	// every modul has its own name
						Modul *new_modul = new Modul(modul_name, mode, modul_enter, opcode_codings.second, vcodings);
						new_modul->insert(opcodes, opcode_codings.first);
						if (first)
						{
							first = false;
							moduls_names_.emplace(modul_name, new_modul);
						}
					}
					delete modul;
				}
				else if (modul_enter.size() == BYTE_SIZE)
				{
					auto value = myutils::to_number(modul_enter);
					modul->insert(opcodes, { opcode_data_t(CodeTrie::placement_t(0, BYTE_SIZE, true), value) });
					moduls_names_.emplace(modul_name, modul);
				}
				else
				{
					delete modul;
					throw std::runtime_error("Enter code for modul must be lower or equal than 1 byte");
				}
			}
			else
			{
				moduls_names_.emplace(modul_name, modul);	// save moduls name
				Loader::moduls.push_back(modul);
			}
		}
	}

	postfix_moduls_start = std::begin(Loader::moduls) + last_prefix_index;
//	fill_codings(Loader::moduls);
}

void Loader::process_operand_rules(DOMNodeList *rules_node)
{
	auto rules = rules_node->item(0)->getChildNodes();
	for (size_t i = 0; i < rules->getLength(); i++)
	{
		if (rules->item(i)->getNodeType() != DOMNode::ELEMENT_NODE)
			continue;
		char *name = attribute(rules->item(i)->getAttributes(), "name");
		int mode = mode_to_int(attribute(rules->item(i)->getAttributes(), "mode"));

		condition_list_t conditions;	
		vector<operand_rules_t> operand_rules;
		for (DOMNode *node : child_nodes(rules->item(i)))
		{
			DOMNamedNodeMap *attrs = node->getAttributes();
			if (XMLString::transcode(node->getNodeName())[0] == 'c')	// condition 
			{
				char *op1 = attribute(attrs, "op1");
				char *op2 = attribute(attrs, "op2");
				conditions.push_back(Condition(op1, op2));
			}
			else   // rule
			{
				char *target = attribute(attrs, "t");
				char *source = attribute(attrs, "s");
				char *cop1 = attribute(attrs, "cop1");	//todo: add to conditions??
				char *cop2 = attribute(attrs, "cop2");
				char *cop3 = attribute(attrs, "cop3");	
				char *cop4 = attribute(attrs, "cop4");
				
				create_operand(conditions, name, mode, target, source, cop1, cop2, cop3, cop4);
			}
		}		
	}
}

void Loader::create_operand(const condition_list_t &conditions, char *name, int mode, char *target, char *source, char *cop1, char *cop2, char *cop3, char *cop4)
{
	if (source == nullptr)
		throw std::runtime_error("Operand source not found in " + std::string(name? name : ""));
	std::bitset<Info::arch_size> bmode(mode);
	for (size_t i = 0; i < bmode.size(); ++i)
		if (bmode[i])
		{
			
			AbstractOperand *ao = create_abstract_operand(to_architecture(i), source, cop1, cop2, cop3, cop4);
			mode_opname_target_t mot(to_architecture(i), name, target ? target : "");
			if (conditions.empty())
			{
				operand_factory.insert(mot, ao);
			}
			else
			{
				if (auto *op = operand_factory.find(mot))
				{
					dynamic_cast<RulesOperand *>(op)->insert(conditions, ao);
				}
				else
				{
					operand_factory.insert(mot, new RulesOperand(to_architecture(i), ao, conditions));
				}
			}
		}
}

AbstractOperand *Loader::create_abstract_operand(size_t mode, const std::string &source, char *cop1, char *cop2, char *cop3, char *cop4) const
{
	Condition condition(cop1, cop2);
	Condition condition2(cop3, cop4);

	if (source[0] == '@')
	{
		abstract_operands_t res;
		string buff;
		bool first_special = false;
		for (auto it = source.begin() + 1; it != source.end(); ++it)
		{
			if (*it == '$')
				if (!first_special)	// first appearance
				{
					if (!buff.empty())
						res.push_back(new NameOperand(buff, mode, condition, condition2));
					buff = '$';
					first_special = true;
				}
				else
				{
					buff += '$';
//					res.push_back(new VariableOperand(operand_factory.find_operand(mode_opname_target_t(mode, buff, "")), mode, condition, condition2));
					res.push_back(operand_factory.find_operand(mode_opname_target_t(mode, buff, "")));
					buff.erase();
					first_special = false;
				}
			else if (*it == '\'')
				if (!first_special)
				{
					if (!buff.empty())
						res.push_back(new NameOperand(buff, mode, condition, condition2));
					buff = *(++it);
					first_special = true;
				}
				else
				{
					res.push_back(new CodingOperand(moduls_names_, buff, mode, condition, condition2));
					buff.erase();
					first_special = false;
				}
			else
				buff += *it;
		}
		if (!buff.empty())	// last
			res.push_back(new NameOperand(buff, mode, condition, condition2));

		if (res.size() == 1)
			return res.at(0);
		
		return new Operand(mode, res, condition, condition2);
	}
	return new NameOperand(source, mode, condition, condition2);
}

bool Loader::load_instructions(const char *file)
{
	try
	{
		xercesc::XMLPlatformUtils::Initialize();
	}
	catch (const xercesc::XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		std::cerr << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return false;
	}

	xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);    // optional

	xercesc::ErrorHandler* errHandler = (xercesc::ErrorHandler*) new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);

	try {
		parser->parse(file);
	}
	catch (const xercesc::XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		std::cerr << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return false;
	}
	catch (const xercesc::DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		std::cerr << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return false;
	}
	catch (exception ex) {
		std::cerr << "Unexpected Exception \n" << ex.what();
		return false;
	}

	xercesc::DOMDocument *doc = parser->getDocument();
	process_operands(doc->getElementsByTagName(XMLString::transcode("operands")));
	process_instructions(doc->getElementsByTagName(XMLString::transcode("instruction")));

	delete parser;
	delete errHandler;

	return true;
}

bool Loader::load_format(const char *file)
{
	try
	{
		xercesc::XMLPlatformUtils::Initialize();
	}
	catch (const xercesc::XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		std::cerr << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return false;
	}

	xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);    // optional

	xercesc::ErrorHandler* errHandler = (xercesc::ErrorHandler*) new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);

	try {
		parser->parse(file);
	}
	catch (const xercesc::XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		std::cerr << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return false;
	}
	catch (const xercesc::DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		std::cerr << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return false;
	}
	catch (exception ex) {
		std::cerr << "Unexpected Exception \n" << ex.what();
		return false;
	}

	xercesc::DOMDocument *doc = parser->getDocument();
	process_moduls(doc->getElementsByTagName(XMLString::transcode("modul")));
	process_operand_rules(doc->getElementsByTagName(XMLString::transcode("operand_rules")));
	process_opcode_rules(doc->getElementsByTagName(XMLString::transcode("opcode_rules")));
	
	delete parser;
	delete errHandler;

	return true;
}


void Loader::move(trie_codes_t &opcodes, opcode_rules_names_t &opcode_rules, arr_modul_t &moduls, arr_modul_t::const_iterator &moduls_after_start)
{
	opcodes = std::move(Loader::opcodes);
	opcode_rules = std::move(Loader::opcode_rules);
	moduls = std::move(Loader::moduls);
	moduls_after_start = std::move(postfix_moduls_start);
}