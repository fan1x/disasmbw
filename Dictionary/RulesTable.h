#pragma once

#include "DNode.h"
#include "Condition.h"

#include <list>
#include <string>
#include <unordered_map>
#include <vector>
#include <unordered_map>

class AbstractOperand;
class Modul;

typedef DNode<AbstractOperand *> operand_node_t;
typedef std::vector<Condition::CCoding> codings_t;
typedef std::vector<std::pair<std::string, std::string>> double_string_list_t;

class RulesTable
{
public:
	RulesTable();
	~RulesTable(void);

	// insert new operand into rulestable
	void insert(const std::vector<Condition> &condition_list, AbstractOperand *operand);
	// set all ptrs to default
	void reset();

	codings_t codings;
	codings_t::iterator curr_coding;	// current coding
	operand_node_t *curr_node;
private:
	operand_node_t *root_;
};

typedef std::unordered_map<std::string, RulesTable *> rulestable_names_t;

