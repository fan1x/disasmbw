#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "Dictionary.h"
#include "utils.h"
#include "Loader.h"
#include "CodeTrie.h"

#include <bitset>
#include <vector>
#include <algorithm>
#include <exception>
#include <iostream>

using std::vector;
using std::bitset;
using std::for_each;
using std::exception;
using std::cerr;
using std::string;

using xercesc::XMLString;
using xercesc::DOMNode;
using xercesc::DOMDocument;
using xercesc::DOMNamedNodeMap;
using xercesc::DOMNodeList;
using xercesc::XMLException;

bool Dictionary::initialized = false;

Dictionary::Dictionary(const std::string &fformat, const std::string &finstructions, const Info &info) :
before_opcode_(true), info(info), curr_trie(nullptr), curr_code(nullptr),
closed_ofile(true), instruction_length(0), second_chance(false),
piece(false), byte_count(0), err_ofstream(err_file()), cerrbuf(std::cerr.rdbuf())
{
	// set cerr to file
	cerrbuf = std::cerr.rdbuf();
	std::cerr.rdbuf(err_ofstream.rdbuf());

	initialized = init(finstructions.c_str(), fformat.c_str());
	old_bytes.reserve(0xf);	// 0xf is max length of 1 instruction
}

Dictionary::~Dictionary(void)
{
	if (!closed_ofile)
		ofile.close();

	// change back cerr
	std::cerr.rdbuf(cerrbuf);
}

void Dictionary::rollback()
{
	curr_trie->reset();	
	if (old_bytes.size() == 1)
	{
		if (!simple_find(*std::begin(old_bytes)))
			store_byte(*std::begin(old_bytes));

		return;
	}

	auto cold_bytes(std::move(old_bytes));	// move, because found instruction clears old_bytes
	auto it = begin(cold_bytes);
	auto start_it = it;	// start of unprocessed bytes
	if (codings_values.empty())	// no prefixes so we tried found old bytes
	{
		store_byte(*start_it++);
		++it;
	}
	size_t old_bc = byte_count;
	while (it != cend(cold_bytes))
	{
		if (!simple_find(*it))
		{
			store_byte(*start_it++);	// throw away first byte
			it = start_it;
		}
		else if (old_bc != byte_count)	// found new instruction
		{
			old_bc = byte_count;
			start_it = ++it;
		}
		else
			++it;
	}

	std::copy(start_it, std::end(cold_bytes), std::back_inserter(old_bytes));	// copy back unprocessed bytes
}

inline bool Dictionary::simple_find(byte_t byte)
{
	if (!curr_code)
	{
		if (!curr_trie->find(byte))
			return false;

		if (curr_code = curr_trie->curr_data())
		{
			data_found();
			curr_code->find(*this, byte);
		}
	}
	
	return true;
}

bool Dictionary::find(byte_t byte)
{
	++instruction_length;
	old_bytes.push_back(byte);
	if (byte_count == 0x4e878)
	{
		auto i = 2;
	}
	if (!curr_code)
	{
		if (!curr_trie->find(byte))
		{
			rollback();			
			return false;
		}
		
		if (curr_code = curr_trie->curr_data())
		{
			data_found();
		}
		else
			return false;
	}

	return curr_code->find(*this, byte);
}

void Dictionary::end()
{
	if (!closed_ofile)
	{
		ofile.close();
		closed_ofile = true;
	}
}

void Dictionary::start(const std::string out_file, size_t start)
{
	if (closed_ofile)
	{
		ofile.open(out_file, std::ifstream::out);
		byte_count = start;
		closed_ofile = false;
		curr_trie = codes.at(info.arch);
		reset();
	}
}

void Dictionary::data_found()
{
	if (!curr_trie->piece)
		found = true;
	else
		piece = true;
}

void Dictionary::reset()
{
	instruction_length = 0;
	curr_code = nullptr;
	prefixes.clear();
	curr_modul_ = begin_moduls_after;	// reset pointer to moduls
	before_opcode_ = true;	// reset to start
	codings_values.clear();
	curr_trie->reset();	// reset tire
	old_bytes.clear();
	second_chance = false;
	piece = false;
}

bool Dictionary::init(const char *instr_file, const char *format_file)
{
	Loader l;
	if (!l.load_format(format_file))
		return false;

	if (!l.load_instructions(instr_file))
		return false;

	l.move(codes, opcode_rules, moduls, begin_moduls_after);

	return true;
}



