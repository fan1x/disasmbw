#pragma once

#include "Info.h"
#include "Modul.h"
#include "Instruction.h"
#include "CodeTrie.h"

#include <array>
#include <vector>
#include <fstream>
#include <iostream>

// fwd decl
class Code;
class RulesTable;
class AbstractOperand;

typedef std::vector<CodeTrie *> trie_codes_t;
typedef std::array<std::pair<std::string, AbstractOperand *>, 4> operands_t;
typedef std::pair<char *, char *> opcode_condition_t;	// pair<coding, value> 
typedef std::tuple<char *, char *, char *, char *, char *, char *> opcode_rule_t;
typedef std::pair<opcode_condition_t, std::vector<opcode_rule_t> > opcode_rules_t;	// <mode, condition, opcode_rules[]>
typedef std::map<std::string, std::vector<opcode_rules_t> > opcode_rules_names_t;

class Dictionary
{
public:
	Dictionary(const std::string &fformat = format_file, const std::string &finstructions = instruction_file, const Info &info = Info(Info::InstructionArchitecture::IA32));
	~Dictionary(void);

	inline void set_flags(const Info &info)
	{
		Dictionary::info = info;
	}

	inline const Info &get_flags() const
	{
		return info;
	}

	// close output file
	void end();
	// open+create output file
	void start(const std::string out_file, size_t start);
	
	// returns true if instruction found
	bool find(byte_t byte);
	arr_modul_t::const_iterator Dictionary::next_modul(operands_t operands, const arr_modul_t::const_iterator &last_modul);	
	// reset all variables for finding instruction
	void reset();

	friend bool Modul::find(Dictionary &dictionary, byte_t byte);
	friend bool Instruction::find(Dictionary &dictionary, byte_t byte);

	friend bool Modul::finish(Dictionary &dictionary);
	friend bool Instruction::finish(Dictionary &dictionary);
	
	static bool initialized;
private:
	static const size_t BITS = 8;
	static const size_t BYTE = 256;
	static const std::string format_file;
	static const std::string instruction_file;

	enum { ROLLBACK, NOT_FOUND };

	void data_found();
	// return true if properly initialized
	bool init(const char *instr_file, const char *format_file);
	// print result to file
	inline void process_result(const std::string &instruction)
	{
		ofile << std::hex << byte_count << ": " <<  instruction << std::endl;
		byte_count += instruction_length;
		instruction_length = 0;
	}
	// find next modul and insert it to curr_modul
	void next_modul(const codings_names_t &codings);
	// resolve problems with bad byte
	void rollback();
	// simple find used in rollback
	bool simple_find(byte_t byte);
	inline void store_byte(byte_t byte)
	{
		byte_t old = instruction_length - 1;
		instruction_length = 1;
		process_result("DB " + myutils::to_hexstring(byte));
		reset();
		instruction_length = old;
	}

	inline const std::string &err_file() const
	{
		static const std::string file = ".\\..\\log.txt";
		return file;
	}

	trie_codes_t codes;		// trie for prefixes + opcode
	
	arr_modul_t moduls;	// moduls 
	arr_modul_t::const_iterator begin_moduls_after;	// moduls before opcode
	opcode_rules_names_t opcode_rules;

	codings_values_t codings_values;
	arr_modul_t prefixes;

	bool piece;	// last byte should be used on opcode + modul
	bool before_opcode_;
	bool shift_start;
	bool found;	

	std::vector<byte_t> old_bytes;
	bool second_chance;	// are we trying rollback?

	byte_t instruction_length;
	size_t byte_count;	
	bool closed_ofile;
	std::ofstream ofile;
	std::ofstream err_ofstream;
	std::streambuf *cerrbuf;

	byte_t old_byte;

	CodeTrie *curr_trie;	// currently used trie
	arr_modul_t::const_iterator curr_modul_;	// current modul
	Code *curr_code;	// current instruction

	Info info;
};

