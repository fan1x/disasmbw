#pragma once

#include <vector>
#include <list>
#include <unordered_map>
#include <utility>
#include <iostream>
#include <string>
#include <array>
#include <tuple>

#include "Condition.h"
#include "CodeTrie.h"

// fwd decl
class AbstractOperand;
class MultiOperand;
class Operand;
class Modul;
class RulesTable;
class xercesc::DOMNodeList;
class Code;


typedef std::unordered_multimap<std::string, Modul *> moduls_names_t;
typedef std::vector<Modul *> arr_modul_t;

typedef std::tuple<size_t, std::string, std::string> mode_opname_target_t;
typedef std::map<mode_opname_target_t, AbstractOperand * > operands_map_t;
typedef std::pair<char *, char *> opcode_condition_t;
typedef std::map<std::string, AbstractOperand * > target_operand_map_t;
typedef std::multimap<std::pair<size_t, std::string >, std::pair<target_operand_map_t, opcode_condition_t > > map_plus_operand_rules_t;

typedef std::array<AbstractOperand *, 4> operands_array_t;
typedef std::array<std::string, 4> opencoding_names_t;
typedef std::unordered_map<std::string, RulesTable *> rulestable_names_t;
typedef std::vector<CodeTrie *> trie_codes_t;
typedef std::tuple<std::string, std::string, char *, char *, char *, char *> operand_rules_t;	// <target, source, cop1, cop2>

typedef std::tuple<char *, char *, char *, char *, char *, char *> opcode_rule_t;
typedef std::pair<opcode_condition_t, std::vector<opcode_rule_t> > opcode_rules_t;	// <mode, condition, opcode_rules[]>
typedef std::map<std::string, std::vector<opcode_rules_t> > opcode_rules_names_t;
typedef std::pair<CodeTrie::placement_t, size_t> opcode_data_t;
typedef std::vector<opcode_data_t> opcode_t;
typedef std::unordered_map<std::string, size_t > codings_values_t;
typedef std::pair<opcode_t, codings_values_t> opcode_codings_pair;

struct OperandFactory
{
	inline const std::string &nop() const
	{
		static std::string NOP = "NA";
		return NOP;
	}
	inline const std::string &opcode() const
	{
		static std::string OPCODE = "opcode";
		return OPCODE;
	}

	// insert operand definition from instruction
	void insert(char *name, char *op1, char *op2, char *op3, char *op4);
	operands_array_t create_operand(const std::string &form, size_t mode, char *op_enc, const target_operand_map_t &target_operand) const;
	void insert(const mode_opname_target_t &mot, AbstractOperand *ao);
	AbstractOperand *find(const mode_opname_target_t &mot) const;
	AbstractOperand *find_operand(const mode_opname_target_t &mot) const;
private:
	std::unordered_map<std::string, opencoding_names_t> op_map;
	operands_map_t operands;
};

class Loader
{
public:
	Loader();
	~Loader();
	/* load instructions from file and returns result */
	bool load_instructions(const char *file);
	/* load instructions format from file and returns result */
	bool load_format(const char *file);

	void move(trie_codes_t &opcodes, opcode_rules_names_t &opcode_rules, arr_modul_t &moduls, arr_modul_t::const_iterator &moduls_after_start);
private:
	/* process & loads moduls to moduls_ */
	void process_moduls(xercesc::DOMNodeList *moduls);
	/* process & loads operand rules to rules_ */
	void process_operand_rules(xercesc::DOMNodeList *rules);
	void process_instructions(xercesc::DOMNodeList *instructions);
	void process_opcode_rules(xercesc::DOMNodeList *rules);
	void process_operands(xercesc::DOMNodeList *nodes);
	std::vector<opcode_codings_pair > process_prefix_condition(const std::vector<std::pair<std::string, Condition> > &vc, const std::string &hash);

	void create_operand(const condition_list_t &conditions, char *name, int mode, char *target, char *source, char *cop1, char *cop2, char *cop3, char *cop4);
	AbstractOperand *create_abstract_operand(size_t mode, const std::string &source, char *cop1, char *cop2, char *cop3, char *cop4) const;
	std::vector<std::pair<Code *, opcode_t> > create_instruction(const std::string &form, const std::string &opcode, char *op_enc, size_t mode, const opcode_rules_names_t &opcode_rules, const moduls_names_t &moduls_names);
	std::vector<std::pair<Code *, opcode_t> > create_instructions(const std::string &form, const std::string &opcode, char *op_enc, char *bit32, char *bit64, char *bit16, const opcode_rules_names_t &opcode_rules, const moduls_names_t &moduls_names);

	arr_modul_t::const_iterator first_modul(const operands_array_t &operands);
	
	rulestable_names_t rules_tables_;
	moduls_names_t moduls_names_;
	opcode_rules_names_t opcode_rules;	
	map_plus_operand_rules_t plus_opcode_rules;

	arr_modul_t moduls;
	arr_modul_t::const_iterator postfix_moduls_start;
	trie_codes_t opcodes;

	OperandFactory operand_factory;
};

