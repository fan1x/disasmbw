#pragma once

#include <array>
#include <vector>
#include <algorithm>
#include <ostream>
#include <string>
#include <utility>

using std::vector;
using std::string;
using std::for_each;
using std::endl;
using std::pair;

template<typename Data>
class DNode
{
public:
	typedef std::vector<DNode *> ArrDNode;
	/// ctor
	DNode(Data data, size_t length = 0):
		m_data(data), m_children(ArrDNode(length, nullptr))
	{
	}
	/// ctor
	DNode(size_t length = 0):
		m_children(ArrDNode(length, nullptr))
	{
	}
	/// dtor
	~DNode(void)
	{
		std::for_each(m_children.begin(), m_children.end(), [](DNode *&node) 
		{
			delete node;
		});
	}
	/// return value of child placed at val 
	/**
	\return node at val place or 0 if there is non
	\param val place of the looking child
	*/
	DNode *child(size_t val) const
	{
		return m_children.at(val);
	}

	DNode *&child(size_t val)
	{
		return m_children.at(val);
	}

	void move_children(ArrDNode &&new_children)
	{
		m_children = std::move(new_children);
	}
	
	/// insert new element whereto
	/**
	\param whereto place where is new element inserted
	\param data data used to fill ctor
	\return pointer to created node
	*/
	DNode *insertNew(unsigned char whereto, Data data, size_t length)
	{
		if (m_children.at(whereto) == nullptr)
			m_children.at(whereto) = new DNode<Data>(data, length);
		return m_children.at(whereto);
	}

	DNode *insertNew(unsigned char whereto, Data data)
	{
		if (m_children.at(whereto) == nullptr)
			m_children[whereto]= new DNode<Data>(data);
		return m_children.at(whereto);
	}
	/*
	\sa insertNew(unsigned char whereto, Data data)
	*/
	DNode *insertNew(unsigned char whereto, size_t length)
	{
		if (m_children.at(whereto) == nullptr)
			m_children.at(whereto) = new DNode<Data>(length);
		return m_children.at(whereto);
	}
	DNode *insertNew(unsigned char whereto)
	{
		if (m_children.at(whereto) == nullptr)
			m_children.at(whereto) = new DNode<Data>(m_children.size());
		return m_children.at(whereto);
	}

	DNode *unchecked_insert(unsigned char whereto, size_t length)
	{
		m_children.at(whereto) = new DNode<Data>(length);
		return m_children.at(whereto);
	}

	/// print node with all its children recursively in order
	/**
	\param os stream where print writes to
	\param shift number of spaces before each line
	*/
	void print(std::ostream &os, size_t shift) 
	{
		string wspace = "";
		for(size_t i = 0;i < shift ; ++i)
		{
			wspace += " ";
		}
		
		os << wspace << to_string(d) << endl;

		for_each(m_children.begin(), m_children.end(), [&os, &shift](DNode *&dnode)
		{
			if (dnode != null)
				dnode->print(os, shift+1);
		});
	}

	void resize(size_t size)
	{
		m_children.resize(size);
	}
	/// find the further node on path from pNode.
	/**
	\param path path we're trying to find
	\param pNode node from we are trying to find next, also the last node on path is returned in this parameter
	\return value whether we find the whole path
	*/
	static bool find(const std::vector<size_t> &path, DNode<Data> *&pNode)
	{
		DNode<Data> *pNext = pNode;
		for(size_t i = 0; i < path.size() && pNext; ++i)
		{
			pNode = pNext;
			pNext = pNext->child(path[i]);
		}

		if (pNext)
		{
			pNode = pNext;
			return true;
		}
	
		return false;
	}

	inline size_t size() const
	{
		return m_children.size();
	}

	Data m_data;	// member for storing data 
private:
	ArrDNode m_children;	// array of children
};