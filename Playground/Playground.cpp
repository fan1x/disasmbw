#include <iostream>
#include <memory>
#include <functional>
#include <Windows.h>
#include <thread>
#include <string>
#include <vector>
#include <exception>
#include <algorithm>
#include <map>
#include <set>
#include <unordered_map>
#include "../utilities/utils.h"

using namespace std;

typedef vector<size_t> vs_t;
typedef set<int *> sip_t;
// vs_t vs;

vs_t get_r(int i)
{
	vs_t vs = vs_t(10, i);
	return std::move(vs);
}

typedef vector<vector<size_t> > vvs_t;

int main()
{
	vs_t vs1(10);
	vs1.push_back(3);

	vs_t::iterator it = begin(vs1) + 3;
	vs_t vs2 = std::move(vs1);
	vs1.clear();

	cout << *it;

	int x;
	cin >> x;
}